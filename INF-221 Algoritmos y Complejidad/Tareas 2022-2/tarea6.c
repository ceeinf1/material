#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char *argv[]) { //se declaran las variables a utilizar 
    int i, j, m, n, S, max, max_i, max_j; //max es el maximo puntaje, max_i y max_j son las posiciones del maximo puntaje
    char *X, *Y; //X y Y son las secuencias a comparar 
    int **M; //M es la matriz de puntajes 

    if (argc != 3) { //se verifica que se ingresen las secuencias a comparar 
        printf("Error: debe ingresar dos secuencias de caracteres como argumentos.\n"); //si no se ingresan se muestra un mensaje de error
        return 1; //se retorna 1 para indicar que hubo un error 
    }

    X = argv[1]; //se asigna la secuencia X 
    Y = argv[2]; //se asigna la secuencia Y 
    m = strlen(X);//se asigna el largo de X 
    n = strlen(Y); //se asigna el largo de y

    M = (int **) malloc((m + 1) * sizeof(int *)); //se reserva memoria para la matriz de puntajes 
    for (i = 0; i <= m; i++) { //se recorre la matriz 
        M[i] = (int *) malloc((n + 1) * sizeof(int)); //[i] es la fila y [j] es la columna 
    } 

    for (i = 0; i <= m; i++) { 
        M[i][0] = -2 * i; //se asigna el valor de la primera columna 
    }
    for (j = 0; j <= n; j++) { //se asigna el valor de la primera fila 
        M[0][j] = -2 * j;   //se asigna el valor de la primera columna
    }

    for (i = 1; i <= m; i++) {  //se recorre la matriz
        for (j = 1; j <= n; j++) {  //se recorre la matriz
            if (X[i - 1] == Y[j - 1]) { //se compara el simbolo de X con el de Y 
                S = 2; //si son iguales se asigna 2 puntos
            } else { 
                S = -1; //si no son iguales se asigna -1 punto
            }
            max = M[i - 1][j - 1] + S; 
            max_i = i - 1; 
            max_j = j - 1;
            if (M[i - 1][j] - 2 > max) { //se compara el valor de la fila anterior con el maximo 
                max = M[i - 1][j] - 2; //si es mayor se asigna el valor de la fila anterior menos 2 puntos
                max_i = i - 1; 
                max_j = j;
            }
            if (M[i][j - 1] - 2 > max) {
                max = M[i][j - 1] - 2;
                max_i = i;
                max_j = j - 1;
            }
            M[i][j] = max;
        }
    }

    printf("%s\n", X);//imprime secuencias de caracteres ingresada 
    printf(" %s\n", Y);
    printf(" %d\n", M[m][n]);//puntaje maximo
}

