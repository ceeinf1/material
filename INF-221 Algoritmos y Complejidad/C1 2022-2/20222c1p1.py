#!/usr/bin/env python

from sympy import *

(a, b, c, e) = symbols('a b c e');

def expr(e):
    expr1 = 1 + a * e + b * e ** 2 + O(e**3, e);
    expr2 = 1 + a * e + c * e * e
    expr3 = sqrt(1 - 2 * a * e)
    return expr1 * expr3 / expr2

print(series(expr(e), e, 0, 2))
