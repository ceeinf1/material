package claselp;

public class Persona {
    private int edad;
    public String nombre;
    protected String rut;
    public Persona(int edad, String nombre, String rut){
        this.edad=edad;
        this.nombre=nombre;
        this.rut=rut;
    }
    public int Getedad(){
        return this.edad;
    }
    public void AddNombre(String nombre){
        this.nombre=nombre;
    }
    public void verPersona(){
        System.out.println("[°] "+this.nombre +" - " + this.edad);
    }
}