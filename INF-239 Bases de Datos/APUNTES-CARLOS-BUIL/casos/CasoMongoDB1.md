# Caso 1: MongoDB y datos semi estructurados

## Introducción

La empresa SandBird, conocida por su servicio de microblogueo, se ha dado cuenta de que el modelo de datos relacional puede que no sea el más adecuado para su servicio. Liohannes Haskell, se ha dado cuenta de que quizá, para una web social, quizá el almacenamiento de documentos sea la alternativa más eficiente debido a la cantidad de datos que no siempre cumplirán con el modelo relacional definido.

Si la idea que tiene es buena o no es debatible, lo que nos importa ahora es que estos últimos meses Liohannes ha contactado otra vez con el Departamento de Informática de la prestigiosa Universidad Técnica Federico Santa Marı́a de nuestro universo para ayudarlo con la realización de un sistema web donde se desarrollará la red social más ambiciosa ideada hasta el momento y popularizarlo por todos los 3000 universos. Y como todo el mundo sabe también, el cliente manda.

Lo que no tiene claro Liohannes es el modelo, ya que es imposible garantizar que un usuario tenga página web, foto, que tenga hashtag o no, etc. Es decir, no es posible garantizar que las restricciones de integridad se cumplan.

## Modelo de datos

El Departamento de Informática ha vuelto a tener una reunión con Liohannes, y éste ha concretado un poco más, indicando en rojo aquellos atributos que no tiene seguridad de que existan para cada instancia de la base de Nuevo.

![datos modelo de datos](./imagenes/modelo.png)

Lamentablemente y como ya se ha comentado Liohannes no tiene muy clara la distribución de los datos que pueden faltar ha decidido experimentar con dichas distribuciones. 
 * Puede haber posts anónimos, son alrededor de un 30% de los posts de la aplicación. Dichos posts tienen todos al mismo usuario `anonymous`.
 * La cantidad de administradores es la misma que en el modelo relacional.
 * Los usuarios pueden seguir o no al usuario anónimo (alrededor de un 10% de usuarios siguen al usuario anónimo). Un usuario sigue al usuario anónimo si un usuario ha borrado su cuenta.
 * El usuario anónimo puede seguirse a sí mismo (un 5% de los seguidores son anónimos entre sí).
 * Los usuarios emisores de posts pueden ser anónimos y pueden estar vacíos (sucede en un 10% del 30% de los posts anónimos).
 * Sólo un 10% de los usuarios tienen su perfil completo, es decir, al 90% de los usuario les falta alguno de los datos de la tabla Usuarios.
 
## Instrucciones de entrega
 
Las preguntas que se les solicita desarrollar vendrán dentro de un Jupyter Notebook, el cual tendrán que desarrollar a partir del Notebook que se adjunta.
 
