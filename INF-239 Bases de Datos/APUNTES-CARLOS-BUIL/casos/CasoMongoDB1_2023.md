# Caso 1: MongoDB y datos semi estructurados

## Introducción

El juego LOL de la Tarea de Base de Datos parece que necesita una estructura más flexible, ya que en cualquier momento puede ser que no todos los usuarios de la aplicación recuerden introducir sus datos. 

Lo que sucede en la realidad es que hay jugadores sin nombre, por diversas razones, como que se les olvidó escribirlo ante las ansias de jugar o borraron su cuenta. Sin embargo, es  necesario guardar sus datos de igual manera. En concreto lo que sucede es lo siguiente: 
 * Puede haber jugadores anónimos, son alrededor de un 30% de los jugadores que participan. Dichos jugadores tienen todos al mismo nombre: `anonymous`.
 * Los jugadores anónimos pueden ser campeones o no (alrededor de un 10% de campeones no tienen nombre). 
 * El jugador anónimo puede tener un historial de partidas.
 * Con las estructuras sucede algo parecido, un jugador anónimo puede destruir estructuras, y dichas estructuras pueden tener distinta vida, dependiendo de si tienen modificadores o no (alrededor de un 20% tiene  modificadores). Añadir el atributo modificadores cuando corresponda.
 
## Instrucciones de entrega
 
Las preguntas que se les solicita desarrollar vendrán dentro de un Jupyter Notebook, el cual tendrán que desarrollar a partir del Notebook que se adjunta.
 
## Instrucciones de entrega
 
Las preguntas que se les solicita desarrollar vendrán dentro de un Jupyter Notebook, el cual tendrán que desarrollar a partir del Notebook que se adjunta.
 
