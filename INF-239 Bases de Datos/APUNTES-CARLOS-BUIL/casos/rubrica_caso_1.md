# Rúbrica Caso 1: MongoDB

| Pregunta     | Respuesta bien       | Respuesta más o menos            | Respuesta mala |
|-----------------------------------------|-------------------------------------------------------|------------------------------------------------------|------------------------|
| Creación de colecciones (20 puntos)     | Crea las colecciones correctamente (100 puntos)       | No crea todas las colecciones (40 puntos)            | No crea nada (0 puntos) |
| Generación de datos (40 puntos)         | Inserta con las distribuciones adecuadas (100 puntos) | Inserta sin las distribuciones adecuadas (40 puntos) | No inserta (0 puntos)  |
| Consultas a las colecciones (20 puntos) | Las consultas son correctas y retornan los resultados correspondientes (100 puntos) | Algunas consultas son correctas (40 puntos) | No hay consultas (0 puntos) |

