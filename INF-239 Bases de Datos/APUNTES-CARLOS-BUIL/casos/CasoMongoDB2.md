# Caso 2: MongoDB y datos semi estructurados

## Introducción

Continuando con el proyecto de microblogueo la empresa SandBird, ha identificado varias consultas frecuentes que los usuarios realizan a su base de datos. Para ello, y usando los mismos datos que generaron con anterioridad, quiere asegurarse de que dichas consultas se reponden en un tiempo razonable. Esas consultas son consultas agregadas (consultas que acceden a un conjunto de datos y que calculan valores en el agregado de dichos datos). Para ello confían en MongoDB y en su sistema de [pipelines](https://www.mongodb.com/docs/manual/core/aggregation-pipeline/) para ejecutarlas. 

## Pipelines en MongoDB

MongoDB, al ser una base de datos NoSQL, no ofrece varias de las funcionalidades de las que proveen los sistemas SQL, pero a cambio tiene otras ventajas (por ejemplo, un modelo menos estructurado y sencillo de actualizar). Para ejecutar consultas agregadas lo que hace MongoDB es pedir al usuario que manualmente introduzca cómo se va a procesar una consulta (de forma imperativa). Un ejemplo de pipeline es el siguiente:

```python
pipeline = [
    { '$match': { 'region': "Valparaiso"}},
    { '$sort': { "poblacion": -1 } }]

```

En un array en Python se indican las operaciones que se van a realizar sobre las colecciones, en este caso una selección primero y después un ordenamiento, de forma secuencial. Para más información es posible consultar la referencia de [MongoDB sobre pipelines](https://www.mongodb.com/docs/manual/core/aggregation-pipeline/)

 
## Instrucciones de entrega
 
Las preguntas que se les solicita desarrollar vendrán dentro de un Jupyter Notebook, el cual tendrán que desarrollar a partir del Tutorial en el Notebook que se adjunta en el repo Git.
 
## Rúbrica Caso 2

| Pregunta     | Respuesta bien       | Respuesta más o menos            | Respuesta mala |
|-----------------------------------------|-------------------------------------------------------|------------------------------------------------------|------------------------|
| Consultas agregadas (50 puntos)     | Consultas retornan datos esperados, usando pipelines (100 puntos)       | Algunas consultas retornan datos esperados usando pipelines (20 puntos por consulta correcta)            | No hay consultas (0 puntos) |
| Consultas agregadas con optimización (40 puntos)         |  Consultas tienen optimziación y se explica el por qué de dicha optimización usando pipelines (100 puntos)       | Algunas están optimizadas con explicación usando pipelines (20 puntos por consulta correcta)            | No hay consultas (0 puntos) |
