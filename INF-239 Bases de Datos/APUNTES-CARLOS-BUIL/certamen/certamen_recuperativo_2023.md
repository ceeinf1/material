# Certamen Recuperativo Bases de Datos - S2-2023

## Recuperativo 1

## Parte 1: Conceptos en Verdaderos o Falsos con justificación 40%

Responda a las siguientes preguntas. Agregue una justificación suficiente en cada respuesta que no pase de 1 párrado (si no entrega justificación suficiente, tendrá 0 de 5 puntos). Se evalúa el uso de un buen vocabulario técnico

1. Indique los elementos principales del modelo de datos relacional.
2. Indique las diferencias entre el modelo Entidad-Relación (ER) y el modelo de documentos.
3. ¿Qué es un modelo lógico de datos?
4. ¿Qué es un modelo conceptual de datos?
5. ¿Qué es un modelo de datos semi estructurado?
6. Si quiero modelar una Wiki (como Wikidata), en la cual hay información altamente cambiante (se añaden y actualizan datos con nuevos atributos frecuentemente), ¿qué modelo utilizaría y por qué?
7. ¿Qué son las restricciones de integridad y en qué momento son útiles?
8. ¿En qué momento es útil particionar horizontalmente las tablas de una base de datos?


## Parte 2: Discord 60%

Se pide realizar un modelo de datos conceptual usando la notación Crow's Perch, identificando entidades con mínimo 4 atributos (incluida PK), asociaciones (si hay M:N mostrarla y luego descomponerla aparte) y el máximo de semántica posible, considerando la siguiente descripción: Como todos conocemos en Discord hay servidores, canales en los servidores, usuarios que están en servidores, miembros de los canales y que envían mensajes esos a canales, etc. Además, los usuarios pueden tener distintos roles dentro de un servidor. Los miembros de cada canal pueden estar conectados o desconectados.

![Discord 1](imagenes/discord1.png)

Además, los usuarios de Discord pueden tener amigos y mensajes privados

![Discord 2](imagenes/discord2.png)

## Recuperativo 2: Discord (100%)

### Vista 1

En la siguiente imagen tenemos una vista de discord. Como todos conocemos en Discord hay servidores, canales en los servidores, usuarios que están en servidores, miembros de los canales y que envían mensajes esos a canales, etc. Además, los usuarios pueden tener distintos roles dentro de un servidor. Los miembros de cada canal pueden estar conectados o desconectados.

![Discord 1](imagenes/discord1.png)


### Vista 2

Además, los usuarios de Discord pueden tener amigos y mensajes privados

![Discord 2](imagenes/discord2.png)

#### Se solicita, que haciendo los supuestos y mejoras que estimes pertinente, contestar:

 1. Normalizar las dos vistas a 3FN sin saltarse ningún paso. (Vista 1: 20%, Vista 2: 20%)
 2. Integrar el resultado de la normalización. 10%
 3. Graficar el modelo lógico usando la simbología que más te acomode (sólo poner PK y FK). 10%
 4. Una vez desarrollado el modelo lógico, explique se calculan los atributos almacenados en la tabla de usuarios, como amigos, cantidad de mensajes privados, etc. y las implicaciones que tiene en el modelo (Vista 1). 10%
 5. En la Vista 1, ¿qué índice sería más adecuado para obtener los detalles de los miembros de un canal y no de otro? 10%
 6. Considere que queremos obtener los mensajes de los usuarios de un canal, escriba la consulta SQL que realiza esa obtención de datos. 10%
 7. Considere que queremos obtener los mensajes de los amigos que tenemos en un mismo canal, escriba la consulta SQL que realiza esa obtención de datos. 10%

