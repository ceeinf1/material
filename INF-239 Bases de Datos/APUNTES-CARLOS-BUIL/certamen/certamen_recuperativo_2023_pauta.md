# Certamen Recuperativo Bases de Datos - S2-2023

## Recuperativo 1

## Parte 1: Conceptos en Verdaderos o Falsos con justificación 40%

Para cada frase indicar en casillero, si es V (Verdadero) o F (Falso). Agregue una justificación en ambos casos (si no entrega justificación, tendrá 0 de 5 puntos); para justificar los V se sugiere usar ejemplos o definiciones de conceptos; y para los F, dar argumentos sólidos. Se evalúa el uso de un buen vocabulario técnico

1. Indique los elementos principales del modelo de datos relacional.
  * Fundamento principal de las bases de datos. Representa datos en tablas con filas (tuplas) y columnas (atributos), donde cada fila tiene una clave única. Las relaciones entre tablas se establecen mediante claves primarias y foráneas.
2. Indique las diferencias entre el modelo Entidad-Relación (ER) y el modelo de documentos.
  * El modelo relacional representa datos en tablas con filas y columnas, donde cada fla tiene una clave única. El modelo de documentos almacena documentos, que pueden ser documentos json u otros, y son tratados como el elemento principal del modelo (first class citizen).
3. ¿Qué es un modelo lógico de datos?
  * Es una representación abstracta de los datos independiente del hardware o software utilizado. Se enfoca en la estructura de datos y sus relaciones.
4. ¿Qué es un modelo conceptual de datos?
  * Es una representación de alto nivel de los datos y sus relaciones, enfocado en el entendimiento de los usuarios. A menudo se usa en la etapa inicial de diseño de una base de datos para capturar requerimientos y conceptos clave.
5. ¿Qué es un modelo de datos semi estructurado?
  * Un modelo de datos semiestructurado es un enfoque que permite almacenar y organizar datos de manera flexible, sin requerir un esquema rígido o fijo como en los modelos de datos estructurados tradicionales, como los modelos relacionales. En este tipo de modelo, los datos no tienen que cumplir con un esquema predefinido y pueden ser almacenados de forma más flexible, lo que significa que pueden contener diferentes tipos de información y no tienen que seguir una estructura uniforme de tablas con filas y columnas, como en las bases de datos relacionales.
6. Si quiero modelar una Wiki (como Wikidata), en la cual hay información altamente cambiante (se añaden y actualizan datos con nuevos atributos frecuentemente), ¿qué modelo utilizaría y por qué?
  * Usaría un modelo de datos semi estructurado según la respuesta de la pregunta 6, en concreto lo ideal sería o un modelo de grafos para poder modelar de forma más efectiva las relaciones entre datos.
7. ¿Qué son las restricciones deegridad y en qué momento son útiles?
  * Las restricciones deegridad en el modelo relacional son reglas o condiciones que se aplican a los datos almacenados en una base de datos para garantizar su precisión, consistencia y validez. Estas restricciones se utilizan para mantener laegridad de los datos y prevenir operaciones que puedan resultar en inconsistencias o errores.
8. ¿En qué momento es útil particionar horizontalmente las tablas de una base de datos?
  * Al dividir una tabla grande en particiones más pequeñas, las consultas pueden ejecutarse más rápidamente ya que se accede a conjuntos de datos más pequeños. Esto es especialmente útil en sistemas donde se requiere acceder a datos frecuentemente.



## Parte 2: Discord

Se pide realizar un modelo de datos conceptual usando la notación Crow's Perch, identificando entidades con mínimo 4 atributos (incluida PK), asociaciones (si hay M:N mostrarla y luego descomponerla aparte) y el máximo de semántica posible, considerando la siguiente descripción: Como todos conocemos en Discord hay servidores, canales en los servidores, usuarios que están en servidores, miembros de los canales y que envían mensajes esos a canales, etc. Además, los usuarios pueden tener distintos roles dentro de un servidor. Los miembros de cada canal pueden estar conectados o desconectados. Además, los usuarios de Discord pueden tener amigos y mensajes privados.

![Discord 1](imagenes/discord1.png)

![Discord 2](imagenes/discord2.png)

### Modelo conceptual

![Discord 2](imagenes/recuperativo_2023_modelo_conceptual.drawio.png)

### Modelo conceptual descomposicion M:N

![Discord 2](imagenes/recuperativo_2023_modelo_logico.drawio.png)

## Recuperativo 2: Discord (100%)

### Vista 1

En la siguiente imagen tenemos una vista de discord. Como todos conocemos en Discord hay servidores, canales en los servidores, usuarios que están en servidores, miembros de los canales y que envían mensajes esos a canales, etc. Además, los usuarios pueden tener distintos roles dentro de un servidor. Los miembros de cada canal pueden estar conectados o desconectados.

![Discord 1](imagenes/discord1.png)


### Vista 2

Además, los usuarios de Discord pueden tener amigos y mensajes privados

![Discord 2](imagenes/discord2.png)

Se solicita, que haciendo los supuestos y mejoras que estimes pertinente, contestar:

 1. Normalizar las dos vistas a 3FN sin saltarse ningún paso. (Vista 1: 20%, Vista 2: 20%)
 2. Integrar el resultado de la normalización. 10%
 3. Graficar el modelo lógico usando la simbología que más te acomode (sólo poner PK y FK). 10%
 4. Una vez desarrollado el modelo lógico, explique se calculan los atributos almacenados en la tabla de usuarios, como amigos, cantidad de mensajes privados, etc. y las implicaciones que tiene en el modelo (Vista 1). 10%
 5. En la Vista 1, ¿qué índice sería más adecuado para obtener los detalles de los miembros de un canal y no de otro? 10%
 6. Considere que queremos obtener los mensajes de los usuarios de un canal, escriba la consulta SQL que realiza esa obtención de datos. 10%
 7. Considere que queremos obtener los mensajes de los amigos que tenemos en un mismo canal, escriba la consulta SQL que realiza esa obtención de datos. 10%



### Normalización Vista 1

VISTA 1: ({userID, username, email, fecha_creacion}, {serverID, server_name, fecha_creacion}, {channelID, channel_name}, {messageID, message_content, fecha_envio}, {roleID, role_name})

#### 1FN:

   - VISTA 1.1 Usuario (userID (PK), username, email, fecha_creacion, roleID, role_name)
   - VISTA 1.2 Servidor (serverID (PK), server_name, fecha_creacion)
   - VISTA 1.3 Canal (channelID (PK), channel_name, serverID (FK))
   - VISTA 1.4 Mensaje (messageID (PK), message_content, userID (FK), channelID (FK), fecha_envio, channelID)


#### 2FN:

   - VISTA 1.1.1 Usuario (userID (PK), username, email, fecha_creacion)
   - VISTA 1.1.2 Rol (roleID (PK), role_name);
   - VISTA 1.1.3 UsuarioRol (userID (PK), roleID (FK))
   - VISTA 1.3.1 Canal(channelID (PK), channel_name, serverID (FK))
   - VISTA 1.3.2 UsuarioCanal(userID, channelID, userID (FK), channelID (FK))

#### 3FN

- VISTA 1.1.1.1 UsuarioRol (userRoleID (PK), userID (PK FK), roleID (PK FK));
- VISTA 1.3.2.1 UsuarioCanal(userChannelID (PK), userID (FK), channelID (FK))

En las vistas anteriores hay un problema de dependencia transitiva ya que tanto en UsuarioRol como en UsuarioCanal al relacionar indirectamente a Usuario y Rol a través de las claves foráneas userID y roleID.


### Normalización Vista 2

#### 1FN
 - VISTA 2.1 Amigos (friendID (PK), user1ID, user2ID)
 - VISTA 2.2 MensajePrivado (pmID (PK), senderID, receiverID, message_content, fecha_envio)

#### 2FN
 - VISTA 2.1 Amigos (friendID (PK), user1ID, user2ID)
 - VISTA 2.1.2 UsuarioAmigo (friendID (PK), user1ID, user2ID)

Sin embargo, una posible formalización para la tabla "Amigos" podría ser la siguiente, separando los usuarios involucrados en la amistad en una tabla aparte.

#### Integración y dibujo (se puede importar en DBeaver)


```sql
-- Tabla Usuario
CREATE TABLE Usuario (
    userID PRIMARY KEY,
    username,
    email VARCHAR(100),
    fecha_creacion DATE
);

-- Tabla Servidor
CREATE TABLE Servidor (
    serverID PRIMARY KEY,
    server_name,
    fecha_creacion DATE
);

-- Tabla Canal
CREATE TABLE Canal (
    channelID PRIMARY KEY,
    channel_name,
    serverID,
    FOREIGN KEY (serverID) REFERENCES Servidor(serverID)
);

-- Tabla Mensaje
CREATE TABLE Mensaje (
    messageID PRIMARY KEY,
    message_content TEXT,
    userID,
    channelID,
    fecha_envio DATE,
    FOREIGN KEY (userID) REFERENCES Usuario(userID),
    FOREIGN KEY (channelID) REFERENCES Canal(channelID)
);

-- Tabla Rol
CREATE TABLE Rol (
    roleID PRIMARY KEY,
    role_name
);

-- Tabla UsuarioRol
CREATE TABLE UsuarioRol (
    userID,
    roleID,
    PRIMARY KEY (userID, roleID),
    FOREIGN KEY (userID) REFERENCES Usuario(userID),
    FOREIGN KEY (roleID) REFERENCES Rol(roleID)
);

-- Tabla ServidorRol
CREATE TABLE ServidorRol (
    serverID,
    roleID,
    PRIMARY KEY (serverID, roleID),
    FOREIGN KEY (serverID) REFERENCES Servidor(serverID),
    FOREIGN KEY (roleID) REFERENCES Rol(roleID)
);

-- Tabla UsuarioAmigo
CREATE TABLE UsuarioAmigo (
    friendID INT PRIMARY KEY,
    userID INT,
    friendUserID INT
);

CREATE TABLE MensajePrivado (
    pmID INT PRIMARY KEY,
    senderID INT,
    receiverID INT,
    message_content VARCHAR(255),
    fecha_envio DATE
);
```

 ### 4. Una vez desarrollado el modelo lógico, explique cómo se calculan los atributos almacenados en la tabla de usuarios, como amigos, cantidad de mensajes privados, etc. y las implicaciones que tiene en el modelo (Vista 1). 10%

Son tributos derivados, es parte de una desnormalización ya que no deberían estar pre calculados.

 ### 5. En la Vista 1, ¿qué índice sería más adecuado para obtener los detalles de los miembros de un canal y no de otro? 10%

Los detalles de los miembros son el nick y el id de usuario, por ejemplo. Entonces primero tenemos que acceder al ID del canal. Este índice se crea por defecto en la base de datos. El índice a colocar sería en los detalles de los miembros, y en este caso un hash sería lo mejor, ya que a partir del ID del canal sacaríamos directamente la lista de usuarios, sus IDs, y ahí podríamos usar un hash para acceder directamente a los detalles. De todas formas, ese índice ya se crea por defecto en la base de datos.

 ### 6. Considere que queremos obtener los mensajes de los usuarios de un canal, escriba la consulta SQL que realiza esa obtención de datos. 10%

```sql
SELECT Mensaje.messageID, Mensaje.message_content, Mensaje.fecha_envio, Usuario.username
FROM Mensaje
JOIN Usuario ON Mensaje.userID = Usuario.userID
WHERE Mensaje.channelID = 'general';
```

 ### 7. Considere que queremos obtener los mensajes de los amigos que tenemos en un mismo canal, escriba la consulta SQL que realiza esa obtención de datos. 10%

 ```sql
 SELECT Usuario.username AS friend_username
FROM Usuario
JOIN UsuarioCanal AS UC1 ON Usuario.userID = UC1.userID
JOIN UsuarioCanal AS UC2 ON UC1.channelID = UC2.channelID
JOIN Amistad ON (Amistad.user1ID = UC1.userID AND Amistad.user2ID = UC2.userID) OR (Amistad.user1ID = UC2.userID AND Amistad.user2ID = UC1.userID)
WHERE UC1.userID = 'your_user_id' AND UC1.channelID = 'your_channel_id';
```