# Certamen 1

## Parte 1: Conceptos en Verdaderos o Falsos con justificación 40%

Para cada frase indicar en casillero, si es V (Verdadero) o F (Falso). Agregue una justificación en ambos casos (si no entrega justificación, tendrá 0 de 5 puntos); para justificar los V se sugiere usar ejemplos o definiciones de conceptos; y para los F, dar argumentos sólidos. Se evalúa el uso de un buen vocabulario técnico

1. Los niveles operacionales a los que apoya un sistema de base de datos son los niveles estratégico y operacional. 
2. El diccionario es un componente de la base de datos.
3. Las relaciones en una base de datos relacional son elementos fundamentales en una BD. 
4. El modelo de grafos es el modelo de datos más estructurado. 
5. Restricciones de integridad son aquellas que permiten borrar datos de una tabla sin verificar si existe alguna referencia a los datos borrados.
6. Una relación muchos a muchos no requiere de ninguna traducción a un modelo de datos relacional. 
7. Una base de datos con partición vertical es más eficiente cuando se seleccionan un subconjunto de columnas de las tablas.
8. Los modelos de datos conceptuales son independientes de los sistemas de gestión de base de datos.

## Parte 2: modelado conceptual de dominios

### Los elfos domésticos

Se pide realizar un modelo de datos conceptual usando la notación Crow's Perch, identificando entidades con mínimo 4 atributos (incluida PK), asociaciones (si hay M:N mostrarla y luego descomponerla aparte) y el máximo de semántica posible, considerando la siguiente descripción:

Los elfos domésticos son una parte importante de todo castillo mágico, sin embargo gestionarlos es una tarea muy complicada. Primero, los elfos domésticos se encargan de la cocina, donde cocinan multitud de recetas. Esas recetas tienen múltiples ingredientes, y consideran que puede haber comensales vegetarianos estrictos, ovo-lacto vegetarianos, personas con distintas alergias y magos que comen de todo. Por cada receta los elfos guardan (el título, el ingrediente principal como carne, pollo, huevo, vegetales). De cada elfo hay que guardar el nombre, el poder mágico especial que poseen, y las recetas que son capaces de cocinar. Como todo el mundo sabe, los elfos domésticos también tienen RUT, además de nombre, apellido y fecha de nacimiento. Como todo en el mundo mágico, los platos también se pueden aparecer. Es decir, es posible encargar a otros elfos domésticos de otras cocinas que envíen mediante aparición los platos cocinados. Eso sí, los elfos domésticos sólo trabajan en una cocina (de un castillo). Es importante notar que los elfos tienen que saber identificar de dónde viene cada plato, es decir, saber que proviene de una cocina determinada identificando al dueño de la cocina, cuántos elfos domésticos trabajan en esa cocina y la dirección de la misma.


### Explique el siguiente modelo conceptual

En el siguiente diagrama de clases: 

![El certamen](./imagenes/el_certamen.png)

2. Convierta el modelo a tablas relacionales.
1. el Explique concepto de restricción de integridad y en qué afecta a la relación entre las clases Asignatura y Certamen.
2. Explique posibles mejoras al modelo para poder acelerar el procesamiento de algunas consultas (nota promedio del certamen por asignatura y nota de los estudiantes en la asignatura)
3. Explique los problemas introducidos al mejorar la eficiencia de esas consultas.

### Las cervezas

Hoy en día la popularidad de las cervezas es muy grande, casi tanto como el vino! Hay cervezas de distintas destilerías, como Volcanes, Kross, u otras muchas, de distintas localidades. Las cervezas que fabrican se promocionas de distintas formas, por ejemplo indicando que tienen agua de glaciares, de termas, de la llave, etc. O que los ingredientes son lúpulo, trigo, sin gluten, u otros. Además, hay múltiples variedades, como por ejemplo, lager, negra, strong lager, IPA, y a su vez tienen triple hop, estar filtradas o no.

Dibuje el modelo conceptual, indicando la aridad de las relaciones, su primary key, y dos atributos de cada clase.

