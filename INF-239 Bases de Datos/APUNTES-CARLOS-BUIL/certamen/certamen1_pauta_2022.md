# Certamen 1

## Parte 1: Conceptos en Verdaderos o Falsos con justificación 40%

Para cada frase indicar en casillero, si es V (Verdadero) o F (Falso). Agregue una justificación en ambos casos (si no entrega justificación, tendrá 0 de 5 puntos); para justificar los V se sugiere usar ejemplos o definiciones de conceptos; y para los F, dar argumentos sólidos. Se evalúa el uso de un buen vocabulario técnico

1. Los niveles operacionales a los que apoya un sistema de base de datos son los niveles estratégico y operacional. **Falso** También apoya al nivel táctico a través de consultas agregadas de los datos operacionales (conteos de conjuntos de datos en la BD operacional).
2. El diccionario es un componente de la base de datos. **Cierto**, es un componente que almacena los metadatos de la base de datos (información acerca de los datos) y estos incluyen el tipo de dato de cada tabla, el largo, el propietario, y estadísticas acerca de los mismos. 
3. Las relaciones en una base de datos relacional son elementos fundamentales en una BD. **Cierto**, ya que esas relaciones son tablas bi dimensionales que no permiten elementos repetidos pero sí desordenados, y cada columna tiene un valor simple
4. El modelo de grafos es el modelo de datos más estructurado. **Falso**, el más estructurado (que hemos visto) es el relacional ya que el modelo de datos de grafo no tiene un esquema fijo.
5. Restricciones de integridad son aquellas que permiten borrar datos de una tabla sin verificar si existe alguna referencia a los datos borrados. **Falso**, las restricciones de integridad permiten el borrado verificando si existe algún dato que dependa del dato que se está borrando.
6. Una relación muchos a muchos no requiere de ninguna traducción a un modelo de datos relacional. **Falso**, precisamente se requiere dicha traducción para poder modelar en el modelo relacional la relación entre las dos tablas.
7. Una base de datos con partición vertical es más eficiente cuando se seleccionan un subconjunto de columnas de las tablas. **Cierto**, ya que al seleccionar un subconjunto de los datos todo el tiempo se transfieren menos datos entre los operadores de la base de datos y la ejecución de la consulta es más eficiente.
8. Los modelos de datos conceptuales son independientes de los sistemas de gestión de base de datos. **Cierto** ya que al modear conceptualmente un dominio es posible abstraerse del SGBD sobre el que se implementará el modelo conceptual.

## Parte 2: modelado conceptual de dominios

### Los elfos domésticos

Se pide realizar un modelo de datos conceptual usando la notación Crow's Perch, identificando entidades con mínimo 4 atributos (incluida PK), asociaciones (si hay M:N mostrarla y luego descomponerla aparte) y el máximo de semántica posible, considerando la siguiente descripción:

Los elfos domésticos son una parte importante de todo castillo mágico, sin embargo gestionarlos es una tarea muy complicada. Primero, los elfos domésticos se encargan de la cocina, donde cocinan multitud de recetas. Esas recetas tienen múltiples ingredientes, y consideran que puede haber comensales vegetarianos estrictos, ovo-lacto vegetarianos, personas con distintas alergias y magos que comen de todo. Por cada receta los elfos guardan (el título, el ingrediente principal como carne, pollo, huevo, vegetales). De cada elfo hay que guardar el nombre, el poder mágico especial que poseen, y las recetas que son capaces de cocinar. Como todo el mundo sabe, los elfos domésticos también tienen RUT, además de nombre, apellido y fecha de nacimiento. Como todo en el mundo mágico, los platos también se pueden aparecer. Es decir, es posible encargar a otros elfos domésticos de otras cocinas que envíen mediante aparición los platos cocinados. Eso sí, los elfos domésticos sólo trabajan en una cocina (de un castillo). Es importante notar que los elfos tienen que saber identificar de dónde viene cada plato, es decir, saber que proviene de una cocina determinada identificando al dueño de la cocina, cuántos elfos domésticos trabajan en esa cocina y la dirección de la misma.

![Modelo conceptual pregunta los elfos](imagenes/P1-ModeloConceptual.png)

![Modelo relacional pregunta los elfos](imagenes/P1-ModeloRelacional.png)

### Explique el siguiente modelo conceptual

En el siguiente diagrama de clases: 

![El certamen](imagenes/el_certamen.png)

1. Convierta el modelo a tablas relacionales.
2. el Explique concepto de restricción de integridad y en qué afecta a la relación entre las clases Asignatura y Certamen.
    * Las restricciones de integridad son aquellas que permiten asegurar la integridad de los datos cuando se modifica alguna tupla de una tabla, y dicha tupla es referenciada por tuplas de otra tabla relacionada. En el caso de Asignatura y Certamen, si se borra la asignatura hay que hacer algo con los certamenes de la asignatura, ya que quedarían sin la relación de qué asignatura los ha generado.
3. Explique posibles mejoras al modelo para poder acelerar el procesamiento de algunas consultas (nota promedio del certamen por asignatura y nota de los estudiantes en la asignatura).
    * Una mejora sería para calcular no tener que calcular el promedio de notas de un certamen sería añadir un atributo a la clase certamen tuviese precalculado dicho promedio. La solución es la misma para la nota de los estudiantes de la asignatura.
4. Explique los problemas introducidos al mejorar la eficiencia de esas consultas.
    * El problema es que no corresponde esa nota donde la hemos puesto, ya que es un atributo que se calcula accediendo a cada certamen, pero para acelerar el proceso lo colocamos como extensión de la clase certamen.

![Modelo relacional pregunta certamen](imagenes/P2-ModeloRelacional.png)

### Las cervezas

Hoy en día la popularidad de las cervezas es muy grande, casi tanto como el vino! Hay cervezas de distintas destilerías, como Volcanes, Kross, u otras muchas, de distintas localidades. Las cervezas que fabrican se promocionas de distintas formas, por ejemplo indicando que tienen agua de glaciares, de termas, de la llave, etc. O que los ingredientes son lúpulo, trigo, sin gluten, u otros. Además, hay múltiples variedades, como por ejemplo, lager, negra, strong lager, IPA, y a su vez tienen triple hop, estar filtradas o no.

Dibuje el modelo conceptual, indicando la aridad de las relaciones, su primary key, y dos atributos de cada clase.

![Modelo relacional pregunta Cervezas](imagenes/P3-ModeloRelacional.png)

