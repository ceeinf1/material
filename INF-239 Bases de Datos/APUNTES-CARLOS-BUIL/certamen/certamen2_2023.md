# Certamen 2 Bases de Datos - S2-2023


## PREGUNTA 1: Git, otra vez (60%)

Como todos los estudiantes saben ya, Git es un software de control de versiones colaborativo diseñado por Linus Torvalds. Los conceptos principales de Git ya se discutieron y modelaron de forma conceptual en el primer certamen y ahora modelaremos de forma lógica. Para ello nos basaremos en las siguientes vistas:

VISTA 1: directorio principal. Hay que modelar archivos y directorios (aunque todos se tratan como archivos), ramas (devel, main, etc.), tags (mysql, java, gui, etc.), estrellas, número de ramas, atributos (licencia, actividad, política de seguridad) y los commit.

![Vista Repositorio](imagenes/github1.png)


#### VISTA 2: discusiones. 

En esta vista se muestran las discusiones que hay en el repositorio acerca de varios problemas que surgen de las funcionalidades del software. Hay que modelar la lista de discusiones, que está en un repositorio, los mensajes y las respuestas a los  mensajes.

![Discusiones Github](imagenes/github2.png)

VISTA 3: la Wiki de Github. Los proyectos en Github suelen tener una wiki que describe el proyecto, instrucciones de instalación y de ejecución del software. Es una Wiki normal donde los usuarios "Commiters" escriben instrucciones de uso.

![Github Wiki](imagenes/github_wiki.png)

Se solicita, que haciendo los supuestos y mejoras que estimes pertinente, contestar:

 1. Normalizar las 3 vistas a 3FN sin saltarse ningún paso. (Vista1: 15%, Vista2: 15%, Vista3: 10%)
 2. Integrar el resultado de la normalización. 5%
 3. Graficar el modelo lógico usando la simbología que más te acomode (sólo poner PK y FK). 5%
 4. Una vez desarrollado el modelo lógico, explique se calculan los atributos almacenados en la tabla del repositorio (estrellas, ramas, etc.) y las implicaciones que tiene en el modelo (Vista 1). 5%
 5. En la Vista 1, si la consulta que obtiene los commits de un usuario tardase mucho en ejecutarse, ¿cómo la optimizaría?. 5%

## PREGUNTA 2: Seguimos con Git (40%)

Aplicar la metodología top-down **para obtener un modelo de datos lógico** a partir del siguiente modelo conceptual:

![Git modelo conceptual](imagenes/certamen1_2023_conceptual.png)

1. Detallar los siguientes elementos (80%):
    1. Entidades fuertes.
    2. Entidades débiles.
    3. Asociaciones 1:1
    4. Asociaciones 1:N
    5. Asociaciones M:N
    6. Asociaciones n-arias
    7. Herencia
    8. Herencia selectiva

2. Graficar el modelo lógico (20%)