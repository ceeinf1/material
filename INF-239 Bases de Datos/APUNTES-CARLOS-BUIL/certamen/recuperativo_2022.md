# Certamen Recuperativo Bases de Datos S2 - 2022

## Certamen 1

### Certamen 1 - Parte 1

1. ¿Cuáles son los niveles necesarios para poder describir los datos?
- [ ] Realidad, Diccionario de datos, Bases de datos
- [ ] Realidad y Bases de datos
- [ ] Bases de datos y diccionario
- [ ] Realidad, bases de datos, y modelo de datos
- [ ] Ninguna de las anteriores

2. ¿Cuáles son los componentes de la Realidad? la cual comprende el mundo real (sistema, organización), con sus
componentes y el medio ambiente en el cual opera.
- [ ] Entidades, atributos, listas de elementos
- [ ] Entidades, atributos, clases
- [ ] Entidades y clases
- [ ] Entidades y atributos
- [ ] Ninguna de las anteriores

3. Las clases (tipos de entidades) son:
- [ ] Personas, objeto (cosa, concepto), lugar o evento, sobre el cual la organización decide coleccionar y almacenar datos. 
- [ ] Un conjunto de entidades que poseen características similares, por ejemplo Clientes, Estudiantes, Pacientes
- [ ] Una propiedad de una entidad que se desea registrar. Para cada tipo de entidades, existe un conjunto de atributos de interés para la organización
- [ ] La descripción de los atributos que forman la base de datos
- [ ] Ninguna de las anteriores

4. ¿Qué es un registro en una base de datos?
- [ ] Una asociación entre tipos de entidades de una realidad
- [ ] Una asociación entre tipos de entidades de una realidad
- [ ] Un conjunto de ítems de datos que corresponde a la definición de una clase de entidades.
- [ ] Un conjunto de ítems de datos y/o datos agregados, y corresponde a la definición de una clase de entidades.
- [ ] Ninguna de las anteriores

5. Indique cuál de las siguientes opciones no es un modelos de datos
- [ ] Relacional
- [ ] Grafos
- [ ] Árboles
- [ ] XML
- [ ] Objetos **revisar**

6. Semántica de los datos: la cardinalidad dentro de un modelo de datos se refiere:
- [ ] a cuando es obligatorio indicar el número de relaciones presentes en la relación
- [ ] a cuando es obligatorio indicar el número de atributos presentes en la relación 
- [ ] a cuando una entidad X se relaciona con varias entidades, bajo determinada asociación
- [ ] a cuando una entidad X se relaciona con varias entidades, bajo determinada asociación, pero cada una de éstas sólo lo hace con X.
- [ ] al número de entidades con que otra entidad se relaciona

7. Los posibles grados dentro de las asociaciones en un modelo de datos relacional son:
- [ ] Unarias 
- [ ] Binarias
- [ ] Binarias, ternarias, n-arias
- [ ] Binarias y ternarias 
- [ ] Todas las anteriores

8. El concepto de integridad referencial se refiere a:
- [ ] Se refiere al hecho de que la ocurrencia de una entidad no depende de la presencia de otra entidad.
- [ ] Se refiere a que si borramos la PK de una entidad no es necesario actualizar nada de otras entidades.
- [ ] Se refiere a la relación n-aria entre dos entidades.
- [ ] Se refiere a que una entidad no puede existir si el que le da sentido es eliminado.
- [ ] Se refiere a que un atributo no puede existir sin su clase.

### Parte 2 - Certamen 1

Estamos cerca de las Fiestas de Navidad y todas las tiendas online de videojuegos preparan sus rebajas. Para poder comprar juegos, los clientes tienen que estar registrados en la tienda, con nombre, fecha de nacimiento, RUT y email. Los distribuidores seleccionan los juegos participan en las ofertas de Navidad, juegos que pueden haber participado en otras campañas de ofertas (como por ejemplo la de verano). Por supuesto, hay juegos que tienen una oferta especial si se compra un combo, como por ejemplo un juego + expansión puede tener un descuento extra.

Diseñar un modelo con simbología Crow's Perch (se descontará si se usa otra simbología) que represente esta realidad (poner mínimo 4 atributos por entidad, identificar PK y además se debe identificar el máximo de semántica posible).

## Certamen 2
