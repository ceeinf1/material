# Certamen 1

## Parte 1: Conceptos en Verdaderos o Falsos con justificación 40%

Para cada frase indicar en casillero, si es V (Verdadero) o F (Falso). Agregue una justificación en ambos casos (si no entrega justificación, tendrá 0 de 5 puntos); para justificar los V se sugiere usar ejemplos o definiciones de conceptos; y para los F, dar argumentos sólidos. Se evalúa el uso de un buen vocabulario técnico

1. En una base de datos relacional las tuplas son consideradas como la unidad fundamental de dato. Es decir, una tupla es un dato y una base de datos almacena conjuntos de datos.
  * **Cierto**. Una tupla es un conjunto ordenado de campos donde cada campo almacena un tipo específico de dato. 
2. Las tuplas en una relación de una base de datos son únicas.
  * **Cierto**. Las tuplas tienen una Clave Primaria, una restricción que hace que cada elemento distinguible del resto. De esta forma se garantiza que cada tupla es identificada de forma única y es posible extraerla de la BD.
3. Las cardinalidades en un modelo de base de datos son 1:N, 1:1, M:N y N:1.
  * **Cierto**. El término cardinalidad se refiere al número de relaciones entre las tablas y al número de tuplas que pueden ser asociadas entre sí. Cardinalidad describe las cantidades de esas relaciones pudiendo ser 1:N, 1:1, M:N y N:1.
4. La integridad referencial en una base de datos se verifica a través de reglas en el sistema de base de datos.
  * **Cierto**. Para poder verificar esa integridad (por ejemplo claves foráneas válidas) se utilizan reglas predefinidas en el sistema.
5. Las Primary Keys o Claves Primarias son una forma de asegurar la integridad de los datos.
  * **Cierto**. Ese es el objetivo de dichas claves, ya garantizan que los datos dentro de cada tabla sean únicos.
6. Estos modelos de datos están ordenados de más estructurado a menos estructurado: texto, árbol, grafo y relacional.
  * **Falso**, es justo al contrario, están ordenados de menos estructurado a más estructurado.
7. Una base de datos con partición vertical es más eficiente cuando se seleccionan habitualmenmte un subconjunto de columnas de las relaciones de la base de datos.
  * **Cierto**, ya que al seleccionar menos datos se transfieren menos datos desde el disco para su procesamiento.
8. Los modelos de datos conceptuales son independientes de los sistemas de gestión de base de datos.
  * **Cierto**. Los modelos se diseñan y luego se traducen al modelo lógico el cual es almacenado en el sistema de bases de datos elegidos.

## Parte 2: modelado conceptual de dominios

### Git

Se pide realizar un modelo de datos conceptual usando la notación Crow's Perch, identificando entidades con mínimo 4 atributos (incluida PK), asociaciones (si hay M:N mostrarla y luego descomponerla aparte) y el máximo de semántica posible, considerando la siguiente descripción:

Git es un software de control de versiones diseñado por Linus Torvalds, pensando en la eficiencia, la confiabilidad y compatibilidad del mantenimiento de versiones de aplicaciones cuando estas tienen un gran número de archivos de código fuente. El concepto principal de Git es el de repositorio, en el cual se almacenan los archivos de los usuarios que pertenecen a un proyecto. Un Commit en el repositorio representa una versión específica de un proyecto en un momento, y captura los cambios hechos a archivos en el repositorio. Un repositorio puede contener muchos commits y un commit pertenece sólo a un repositorio. Una rama del repositorio representa una rama del desarrollo en el repositorio (por ejemplo, una rama puede ser de desarrollo y otra la de una versión del software) y de forma similar a los commits, un repositorio puede tener muchas ramas y cada rama pertece sólo a un repositorio.  Un usuario puede realizar múltiples commits afectando a múltiples archivos y un archivo puede tener asociados múltiples commits. Además, los commit pueden tener asociadas etiquetas para poder describirlos y los usuarios pueden tener múltiples repositorios.

#### Modelo Conceptual

![Modelo Conceptual](imagenes/certamen1_2023_conceptual.png)

#### Modelo Relacional

![Certamen](imagenes/certamen1_2023_git.png)

```sql
-- Repository table
CREATE TABLE Repository (
    RepositoryID INT PRIMARY KEY,
    Name VARCHAR(255) NOT NULL,
    Description TEXT,
    CreationDate timestamp
);

-- Commit table
CREATE TABLE Commit (
    CommitID INT PRIMARY KEY,
    SHA VARCHAR(40) NOT NULL, 
    Author VARCHAR(255) NOT NULL,
    Committer VARCHAR(255) NOT NULL,
    Timestamp timestamp NOT NULL,
    Message TEXT,
    RepositoryID INT,
    FOREIGN KEY (RepositoryID) REFERENCES Repository(RepositoryID)
);

-- Branch table
CREATE TABLE Branch (
    BranchID INT PRIMARY KEY,
    Name VARCHAR(255) NOT NULL,
    CreatedBy VARCHAR(255) NOT NULL,
    CreatedDate timestamp NOT NULL,
    RepositoryID INT,
    FOREIGN KEY (RepositoryID) REFERENCES Repository(RepositoryID)
);

-- User table
CREATE TABLE GitUser (
    UserID INT PRIMARY KEY,
    Username VARCHAR(255) NOT NULL,
    Name VARCHAR(255),
    Email VARCHAR(255)
);

-- File table
CREATE TABLE File (
    FileID INT PRIMARY KEY,
    Path VARCHAR(255) NOT NULL,
    Content TEXT,
    Size INT,
    RepositoryID INT,
    FOREIGN KEY (RepositoryID) REFERENCES Repository(RepositoryID)
);

-- CommitFile table para representar la relación muchos a muchos entre commit y archivo
CREATE TABLE CommitFile (
    CommitID INT,
    FileID INT,
    PRIMARY KEY (CommitID, FileID),
    FOREIGN KEY (CommitID) REFERENCES Commit(CommitID),
    FOREIGN KEY (FileID) REFERENCES File(FileID)
);
```
