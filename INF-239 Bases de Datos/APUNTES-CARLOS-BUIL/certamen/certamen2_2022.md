# Certamen 2 Bases de Datos - S2-2022


### PREGUNTA 1: Las competencias de programación (60%)

Las competencias de programación son eventos en los que una organización (en este caso Codeforces) propone un conjunto de problemas que han de ser resueltos en un tiempo máximo (típicamente 5 horas). Estos problemas se resuelven utilizando distintas técnicas en combinación con estructuras de datos adecuadas para los problemas. En Codeforces quieren reorganizar la base de datos y les pide que les ayuden.

VISTA 1: los competidores. Los competidores pueden ser de varios países, perteneces a una organización como máximo y a un equipo como máximo. Pueden tener varios títulos, como maestro, gran maestro, o aprendiz. También tienen un rating que depende de la cantidad y la dificultad de los problemas que hayan resuelto durante las competencias.

![competidor](imagenes/competidor.png)

VISTA 2: los problemas. Los problemas de programación están clasificados según su dificultad, la cantidad de competidores que los han resuelto y las estructuras de datos que se pueden utilizar para resolverse. 

![problema1](imagenes/problema1.png)

![problema2](imagenes/problema2.png)


VISTA 3: los envíos. Los problemas se evalúan en un servidor que ejecuta el código de la solución del problema, que puede ser en varios lenguajes de programación. Cada envío tarda entre 0,1 y varios segundos en ejecutarse, además de usar distinta memoria según el código.

![problema3](imagenes/problema3.png)

Se solicita, que haciendo los supuestos y mejoras que estimes pertinente, contestar:

 1. Normalizar las 3 vistas a 3FN sin saltarse ningún paso. (Vista1: 15%, Vista2: 15%, Vista3: 10%)
 2. Integrar el resultado de la normalización. 5%
 3. Graficar el modelo lógico usando la simbología que más te acomode (sólo poner PK y FK). 5%
 4. Para una de las vistas explicar cómo ayudaría una organización de archivo ordenado. 5%
 5. Explicar para cualquiera de las tablas resultantes el aporte que haría un índice del tipo "B Tree". 5%

### PREGUNTA2: Las cervezas (40%)
Aplicar la metodología top-down **para obtener un modelo de datos lógico** a partir del siguiente modelo conceptual:

![Las cervezas](imagenes/certamen2_2.png)

1. Entidades fuertes.
2. Entidades débiles.
3. Asociaciones 1:1
4. Asociaciones 1:N
5. Asociaciones M:N
6. Asociaciones n-arias
7. Herencia
8. Herencia selectiva


## Pauta

### PREGUNTA 1: las competencias de programación.

#### Normalización 

**VISTA1** (id-comp, nom-comp, rating-comp, contribution-comp, blog-comp{blog-entries, id-blog-post}, friends-comp, titulo-comp{titulo-id, tit-name}, direccion-comp{id-direccion, nom-ciudad, nom-pais, nom-organización}, comments-comp)

**Mejoras y supuestos**: Se agrega id-comp, id-blog-post, id-direccion, titulo-id, suponemos que no sabemos quiénes son los amigos del competidor, sólo cuántos son. 

**1FN**

 * Identificamos los grupos repetitivos (atributos no atómicos), y/o qué atributos determina la clave principal (PK)
 * VISTA1.1 (**id-comp**, nom-comp, nick-comp, rating-comp, contribution-comp, num-friends-comp, titulo-comp, comments-comp, blog-post)
 * VISTA1.2 (**id-direccion**, direccion-comp, nom-ciudad, nom-pais, nom-organización)
 * VISTA1.3 (**id-titulo**, nombre-titulo, rating-titulo)
 * VISTA1.4 (**id-blog-post**, blog-entry)

**2FN**

Eliminar dependencias parciales

 * VISTA1.1 (**id-comp**, nom-comp, nick-comp, rating-comp, contribution-comp, num-friends-comp, titulo-comp, comments-comp, id-blog, id-direccion)
 * VISTA1.2 (**id-direccion**, direccion-comp, nom-ciudad, nom-pais, nom-organización)
 * VISTA1.3 (**id-titulo**, nombre-titulo, rating-titulo)
 * VISTA1.3.1 (**id-titulo**, id-comp)
 * VISTA1.4 (**id-blog-post**, blog-entry) 
 * VISTA1.4.1 (**id-blog**, id-comp) (esto es opcional, no cuenta para la nota)
 * VISTA1.4.2 (**id-blog-post**, blog-entry, id-blog) (esto es opcional, no cuenta para la nota)

**3FN**

No hay

**VISTA2** (id-prob, titulo-prob, time-limit, mem-limit, input, output, enunciado-prob, estructuras-prob{estructura-id, nombre-estructura}, rating-prob, soluciones-prob{id-sol})

**Mejoras y supuestos** Se agrega id-prob, id-sol.

**1FN**

Identificamos qué atributos determina la clave principal (PK)

 * VISTA2.1 (**id-prob**, titulo-prob, time-limit, mem-limit, input, output, enunciado-prob, rating-prob, prob-sol, _id-estructrura_)
 * VISTA2.2 (**id-estructrura**, nombre-estructura, sol-prob)
 * VISTA2.3 (**id-sol**, coddigo-sol)


**Mejoras y supuestos ** añadidmos id-estructrura

**2FN**

Eliminar dependencias parciales

Supuesto: añadimos estructura-descripcion, id-sol

 * VISTA2.1 (**id-prob**, titulo-prob, time-limit, mem-limit, input, output, enunciado-prob, rating-prob)
 * VISTA2.2 (**estructura-id**, estructura-nombre, estructura-descripcion)
 * VISTA2.3 (**id-sol**, sol-prob)
 * VISTA2.4 (**id-prob**, **estructruras-prob**, descripcion)

**3FN**

No hay

 * VISTA3(envio-id, fecha-envio, id-comp, id-problema, compilador-envio, veredicto-envio, time-envio, memoria-envio)

**1FN**

 * VISTA3.1(**envio-id**, fecha-envio, **id-comp**, **id-problema**, compilador-envio, veredicto-envio, time-envio, memoria-envio)

**2FN**

 * VISTA3.1(**envio-id**, fecha-envio, **id-comp**, **id-problema**, compilador-envio, veredicto-envio, time-envio, memoria-envio)

**3FN**

 * VISTA3.1(**envio-id**, fecha-envio, **id-comp**, **id-problema**, compilador-envio, veredicto-envio, time-envio, memoria-envio)


#### Para una de las vistas explicar cómo ayudaría una organización de archivo ordenado. 5%

Al acceder a los problemas ordenados por fecha en la vista 2 es posible acceder de forma rápida a los envíos realizados, ya que podrían estar ordenados por fecha de envío.

#### Explicar para cualquiera de las tablas resultantes el aporte que haría un índice del tipo "B Tree". 5%

Al usar un B-Tree es posible acceder a los problemas con un rating determinado (por ejemplo entre 1500 y 2000), ya que el árbol permite el acceso rápido a los problemas de un rating específico, y todos deberían estar enlazados en las hojas del árbol.

#### Integración y mejoras

 * Competidor(**comp-id**, comp-nom, comp-nick, comp-rating, _direccion-id_, _blog-id_)
 * Blog(_blog-id_, blog-nombre) (es posible dejar que un competidor tenga muchos blogs si la relación 1:1 de en medio ya que no estaba directamente especificado en el enunciado ni en la imagen)
 * BlogPost(**post-id**, post-titulo, post-contenido, _blog-id_)
 * Direccion(**direccion-id**, nom-pais, nom-ciudad, nom-institucion)
 * TituloCompetidor(**titulo-id**, titulo-nombre, titulo-descripcion, _comp-id_)
 * Envio(envio-id, compilador-envio, tiempo-envio, memoria-envio, fecha-envio, _comp-id_, _problema-id_)
 * Problema(**problema-id**, problema-nombre, problema-descripcion, problema-rating, problema-restriccion-mem, problema-restriccion-tiempo, problema-dificultad)
 * Estructuras(**estructura-id**, estructrura-nombre, estructrura-descripcion, _problema-id_)

#### Graficar 

![Pregunta 2](imagenes/certamen2_1.png)


### Pregunta 2
* Entidades fuertes: Destilería
* Entidades débiles: Lager, IPA, Características
* Asociaciones 1:1: No hay
* Asociaciones 1:N: Destileria(id-destileria, id-cerveza)
* Asociaciones M:N: Características-Cerveza (No hay también estaría correcto debido a la explicación de más abajo)
* Asociaciones n-arias: no hay
* Herencia: hay de cerveza a sus tipos, se escoge la alternativa 1, usando una tabla para cada tipo de cerveza con FK a la clase Cerveza en cada uno de los tipos.
 - Quedaría Cerveza(**id-cerveza**...), Lager(**id-lager**, _id-cerveza_), IPA(**id-IPA**, _id-cerveza_)

#### Modelo final

 - Cerveza(**cerveza-id**, cerveza-nombre, cerveza-botella, cerveza-forma)
 - Características(**caracteristicas-id**, caracteristicas-color, caracteristicas-grados)
 - Características-cerveza(**cerveza-id**, **caracteristicas-id**, caracteristica-cerveza)
 - Destileria(**id-destileria**, destileria-nombre, destileria-direccion, destileria-gerente) 
 - IPA(**IPA-id**, ipa-fermentacion, ipa-agua, _cerveza-id_)
 - Lager(**lager-id**, lager-color, lager-agua, _cerveza--id_)

Debido a que el modelo de la prueba estaba incorrecto (aunque rectifiqué en clase), el siguiente modelo también está correcto:

 - Cerveza(**cerveza-id**, cerveza-nombre, cerveza-botella, cerveza-forma)
 - Características(**caracteristicas-id**, caracteristicas-color, caracteristicas-grados)
 - Destileria(**id-destileria**, destileria-nombre, destileria-direccion, destileria-gerente) 
 - IPA(**IPA-id**, ipa-fermentacion, ipa-agua, _cerveza-id_)
 - Lager(**lager-id**, lager-color, lager-agua, _cerveza--id_)
 
