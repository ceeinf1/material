# Certamen 2 Bases de Datos - S2-2023


## PREGUNTA 1: Git, otra vez (60%)

Como todos los estudiantes saben ya, Git es un software de control de versiones colaborativo diseñado por Linus Torvalds. Los conceptos principales de Git ya se discutieron y modelaron de forma conceptual en el primer certamen y ahora modelaremos de forma lógica. Para ello nos basaremos en las siguientes vistas:

VISTA 1: directorio principal. Hay que modelar archivos y directorios (aunque todos se tratan como archivos), ramas (devel, main, etc.), tags (mysql, java, gui, etc.), estrellas, número de ramas, atributos (licencia, actividad, política de seguridad) y los commit.

![Vista Repositorio](imagenes/github1.png)


#### VISTA 2: discusiones. 

En esta vista se muestran las discusiones que hay en el repositorio acerca de varios problemas que surgen de las funcionalidades del software. Hay que modelar la lista de discusiones, que está en un repositorio, los mensajes y las respuestas a los  mensajes.

![Discusiones Github](imagenes/github2.png)

VISTA 3: la Wiki de Github. Los proyectos en Github suelen tener una wiki que describe el proyecto, instrucciones de instalación y de ejecución del software. Es una Wiki normal donde los usuarios "Commiters" escriben instrucciones de uso.

![Github Wiki](imagenes/github_wiki.png)

Se solicita, que haciendo los supuestos y mejoras que estimes pertinente, contestar:

 1. Normalizar las 3 vistas a 3FN sin saltarse ningún paso. (Vista1: 15%, Vista2: 15%, Vista3: 10%)
 2. Integrar el resultado de la normalización. 5%
 3. Graficar el modelo lógico usando la simbología que más te acomode (sólo poner PK y FK). 5%
 4. Una vez desarrollado el modelo lógico, explique se calculan los atributos almacenados en la tabla del repositorio (estrellas, ramas, etc.) y las implicaciones que tiene en el modelo (Vista 1). 5%
 5. En la Vista 1, si la consulta que obtiene los commits de un usuario tardase mucho en ejecutarse, ¿cómo la optimizaría?. 5%

### Pauta

#### Normalización de vistas


#### VISTA 1: id-repositorio, nom-repositorio, {nom-archivo, id-archivo}, {descr-commit, id-commit, fecha-commit}, {tag, id-tag}, {id-rama, nom-rama, descr-rama}, num-estrellas, num-ramas, licencia.

1FN
* VISTA 1.1 **id-repositorio**, nom-repositorio, num-estrellas, num-ramas, licencia, <u>id-usuario</u>.
* VISTA 1.2 **id-archivo**, nom-archivo, descr-commit, id-commit, fecha-commit, <u>id-repositorio</u>.
* VISTA 1.3 **<u>id-repositorio</u>, id-rama**, nom-rama, descr-rama.
* VISTA 1.4 **<u>id-repositorio</u>**, **<u>id-tag</u>**, nom-tag.
* **Supuestos**: archivo, commit, rama y tag tienen ID.
* VISTA 1.5 **id-usuario**, nom-usuario 

2FN
* VISTA 1.1 **id-repositorio**, nom-repositorio, num-estrellas, num-ramas, licencia.
* VISTA 1.2 **id-archivo**, nom-archivo, descr-commit, id-commit, fecha-commit, <u>id-repositorio</u>.
* VISTA 1.3 **<u>id-repositorio</u>, id-rama**, nom-rama, descr-rama.
* VISTA 1.4.1 **<u>id-repositorio</u>**, **<u>id-tag</u>**
* VISTA 1.4.2 **<u>id-tag</u>**, nom-tag, descr-tag

3FN
* VISTA 1.1 **id-repositorio**, nom-repositorio, num-estrellas, num-ramas, licencia.
* VISTA 1.2 **id-archivo**, nom-archivo, descr-commit, id-commit, fecha-commit, <u>id-repositorio</u>.
* VISTA 1.3 **<u>id-repositorio</u>, id-rama**, nom-rama, descr-rama.
* VISTA 1.4.1 **<u>id-repositorio</u>**, **<u>id-tag</u>**
* VISTA 1.4.2 **<u>id-tag</u>**, nom-tag, descr-tag


#### VISTA 2: id-repositorio, {id-publicación, titulo-publicacion, contenido-publicacion, id-usuario, nombre-usuario, fecha-publicacion, votos}, {id-respuesta, conenido-respuesta, fecha-respuesta, id-usuario, nombre-usuario}.

1FN
* VISTA 2.1: **id-usuario**, nom-usuario
* VISTA 2.2: **id-publicación**, **<u>id-usuario</u>**, titulo-publicacion, contenido-publicacion, votos
* VISTA 2.3: **id-respuesta**, **<u>id-usuario</u>**, respuesta-contenido, respuesta-fecha, nom-usuario.

2FN
* VISTA 2.1: **id-usuario**, nom-usuario
* VISTA 2.2: **id-publicación, <u>id-usuario</u>**, titulo-publicacion, contenido-publicacion, respuesta-id, votos
* VISTA 2.2.1: **id-publicacion**, contenido-publicacion, fecha-publicacion.
* VISTA 2.3: **id-respuesta**, **<u>id-usuario</u>**, respuesta-contenido, respuesta-fecha, nom-usuario.
* VISTA 2.3.1: **id-respuesta**, contenido-respuesta, fecha-respuesta.

En este ejercicio, hay un problema potencial con la 2NF porque tanto las tablas Publicacion como respuesta tienen el atributo usuario-id, que es funcionalmente dependiente de la clave primaria (usuario-id) en la tabla Usuario. Esto significa que los detalles de un usuario, como el correo electrónico, se almacenan en varios lugares (tanto en Publicacion como en respuesta) en lugar de un solo lugar (la tabla Usuario).



#### VISTA 3: id-usuario, titulo-articulo, id-articulo, contenido-articulo, articulo-enlaces, {id-revision, id-usuario, contenido-revision}, id-articulo, tit-articulo, contenido-articulo, {articulo-enlaces}

1FN
* VISTA 3.1: **id-usuario**, tit-articulo, id-articulo, contenido-articulo, enlaces-articulo
* VISTA 3.2 **id-revision**, <ul>id-usuario</ul>, contenido-revision

2FN
* VISTA 3.1: **id-articulo**, tit-articulo, id-articulo, contenido-articulo, enlaces-articulo, <ul>id-usuario</ul>
* VISTA 3.2.1 **id-revision**, id-usuario, contenido-revision
* VISTA 3.2.2 **id-revision**, contenido-revision


#### Integración y mejoras
* Repositorio **id-repositorio**, nom-repositorio, num-estrellas, num-ramas, licencia.
* Archivo **id-archivo**, nom-archivo, descr-commit, id-commit, fecha-commit, <u>id-repositorio</u>.
* Rama **<u>id-repositorio</u>, id-rama**, nom-rama, descr-rama.
* TagRepositorio **<u>id-repositorio</u>**, **<u>id-tag</u>**
* Tags **<u>id-tag</u>**, nom-tag, descr-tag
* Usuario: **id-usuario**, nom-usuario
* Publicación: **id-publicación, <u>id-usuario</u>**, titulo-publicacion, contenido-publicacion, respuesta-id, votos
* Respuesta: **id-respuesta, <u>id-usuario</u>**, contenido-respuesta, fecha-respuesta.
* RespuestaUsuario: **id-respuesta, id-usuario**, contenido-respuesta, fecha-respuesta.
* Usuario: **id-usuario**, tit-articulo, id-articulo, contenido-articulo, enlaces-articulo
* RevisiónUsuario **id-revision**, id-usuario, contenido-revision
* Revisión **id-revision**, contenido-revision
* Artículo **id-articulo**, tit-articulo, contenido-articulo, articulo-enlaces

#### Graficar 

![Integración del Modelo](imagenes/certamen2_2023_integrado.png)


 4. Una vez desarrollado el modelo lógico, explique se calculan los atributos almacenados en la tabla del repositorio (estrellas, ramas, etc.) y las implicaciones que tiene en el modelo (Vista 1). 5%
 Hay que hacer un pre cálculo ya que la información está almacenada en varias entidades. Para ello hay que desnormalizar, pero es necesario ya que son datos que se consultan muy frecuentemente.

 5. En la Vista 1, si la consulta que obtiene los commits de un usuario tardase mucho en ejecutarse, ¿cómo la optimizaría?. 5%
 Una opción es colocar un índice pero no en las PK ni FKs ya que por defecto el sistema ya coloca índices en esos atributos. Un commit además afecta a un conjunto de archivos, entonces la operación no es tan trivial como solucionarla con un índice. Una solución sería precalcular en otra tabla los commits por usuario, desnormalizando otra vez. Aunque el uso de índices sí que ayuda a la eficiencia de las consultas.

## PREGUNTA 2: Seguimos con Git (40%)

Aplicar la metodología top-down **para obtener un modelo de datos lógico** a partir del siguiente modelo conceptual:

![Git modelo conceptual](imagenes/certamen1_2023_conceptual.png)

1. Entidades fuertes.
2. Entidades débiles.
3. Asociaciones 1:1
4. Asociaciones 1:N
5. Asociaciones M:N
6. Asociaciones n-arias
7. Herencia
8. Herencia selectiva



### Pauta Pregunta 2: Seguimos con Git

1. Entidades fuertes. Usuario.
2. Entidades débiles. Commit, archivo, rama, repositorio.
3. Asociaciones 1:1: no hay.
4. Asociaciones 1:N: Repositorio - Commit; Usuario - repositorio; Repositorio - Rama
5. Asociaciones M:N: Commit - Archivo
6. Asociaciones n-arias: no hay
7. Herencia: no hay.
8. Herencia selectiva: no hay.



