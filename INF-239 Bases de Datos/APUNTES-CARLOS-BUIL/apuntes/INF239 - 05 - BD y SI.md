# INF239 - Bases de Datos - Base de Datos en el desarrollo de SI

## Base de Datos en el desarrollo de SI

Un Sistema de Información (SI) es un conjunto de aplicaciones (software), datos, recursos materiales (equipos) y personas (usuarios) que interactúan para procesar datos y convertirlos en información relevante para una organización.

Para desarrollar un SI existen diferentes metodologías, pero todas ellas de una u otra forma, implican seguir las siguientes etapas: Análisis, Diseño, Construcción (codificación o programación), Implementación y Mantenimiento.

Las BD son el soporte principal para que los SI logren procesar datos y convertirlos en información. Desde un punto de vista metodológico, las etapas de desarrollo de los SI en las cuales se debe considerar el impacto de las BD son la etapa de Análisis (donde se debe lograr una estructura de los datos o modelo de datos conceptual, que se adapte a los requerimientos de información de los usuarios) y la de Diseño (donde se debe convertir el modelo de datos conceptual en un modelo implementable en el DBMS disponible).

En las asignaturas de Análisis y Diseño de Software y Organización y Sistemas de Información, verán con más detalles metodologías para desarrollar software y SI respectivamente. En la asignatura de Bases de Datos veremos una metodología con foco exclusivamente en los datos, no en el software (como lo verán en Análisis y Diseño de Software), ni en los procesos de la organización (como lo verán en Organización y Sistemas de Información).

Los SI y las BD deben satisfacer los requerimientos de información de todos los niveles de la organización (operacional, táctico y estratégico). Sin embargo, los requerimientos en los distintos niveles son bastantes diferentes. Estos niveles se caracterizan por la decisión que apoyan, el tipo de decisión, el modelo usado para apoyar tal decisión y el tipo de información que requieren. Todos estos elementos se muestran en la tabla que aparece en esta diapositiva. Analicemos la tabla siguiente, tomado como ejemplo, una multitienda. 

| Características | Nivel Estratégico | Nivel Táctico | Nivel Operacional | 
|-----|--------|---|---|
| Decisión que apoya | Planificación Largo Plazo | Control Gerencial | Control Operacional | 
| Tipo de decisión | No Estructurada | Semiestructurada | Estructurada | 
| Modelo más usado | Predictivo | Descriptivo | Normativo | 

<br/>

**Características de la Información**: 
| Características | Nivel Estratégico | Nivel Táctico | Nivel Operacional | 
|-----|--------|---|---|
| Fuente | Medio Ambiente | Registros Internos | Operación Interna | 
| Exactitud | Razonable | Buena | Exacta | 
| Amplitud | Resumida | Detallada | Muy Detallada | 
| Frecuencia | A Solicitud | Periódica | Tiempo Real | 
| Rango de tiempo | Años | Años | Meses | 
| Uso | Predicción | Control | Acción Diaria | 

<br/>

Partamos por las decisiones que los SI apoyan en cada nivel. En el nivel operacional, las decisiones que se toman son para controlar la operación de la organización, es decir, lo que se hace en el día a día (cajeros: ¿cuánto compró cada cliente? se registran las transacciones del negocio). En el nivel táctico, se toman decisiones principalmente para controlar lo que se realiza en nivel operacional (jefe de la sucursal al final del día: ¿cuánto dinero hay en cada caja versus cuánto dinero quedó registrado en la BD, ¿cuadran?). En el nivel estratégico, se está pensando en el largo plazo (gerente: ¿podremos abrir una nueva sucursal el próximo año?).

Luego analicemos el tipo de decisiones que los SI apoyan en cada nivel. En el nivel operacional, las decisiones son del tipo estructuradas, están bien normadas, son fáciles de automatizar pues está claro lo que se realiza y cómo se realiza (cajeros: deben pasar los productos por código de barra, si no tiene el código, llamar a encargada cajas). En el nivel táctico, son del tipo semiestructuradas, hay un cierto grado de incertidumbre (jefe de la sucursal al final del día: ¿qué hacer si no cuadra una caja?). En el nivel estratégico, las decisiones son no estructurada, la incertidumbre es alta, existen efectos externos que aumentan esta incertidumbre (gerente: ¿cómo están los mercados externos para pensar en invertir en abrir una nueva sucursal el próximo año?).

Por otra parte, está el modelo de cómo procesar los datos para obtener la información requerida en cada nivel. En el nivel operacional, es un modelo normativo; existe una norma o un manual de procedimiento donde se explica claramente lo qué se debe hacer (cajero: tiene super normado lo qué debe hacer y lo que no debe hacer). En el nivel táctico, el modelo es descriptivo; se basa en lo que ya sucedió en el nivel operacional, es decir, se requiere información que describa lo qué ya pasó (jefe de la sucursal al final del día: necesita información de cuánto se vendió en cada caja, cuánto se vendió de cada producto, etc.). En el nivel estratégico, el modelo es predictivo, se requiere información que prediga lo qué podría pasar en el futuro, no es necesario que sea con un 100% de exactitud, pero que muestre alguna tendencia (gerente: necesita información de cómo estarán los mercados financieros para el próximo año).

En resumen, las BD que son parte de los SI, apoyan a la organización en forma diferente según el nivel organizacional en que estén los usuarios a los cuales les desarrollaremos un SI. Los 3 criterios anteriores, esto es: decisiones que apoya, tipo de decisiones y modelo más usado, nos dan los lineamientos en lo qué debemos fijarnos como diseñadores de BD y desarrolladores de software, para poder satisfacer los requerimientos de información de nuestros usuarios. Estos requerimientos de información se caracterizan según sea la fuente desde donde se extraen los datos para obtener la información, la exactitud de la información que espera el usuario, la amplitud o grado de detalle de la información, la frecuencia con que requiere la información el usuario, el rango de tiempo que deben cubrir los datos para obtener la información y el uso que le vaya a dar el usuario a la información dentro de la organización.

### Tipos de SI 
Si bien existen diversas clasificaciones de SI, en el curso usaremos aquella que permite realizar una mayor diferenciación con respecto al soporte que dan las BD a los SI. Esta clasificación es:
* OLTP (OnLine Transaction Processing): son SI que permiten automatizar los procesos de nivel operacional de una organización, en los cuales se producen las transacciones del negocio. Por ejemplo, en un banco las transacciones son apertura cuenta corriente, depósitos, giros, obtención crédito hipotecario, pago dividendos, etc. También cubren algunos procesos de nivel táctico. Algunos los llaman SIA (Sistemas de Información Administrativos). Los sistemas OLTP se apoyan generalmente en bases de datos del tipo relacional.

* OLAP (OnLine Analytic Processing): son SI que permiten apoyar el nivel estratégico de una organización, donde se toman decisiones a largo plazo y rodeadas de alto grado de incertidumbre, que definen el futuro del negocio. Por ejemplo, en un banco serían decisiones del tipo poner un nuevo producto en el mercado, abrir una nueva sucursal, etc. También cubren algunos procesos de nivel táctico. Estos sistemas también son llamados DSS (Decision Support Systems). Los sistemas OLAP se apoyan generalmente en bases de datos del tipo multidimensional (Data Warehouse).
Si consideramos a una organización como una pirámide, los sistemas OLTP conforman la base de la pirámide y almacenan grandes volúmenes de datos (todas las transacciones de un negocio), que tienen bajo valor para el negocio si es que no son integrados en una BD y procesados para obtener información. En cambio, los sistemas OLAP apoyan la parte superior de la pirámide, utilizan datos más agregados que aportan mayor valor al negocio pues entregan conocimiento más que información, como se explica en la figura de la siguiente diapositiva.

#### OLTP v/s OLAP

La figura muestra una caja de negra con entrada y salida, donde la entrada son datos y la salida es información. La caja negra representa al sistema OLTP que apoyado en una base de datos relacional es capaz de transformar los datos en información para fortalecer la toma de decisiones de los usuarios.

![OLTP v/s OLAP](imagenes/oltp_vs_olap.png "OLTP v/s OLAP")

Estos sistemas OLTP van registrando todas las transacciones de la organización en el tiempo, lo cual genera grandes volúmenes de datos, que llegado un momento pasan a conformar archivos históricos de la organización pues pierden vigencia en los procesos cotidianos y así no degradan el rendimiento de la BD que está en producción. Por ejemplo, en una Universidad, las inscripciones de asignaturas son transacciones que realizan todos los semestres los alumnos, pero cuando esos alumnos se titulan, ya no deben formar parte de la BD que está en producción dentro del proceso de inscripción de asignaturas; esos alumnos pasan a un estado de exalumnos, pasan a un archivo histórico. En una multitienda las ventas que se realizan forman parte de la BD que está en producción, pero pasado un par de años pasan a archivos históricos, pues ya no se requieren para los sistemas contables o para devoluciones, por ejemplo.

Esto hace que con el tiempo se generen grandes volúmenes de datos en las organizaciones, los cuales suelen ser considerados como una gran “mina” de oro, ya que está la historia del negocio a partir de la cual se puede inferir el futuro, si es que se aplican análisis estadístico de datos.

Aquí nace, un nuevo tipo de base de datos, llamada Data Warehouse. Se trata de un “almacén” donde las organizaciones pueden depositar todos aquellos datos con importancia crítica para la toma de decisiones. Un Data Warehouse consiste básicamente de tres componentes:
* Herramientas de software extractoras, de transformación y carga que se utilizan sobre los datos operacionales y fuentes externas (ETL: Extract, Transform, Load).
* Un warehouse (almacén) para almacenar los datos seleccionados.
* Herramientas para analizar los datos contenidos en el warehouse.

El Data Warehouse es un concepto que trata de resolver la problemática que tienen actualmente las empresas en el análisis rápido de situaciones, la integración de datos procedente de diversas fuentes, el contar con una perspectiva histórica de los datos y el aprovechamiento óptimo de la información organizacional, para a partir de ella generar conocimiento que permita rentabilizar mejor a la organización. Para ello, se necesitan sistemas de información inteligentes, así aparece una nueva generación de sistemas, dentro de los cuales están los sistemas OLAP, y el uso de algoritmos de Data Mining (redes neuronales, árboles de decisión, análisis estadístico, etc.), que junto al Data Warehouse constituyen las tendencias más importantes en el área de bases de datos del último tiempo, y que se engloban todas ellas bajo el nombre de Inteligencia de Negocios (Business Intelligence).

En síntesis, se puede establecer que hoy en día, los sistemas de información en general, son clasificables en aquellos que están orientados a las transacciones (sistemas OLTP: On-Line Transaction Processing) y aquellos orientados a analizar temas de interés específico del tomador de decisiones (sistemas OLAP: On-Line Analytic Processing). Los TPS y MIS, apoyados la mayoría de las veces en bases de datos relacionales, son ejemplos de sistemas OLTP. En cambio, los sistemas OLAP funcionan mejor si hay un Data Warehouse (implementado como una Bases de Datos Multidimensional) que integre y depure los datos provenientes de los distintos sistemas OLTP de una organización, y que permita visualizarlos como un cubo multidimensional y no como una tabla bidimensional. 