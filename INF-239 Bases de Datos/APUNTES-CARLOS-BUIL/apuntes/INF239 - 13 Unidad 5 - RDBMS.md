# INICIO UNIDAD 5: Sistemas Administradores de Bases de Datos Relacionales (RDBMS)

Puntos a ver en esta Unidad:

1.  Componentes de un DBMS

2.  Funciones de un DBMS

    1.  Funciones generales

    2.  Servicios de respaldos

    3.  Mecanismo de recuperación

    4.  Control concurrente

    5.  Mecanismos de seguridad

## Componente de un DBMS

En esta figura con los elementos principales de un DBMS (o SABD por su traducción en español de Sistema Administrador de Bases de Datos, también traducido como SGBD, donde administrador es cambiado por gestionador). Es una figura que detalla lo planteado en la Unidad 1, donde se mencionaron estos componentes, pero desde una perspectiva más general o de usuarios.

![Arquitectura de un SGBD](imagenes/arquitectura_sgbd.png)

La figura muestra dentro de un marco de línea punteada, los componentes principales de un DBMS, y los elementos del entorno con los cuáles interactúa, principalmente instrucciones (o sentencias o comandos o estamentos de algún lenguaje de programación) y la base de datos física (donde están los valores de los datos).

De esta manera a través de un programa de aplicación escrito en algún lenguaje de programación, denominado lenguaje huésped pues aloja comandos de interacción con la BD, o de comandos como los de SQL, se interactúa con los siguientes componentes del DBMS:

-   **Compilador DDL (Data Definition Language):** recibe un comando del
    DDL del SQL y lo traduce a una entrada en el catálogo o BD lógica o
    Diccionario de Datos, por ejemplo, un comando CREATE TABLE que
    genera los metadatos asociados la descripción de una tabla (nombre
    tabla, atributos, tipo de cada atributo, restricciones sobre si es
    PK, FK, not NULL, etc.).

-   **Procesador de Consultas y de Transacciones:** recibe comando del
    DML (Data Manipulation Language) del SQL y ejecuta la consulta
    apoyándose en el catálogo o BD lógica o Diccionario de Datos, por
    ejemplo, un comando SELECT que buscar leer algunas filas de una o
    más tablas. Si se trata de un solo comando se tiene una consulta, si
    se trata de un conjunto de comandos se tiene una transacción.

-   **Compilador DML (Data Manipulation Language):** en caso de que, la
    interacción con el DBMS es a través de un programa de aplicación
    escrito en algún lenguaje de programación, este compilador es capaz
    de traducir a lenguaje de máquina dichas instrucciones para ser
    procesadas apoyadas por el catálogo o BD lógica o Diccionario de
    Datos.

Algunos DBMS también incorporan componentes que permiten definir
estrategias de control concurrente y mecanismos de respaldos y
recuperación ante fallas.

### DBMS versus RDBMS

Un RDBMS (Relational Data Base Management System) es un DBMS basado en
el modelo relacional por lo que almacena los datos como tablas
(relational) asociadas entre si (relationship).

Los DBMS surgen en 1960 basados en estructuras jerárquicas (árbol) y
posteriormente en redes (grafos).

RDBMS fue propuesto por Edgar Codd en 1970, simplificando la navegación
sobre la base de datos de los modelos anteriores.

La normalización ya vista está presente en los RDBMS, así como las
propiedades ACID (Atomicity, Consistency, Isolation, Durability) para
las transacciones, las cuales serán vistas en un rato más.

## Funciones RDBMS

### Funciones RDBMS -- Generales

a)  **Definición de los datos (comandos DDL):** create, alter, drop,
    etc. El DBMS debe proveer un conjunto de comandos que permitan
    definir los metadatos de cada uno de los elementos de nuestras bases
    de datos. Estos comandos son los que interactúan directamente con el
    catálogo o diccionario de datos, dejando allí registrados estos
    elementos., que permiten documentar las distintas bases de datos que
    sean gestionadas por el DBMS.

b)  **Manipulación de los datos (comandos DML):** insert, update,
    delete, etc. El DBMS debe proveer un conjunto de comandos que
    permitan manipular los datos, desde el poblamiento inicial hasta la
    mantención que se debe hacer durante el ciclo de vida de la base de
    datos, como así también todas las lecturas requeridas para generar
    la información requeridas por los usuarios. Aquí están las
    denominadas operaciones CRUD (Create o insert, Read o select,
    Update, Delete).

c)  **Servicios de Integridad:** reglas de validación e integridad
    referencial. El DBMS debe proveer la posibilidad de asegurar la
    calidad de los datos que ingresan a la BD, a través de mecanismos
    que permitan definir reglas de validación (por ejemplo, que el valor
    de un atributo esté dentro de ciertos rangos o incluido en una
    cierta lista de valores) y asegurar la integridad referencial (por
    ejemplo, eliminación en "cascada" al eliminar una entidad "fuerte" o
    chequeo de la existencia de la entidad "fuerte" antes de ingresar
    una entidad "débil".

d)  **Manipulación de un Diccionario de Datos (DD):** el DBMS debe
    proveer un DD, Catálogo o BD lógica, el cual es administrado como
    una BD más por el DBMS, pero como una BD solo de lectura para los
    usuarios del DBMS, ya que las inserciones, eliminaciones y
    actualizaciones no se realizan directamente sobre ella, sino que, en
    forma indirecta, a través de algún comando DDL. Por ejemplo, al usar
    CREATE TABLE se está insertando una fila a la tabla dentro del DD
    que almacena los metadatos de las tablas de todas las BD que
    administra el DBMS. 

### Funciones RDBMS -- Generales -- Diccionario de Datos

Para poder imaginarse lo que es en concreto un DD, la interfaz que existe en el RDBMS llamado SQL-Server de Microsoft para interactuar con el DD; allí se puede identificar que se documentan los elementos principales de todas las BDs que administra el DBMS: Database, Tables, Table Columns, Indexes, Views, View Columns,
Procedures, Functions.

Además, se muestra con más detalle lo que se almacena como metadatos en la tabla que describe a las columnas de una tabla.

No perder de vista que cada uno de estos elementos es almacenado como una tabla, de tal manera que el DD es una base de datos, pero solo de lectura para nosotros los usuarios del DBMS.

### Funciones RDBMS -- Generales -- Integridad de transacciones

Ahora pasamos a:

e)  **Integridad en las transacciones:** consiste en evitar que las
    transacciones finalicen en un estado intermedio, teniendo en cuenta
    que:

-   Una Transacción es un conjunto de operaciones (u órdenes) que se
    ejecutan en forma indivisible (atómica) sobre una BD.

-   El DBMS debe mantener la **integridad de los datos**, haciendo que
    estas transacciones no puedan finalizar en un estado intermedio.

-   Si por algún motivo se debe cancelar la transacción, el DBMS empieza
    a deshacer las órdenes ejecutadas hasta dejar la BD en su estado
    inicial (llamado **punto de integridad**), como si la orden de la
    transacción nunca se hubiese realizado.

Para ejemplificar el tema de integridad en las transacciones, pensemos en un ejemplo típico de hoy en día en el ámbito de la banca digital.
Suponer que se necesita hacer una transferencia bancaria, donde tenemos una cuenta origen desde la cual se girará dinero para ser depositado en una cuenta destino. Es decir, se debe ir a la tabla CUENTAS, ubicar la cuenta de quien hará la transferencia y disminuir su atributo SALDO en el monto a transferir (operación 1); luego acceder a la cuenta destino y aumentar su atributo SALDO en el monto transferido (operación 2).

¿Qué pasa si solo se ejecuta la operación 1 y justo cuando se iba a
ejecutar la operación 2 se cae el sistema?

El DBMS debe garantizar que no se pierda el dinero, para ello las dos
operaciones deben ser atómicas, es decir, el DBMS debe garantizar que,
bajo cualquier circunstancia (incluso una caída del sistema), el
resultado final sea: se han realizado las dos operaciones o ninguna de
ellas.


La **integridad en las transacciones** es la función que posee el DBMS
para asegurar que una transacción no finalice en un estado intermedio
erróneo.

Para ello cuentan con los siguientes elementos:

-   **Compromiso de una Transacción:** indicación de que una transacción
    se ha ejecutado por completo, y que todos los cambios se deben
    guardar "para siempre" en la base de datos. Corresponde al comando
    COMMIT de SQL.

-   **Rollback de una Transacción:** trabajo consistente en deshacer
    todas las operaciones ya ejecutadas por una transacción, hasta dejar
    la base de datos en el estado anterior. Corresponde al comando
    ROLLBACK de SQL.

-   Un DBMS debe proveer las funciones necesarias para que sus
    transacciones tengan las características ACID (Atomicity,
    Consistency, Isolation and Durability):


-   **A**tomicidad: cualquier cambio de estado producido por una
    transacción es atómico ("todo o nada"). Es decir, asegura que la
    transacción se realice completamente o no se realice, por lo que
    ante una falla no quedará a medio camino.

-   **C**onsistencia: cada transacción lleva a una base de datos, desde
    un estado consistente a otro también consistente. Es decir, sólo se
    ejecutan operaciones que no rompen la integridad.

-   Aislamiento (***I**solation*): la ejecución concurrente de un
    conjunto de transacciones debe comportarse como si cada transacción
    fuera la única en proceso. Es decir, si hay 2 o más operaciones
    sobre los mismos datos, ellas son independientes.

-   **D**urabilidad: garantiza que los cambios producto de una
    transacción comprometida perduren en el tiempo. Es decir, una vez
    realizada la transacción en forma exitosa, ésta persistirá y no se
    podrá deshacer, aunque falle el sistema.

La forma en que se implementa en SQL la integridad en las transacciones
está dada por los comandos que permiten definir una transacción. Estos
son:

-   BEGIN TRAN: inicio de la transacción

-   COMMIT TRAN: transacción terminada con éxito

-   ROLLBACK TRAN: se ha producido una falla, transacción abortada, la
    BD vuelve al punto de integridad.

En algunos DBMS es posible tener transacciones anidadas, por ejemplo, en
Microsoft SQL Server. Una transacción anidada permite tener
transacciones dentro de otras, es decir, se puede empezar una nueva
transacción sin haber terminado la anterior. En estos casos hay que
tener cuidado con el comportamiento que tienen los comandos:

-   COMMIT TRAN: Dentro de una transacción anidada esta sentencia no
    *finaliza* ninguna transacción interna, ni *guarda* los cambios
    hasta que todas las modificaciones efectuadas sobre los datos desde
    el inicio de la transacción sean parte permanente en la base de
    datos, es decir, se llegue al COMMIT más externo.

-   ROLLBACK TRAN: Dentro de una transacción anidada esta sentencia
    deshace todas las transacciones internas hasta la instrucción BEGIN
    TRANSACTION más externa.

### Funciones RDBMS -- Respaldos

Con la integridad de transacciones, cerramos las funciones generales que
se comenzaron a describir en la ppt 7, ahora pasamos a describir algunos
servicios adicionales que entregan los DBMS.

En primer lugar, están los **Servicios de Respaldo.** Un respaldo o
*backup* es una copia de los datos en otro medio de almacenamiento para
poder en caso de alguna "catástrofe" (o pérdida de datos) sea posible
restaurar la BD al momento previo a la "catástrofe". Si bien los DBMS
proveen comandos para hacer esto, es muy relevante las acciones que
deben realizar los DBAs (Data Base Administrator), quienes
principalmente debe definir un Plan de Respaldo teniendo en cuenta:

-   Debe estar documentado y comunicado

-   ¿Cuándo hacer respaldos? Importante tener un calendario de respaldos

-   ¿Cuáles datos incluir? ¿Toda la BD, algunas tablas, solo algunas
    filas de una tabla?

-   Cantidad de copias

-   Modalidad de las copias

-   Tipos de respaldos

Los respaldos o también llamadas copias de seguridad proveen una
importante solución para proteger los datos críticos almacenados en las
bases de datos. Antes de programar los respaldos, conviene hacer una
estimación de cuánto espacio de disco será usado en caso de requerir una
copia completa de la base de datos, y también cómo esta estimación puede
ir variando en el tiempo.

Se debe tener en cuenta que los tipos de respaldos van de la mano de los
mecanismos de recuperación de la base de datos con que se cuente. Antes
de explicar estos mecanismos, profundicemos en las modalidades de las
copias y los tipos de respaldos.

Los DBMS suelen proveer facilidades para contar con las siguientes
modalidades de copias:

-   **Copia Simple:** se hace un único ejemplar del respaldo.

-   **Copia Doble**: se repite la tarea de respaldar, con el fin de
    tener dos ejemplares del mismo.

-   **Copia generacional** (abuelo-padre-hijo): se hacen respaldos a lo
    largo del tiempo, generándose una historia de los respaldos.


Los DBMS suelen proveer facilidades para contar con los siguientes tipos
de respaldos:

-   **Offline:** se guardan desconectados de la BD que está en
    producción.

-   **Online:** apoyado con discos espejos, quedando conectados a la BD
    que está en producción.

-   **Global:** se respalda el archivo completo (full backup).

-   **Parcial:** se respalda solamente los registros que cambian, puede
    ser del tipo diferencial o incremental.

Los 2 primeros tipos dependen del tipo de conexión que tenga el respaldo
con la BD que está en producción (offline: sin conexión; u online: con
conexión). Los 2 siguientes dependen de la cantidad de registros que se
copien cada vez que se ejecuta un respaldo (full o incremental backup).

En la siguiente tabla se puede identificar la diferencia que existe
entre los respaldos globales (o completos) y los parciales (diferencial
o incremental):


![Tipos de respaldo](imagenes/respaldo.png)

Se puede concluir de la tabla que los respaldos completos copian todos
los datos cada vez que se ejecutan, aunque de un instante al otro
instante de respaldar, no se hayan producido cambios en todos los
registros del archivo. En cambio, los respaldos parciales copian una
cantidad menor de registros, solo aquellos que cambian desde el último
respaldo al siguiente (en el caso de los incrementales), o desde el
momento inicial hasta el actual (en el caso de los diferenciales).
Siendo los parciales consumidores de menos recursos de procesamiento y
almacenamiento.

### Funciones RDBMS -- Mecanismos de recuperación

Los respaldos van de la mano de algún mecanismo de recuperación que
entra actuar cuando se produce algún daño a la BD o a algún archivo de
ella, es decir, alguna "catástrofe".

Los DBMS suelen proveer 3 mecanismos o comandos: RESTORE/RERUN,
ROLLBACK, ROLLFORWARD.

Todos ellos funcionan a partir del momento en que se produce un daño a
la BD, el cual puede ser físico (daño completo del dispositivo donde
está la BD o un archivo, ya sea por un incendio, inundación, etc.) o
lógico (daño de los valores almacenados por el mal funcionamiento de un
programa, por ejemplo). En ese momento se necesita recuperar o subir la
BD o el archivo a producción lo más rápidamente posible, para lo cual
estos comandos actúan apoyándose en algún tipo de respaldo.

A continuación, se analizará cada uno de estos mecanismos de
recuperación.

### Funciones RDBMS -- Mecanismos de recuperación

El RESTORE/RERUN requiere contar con un full-backup de la BD (o del
archivo dañado) y con todas las transacciones ejecutadas en el período
de tiempo que va desde el momento que se sacó el full-backup hasta que
se produjo el daño. Es más usado cuando el daño es del tipo físico y
cuando gran parte de los registros de la BD habían sido modificado en
ese período de tiempo.

Lo que hace este comando RESTORE/RERUN es "reejecutar" todas las
transacciones del período de nuevo, esto lo va haciendo sobre el
full-backup, de tal manera de así recuperar la BD (o el archivo dañado),
a un estado posterior a la "catástrofe".

El **ROLLBACK** requiere contar con un respaldo incremental de la BD (o
del archivo dañado) del tipo on-line, denominado **preimagen**, donde se
ha guardado una copia o imagen previa de todos los registros que se han
cambiado hasta que se produjo el daño. Es más usado cuando el daño es
del tipo lógico y cuando no todos los registros de la BD son modificado
en el tiempo.

Lo que hace este comando ROLLBACK es tomar el archivo dañado lógicamente
y sobre él cargar cada uno de los registros que hay en el respaldo
preimagen, recorriendo éste desde el final al inicio, de tal manera de
recuperar la BD (o el archivo dañado), a un estado previo a la
"catástrofe". Es como hacer un "undo" de las transacciones ejecutadas
antes de la "catástrofe".


El **ROLLFORWARD** requiere contar con un respaldo incremental de la BD
(o del archivo dañado) del tipo on-line, denominado **postimagen**,
donde se ha guardado una copia o imagen posterior de todos los registros
que se han cambiado hasta que se produjo el daño. Es más usado cuando el
daño es del tipo físico y cuando no todos los registros de la BD son
modificado en el tiempo.

Lo que hace este comando **ROLLFORWARD** es tomar el full-backup de la
BD (o del archivo dañado) y sobre él cargar cada uno de los registros
que hay en el respaldo postimagen, recorriendo éste desde el inicio al
final, de tal manera de recuperar la BD (o el archivo dañado), a un
estado posterior a la "catástrofe".


Otro mecanismo complementario a los anteriores es el comando CHECKPOINT,
que actúa de manera preventiva (como los respaldos) y también
post-catástrofe, es decir, durante la recuperación.

El DBMS mantiene una bitácora (o un log) donde van quedando registrado
el momento que ha comenzado una transacción (timestamps), si ha cambiado
o leído algún atributo, si ha terminado en forma exitosa o ha sido
abortada, entre otros aspectos.

Un CHECKPOINT es una marca que se graba periódicamente en la bitácora,
puede ser cada "m" minutos o cada "n" transacciones completadas
exitosamente (esto lo puede definir el DBA). Cada vez que esto sucede se
suspende temporalmente la ejecución de las transacciones hasta que se
grabe esta marca.

Estas marcas permiten que una vez que se produce una catástrofe, se
recorra la bitácora y se vuelva a rehacer desde el último CHECKPOINT,
pues esta marca asegura que todas las transacciones anteriores
alcanzaron a ser ejecutadas con éxito.

A continuación, se entrega una tabla con las posibles fallas (o
"catástrofes") y la sugerencia qe cuál mecanismo de recuperación
conviene usar.

|POSIBLE FALLA                |TECNICA DE RECUPERACION                                                        |
|-----------------------------|-------------------------------------------------------------------------------|
|Transacciones abortadas      |ROLLBACK                                                                       |
|Datos incorrectos            |ROLLBACK  o Transacciones compensatorias                                       |
|Caídas del Sistema           |ROLLBACK (si no hubiera daño en status) CHECKPOINT y ROLLFORWARD (si hubo daño)|
|Destrucción de base de datos |ROLLFORWARD                                                                    |
|Errores de Programación      |ROLLBACK                                                                       |


Ahora veremos  un ejemplo de lo que significan los respaldos de tipo incremental y los
mecanismos de recuperación asociados.

Suponer que se tiene una tabla Cuenta Corriente con el siguiente contenido:

 **TABLA CUENTA CORRIENTE** 
 | NRO-CTA  |         SALDO |
 | ------------------------ | ------- |
 | 111-1           |         100 |
 | 222-2           |         500 |
 | 444-4           |         800 |
 | 666-6           |         50 |
 | 777-7           |         1000 |
 | 888-8           |         600 |

Y que se ha definido para ella una política de respaldos incrementales del tipo pre y post-imagen; además, suponer que durante esta mañana se han registrados las siguientes transacciones bancarias, siendo D un Depósito y G un Giro.

**TABLA TRANSACCIONES**

  | NRO-CTA |       FECHA-HRA-TRAN |   TIPO-TRAN  | MONTO |
  | --------------------- | ----------------------- | ----------- |  ----------- | 
  | 222-2 | 02/06-08:35 |  G |  500 |
  | 888-8 | 02/06-08:35 |  D |  400 |
  | 444-4 | 02/06-08:40 |  G |  600 |
  | 111-1 | 02/06-08:42 |  D |  100 |
  | 222-2 | 02/06-09:40 |  D |  600 |
  | 777-7 | 02/06-10:00 |  G |  200 |
  | 222-2 | 02/06-12:00 |  G |  300 |
  | 444-4 | 02/06-12:01 |  D |  200 |

Estas transacciones fueron ejecutadas sobre la tabla Cuenta Corriente,
generándose la actualización correspondiente, pero dado que había
respaldos pre y post-imagen, esta actualización implicó realizar 3
operaciones de escritura por cada transacción: una en el respaldo
pre-imagen, otra en la tabla Cuenta Corriente y otra en el respaldo
post-imagen.

Es decir, estos respaldos operan de la siguiente forma, al ejecutarse la
transacción va y busca la fila asociada a la cuenta corriente en la
tabla Cuenta Corriente, una vez que la encuentra, la copia al respaldo
pre-imagen (saca copia o foto antes de que se actualice), luego realiza
la actualización y posteriormente, se realiza una copia de la fila
actualizada al respaldo post-imagen (saca copia o foto después de que se
actualice), quedando los 3 archivos involucrados de la siguiente forma:

  **TABLA CUENTA CORRIENTE**   
  | NRO-CTA | SALDO
  | ---------------------------- | --------------- |
  |111-1 | 100 200 |s
  |222-2 | 500 0 600 300 |s
  |444-4 | 800 200 400 |s
  |666-6 | 50 |s
  |777-7 | 1000 800 |s
  |888-8 | 600 1000 |s

  **RESPALDO PRE-IMAGEN**           **RESPALDO POST-IMAGEN**             
  | NRO-CTA |  SALDO |  NRO-CTA | SALDO |
  | ------------------------- | ------- |-------------------------- | --------- |
  | 222-2   | 500 |     222-2 |     0 |
  | 888-8   |   600|      888-8 |     1000 |
  | 444-4   |   800|      444-4 |     200 |
  | 111-1   |   100|      111-1 |     200 |
  | 222-2   |   0|        222-2 |     600 |
  | 777-7   |   1000|     777-7 |     800 |
  | 222-2   |   600|      222-2 |     300 |
  | 444-4   |   200|      444-4 |     400 |

Si suponemos que se produce una "catástrofe", entonces existe la
posibilidad de recuperar la tabla, ocupando ROLLBACK y ROLLFORWARD, dado
que existen respaldos pre-imagen y post-imagen respectivamente.

El ROLLBACK actúa recorriendo el respaldo pre-imagen al revés, parte con
el último registro, lee y copia en la tabla Cuenta Corriente, que se
supone está dañada lógicamente. La gracia es que no se ejecutan de nuevo
las transacciones, solo se lee y copia el registro en la tabla dañada,
por lo cual es menos tiempo de procesamiento el requerido, además que
solo se trabaja con los registros cambiados, así es que son menos los
datos analizados. De esta manera, se obtiene la tabla dañada en el
estado en que estaba antes de la "catástrofe.

El ROLLFORWARD actúa recorriendo el respaldo post-imagen desde el inicio
al final, lee y copia en el full-backup de la tabla Cuenta Corriente,
pues no puede trabajar con la tabla misma, ya que este mecanismo se usa
cuando la tabla original está dañada físicamente. La gracia es que no se
ejecutan de nuevo las transacciones, solo se lee y copia el registro en
el respaldo full-backup, por lo cual es menos tiempo de procesamiento el
requerido, además que solo se trabaja con los registros cambiados, así
es que son menos los datos analizados. De esta manera, se obtiene la
tabla dañada en el estado posterior a la "catástrofe, es decir, la tabla
Cuenta Corriente queda como si no hubiese pasado nada.

### Funciones RDBMS -- Control de la concurrencia

Otra función adicional que proveen los DBMS es el control de la
concurrencia, lo cual permite evitar que se produzcan inconsistencias en
la BD cuando más de una transacción accede a un mismo dato para
actualizarlo. Es decir, debe controlarse que las operaciones simultaneas
no interfieran entre sí.

Por ejemplo, si suponemos una tabla de Cuenta Corriente con los
atributos Nro.Cuenta y Saldo, puede darse la siguiente situación en
cuentas bipersonales. Si se tiene como saldo en la cuenta \$100 y llega
el usuario1 y desea girar \$50, al ejecutarse esa transacción se
traslada la fila de esta cuenta corriente desde el disco a memoria
principal al área de trabajo del usuario 1 para poder ejecutar la
operación de giro, pero si en ese mismo instante llega el otro titular
de la cuenta bipersonal (usuario2) quien desea depositar \$60, se
produce el traslado de la misma fila desde el disco a memoria principal
al área de trabajo del usuario 2 para poder ejecutar la operación de
depósito. Ambas operaciones se ejecutan, quedando un saldo de \$50 para
el usuario1 y un saldo de \$160 para el usuario2, luego se debe grabar
el resultado en el disco, y suponiendo que el usuario1 toma primero el
acceso al disco, se graba el registro con saldo \$50, en otro instante,
le toca al usuario2 dejar grabado en el disco su valor, quedando el
saldo final en \$160, siendo que el saldo verdadero debió ser \$110.

¿Qué ha sucedido acá? No se ha controlado el acceso concurrente, es
decir, debió haberse bloqueado el acceso al usuario2 al determinarse que
la tabla Cuenta Corriente estaba siendo actualizada por el usuario1.
Frente a esto es que los DBMS proveen los mecanismos de bloqueo que se
explicarán a continuación.


Los mecanismos de bloqueo que en general proveen los DBMS para el
control concurrente se presentan a distintos niveles:

1.  **Base de datos completa:** si hay operaciones simultáneas se
    bloquean todas las tablas de la BD. En nuestro ejemplo anterior, esto es ineficiente pues se dejaría esperando
    a todos los usuarios de las diferentes tablas de la BD, mientras se
    hace la actualización del saldo de una cuenta corriente. Pero si
    tendría sentido, por ejemplo, si se desea respaldar toda la BD.

2.  **Archivos de la base de datos:** aquí se bloquea solo la tabla
    donde está el registro que se desea cambiar. Para nuestro ejemplo,
    esto es un poco más eficiente, pues solo se quedarán esperando
    quienes necesiten acceder a la tabla Cuenta Corriente, lo que igual
    es complejo e impacta negativamente en la calidad del servicio que
    el banco da a sus clientes.

3.  **Registros de un archivo:** aquí se bloquea solo la fila dentro de
    la tabla donde están los datos de la cuenta corriente que se desea
    cambiar. Para nuestro ejemplo, esto es más eficiente, pues solo se
    va a quedar esperando el Usuario2, dado que la cuenta era
    bipersonal.

4.  **Atributo de un archivo:** aquí se bloquea solo la columna de la
    fila dentro de la tabla donde están los datos de la cuenta corriente
    que se desea cambiar. Para nuestro ejemplo, esto es aún más
    eficiente, pues solo se va a quedar esperando el Usuario2 para
    actualización, dado que la cuenta era bipersonal, pero al menos
    podría leer los otros atributos.

5.  **Bloque de datos:** aquí el bloqueo no es a nivel de algún elemento
    de la BD, es a nivel del bloque físico que se usa dentro del
    dispositivo de almacenamiento secundario, donde pueden haber más de
    un registro o filas de una tabla.

Según sea el nivel utilizado, se hace referencia al bloqueo como
granularidad "gruesa" (nivel 1 y 2), "fina" (nivel 3 y 4), o
"intermedia" (nivel 5).

Esto soluciona el problema de concurrencia, pero genera otro problema,
conocido como el "abrazo mortal" (deadlock), algo así como que con
tantos bloqueos llega un momento en que todos se quedan bloqueados. Esto
puede pasar, por ejemplo, si alguno de los usuarios que ha generado un
bloqueo entra en un loop. Algunos DBMS enfrentan este problema,
monitoreando constantemente la bitácora de las transacciones para
detectar si hay usuarios que llevan mucho tiempo con un elemento de la
BD tomado.

### Funciones RDBMS -- Mecanismos de seguridad

En las BDs están los datos críticos de la organización, por lo cual hay
que protegerlos de accesos no autorizados. Para ello los DBMS proveen
diferentes mecanismos, entre ellos los más comunes son:

-   Vistas (view) o subesquemas: son tablas virtuales cuyo contenido
    está definido a partir de los datos que se desean proteger. Al igual
    que una tabla, una vista consta de un conjunto de columnas y filas
    de datos con un nombre. Son porciones de la BD que permiten asignar
    a los usuarios solo acceso a aquellos datos que necesitan. Pueden
    estar conformadas por una o más tablas, o por partes de algunas
    tablas (solo algunas filas o columnas). Se definen a través de
    comando CREATE VIEW.

-   Encriptación: consiste en almacenar los datos de una manera tal que
    no sean legibles por terceras personas. Se puede encriptar desde la
    base de datos completa hasta solo una determinada columna, pasando
    por tablas o filas en particular. Los DBMS tiene distintos
    algoritmos de encriptación, el costo de esto si eleva los tiempos de
    procesamiento pues se debe encriptar al almacenar y desencriptar al
    leer. La encriptación se define a través de comandos como CREATE
    CERTIFICATE y CREATE SYMMETRIC KEY.

-   Autentificación: Algún dispositivo, generalmente biométrico (huella
    digital, iris del ojo, voz, etc.), que asegure que quien está
    accediendo a los datos es el usuario autorizado para ello.

-   Perfiles de usuarios o Reglas de autorización (mediante matrices):
    consiste en definir las operaciones que puede o no hacer un usuario
    sobre determinados recursos de la BD. Para esto se utiliza el
    comando GRANT.

Para manejar los perfiles de usuarios se definen los distintos objetos de la BD que se quieren proteger, los sujetos o usuarios (o grupos de usuarios) que tendrán acceso a ellos con las acciones y restricciones que deben cumplirse para dar el acceso.