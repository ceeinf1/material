# UNIDAD 4: Diseño físico de bases de datos relacionales (BDR)

Identifica los 4 puntos a ver en esta Unidad:

1.  Contexto Diseño Físico

2.  Organización de Archivos

3.  Índices

4.  Método para el Diseño Físico de una BDR


## Etapas para Diseño de BD

Recordar esta figura con las etapas para el diseño de BD vistas en el
capítulo 1.

![Etapas en el diseño de una base de datos](imagenes/etapas_bd.png "Etapas en el diseño de una base de datos")

Planteamos que existen diferentes metodologías para el desarrollo de software, las cuales tienen etapas muy claras donde se identifica el
cómo y cuándo se debe profundizar en el diseño de la BD.

Las etapas principales de estas metodologías se pueden sintetizar en el esquema que aparece en esta diapositiva, donde se identifican 2 enfoques, uno orientado a los procesos (lado izquierdo del esquema) y otro a los datos (lado derecho del esquema), en el cual estamos profundizando este semestre.

Con la materia vista hasta el momento, hemos cubierto las etapas de:

1.  Recolección y Análisis de Requisitos, permite obtener los requisitos
    o requerimientos que se le piden a la BD, es decir, qué información
    debemos poder extraer de ella (requerimientos de información).

2.  Diseño Conceptual, permite obtener un modelo de datos conceptual que
    satisfaga los requisitos identificados en etapa anterior (equivale a
    la maqueta de un arquitecto).

3.  Elección del Software o DBMS que se utilizará.

4.  Diseño Lógico, permite traducir el modelo de datos conceptual en
    algo entendible por el DBMS, es decir, en un modelo lógico (equivale
    al plano de un arquitecto).

Ahora nos corresponde cubrir las siguientes etapas:

5.  Diseño Físico, permite agregar características físicas al modelo que
    optimicen el funcionamiento de la BD.

6.  Implementación de la BD o creación de la BD.

### Etapas para Diseño de BD

En esta diapositiva se grafica lo que implican estas 2 últimas etapas. El punto de partida es un modelo de datos lógico (relacional en nuestro
caso) al cual, gracias a la etapa de Diseño Físico, se le deben
incorporar algunas características provistas por el DBMS que permitan
potenciar el funcionamiento de la BD, y luego en la etapa de
Implementación, traducir todo a un conjunto de comandos del DDL del SQL
del DBMS para poder crear cada una de las tablas del modelo y sus
características físicas adicionales.

#### Diseño Físico

Esta etapa busca elegir las **estructuras de almacenamiento**
específicas y **caminos de acceso** para los archivos (o tablas), con el
objetivo de obtener un buen rendimiento de las aplicaciones que hacen
uso de las distintas bases de datos.

Cuando se habla de estructuras de almacenamiento, se está haciendo
referencia a las alternativas que ofrecen los DBMS para organizar el
almacenamiento de los datos dentro de algún dispositivo de memoria
secundaria (organizaciones de archivos) y qué ofrece para después ir a
recuperar esos datos (mecanismos de acceso como índices).

Por ejemplo, un dispositivo de memoria secundaria como un **disco**,
está dividido en un conjunto de **platos,** que se dividen en **pistas**
y cada pista en **sectores**. Todos los platos están unidos por un mismo
eje concéntrico que gira a gran velocidad, donde sobre cada plato y en
cada una de sus caras hay una **cabeza lecto/grabadora** que permite
acceder a los datos en **bloques** o conjuntos de sectores agrupados en
lo que se denomina un **cilindro**, se requiere que el DBMS aproveche
esta estructura, y almacene los datos en bloques que puedan ser leídos
por cilindros para aprovechar al máximo el movimiento de las cabezas
lecto/grabadoras.

Como diseñadores de BD en esta etapa, debemos tener las siguientes
consideraciones que nos permitan optimizar el funcionamiento de la BD en
el tiempo:

-   **Rendimiento (performance) del Sistema:** número promedio de
    **transacciones** que pueden ser procesadas por minuto por la base
    de datos.

-   **Tiempo de Respuesta:** tiempo transcurrido entre la **entrada de
    la transacción** a la base de datos y la **llegada de la
    respuesta**.

-   **Utilización del Espacio en Disco:** es la cantidad de memoria que
    los **archivos y sus índices** ocupan en los discos del sistema.

Lo anterior implica que, como diseñadores de BD, en esta etapa debemos
realizar análisis sobre las:

-   **Consultas:** se entiende por consulta a una operación única (por
    ejemplo, una lectura directa)

-   **Transacción**: un conjunto de operaciones que se ejecutan formando
    una unidad de trabajo indivisible o atómica (por ejemplo, navegar
    por la BD a través de todas las tablas requeridas para emitir una
    factura).

Estos análisis implican realizar estudios sobre:

-   **Frecuencia** esperada de la invocación de la **consulta y
    transacciones**.

-   Posibles **restricciones de tiempo** de las consultas y
    transacciones.

-   **Frecuencias** esperadas de las operaciones de **actualización**.

El DBMS es muy relevante en el diseño físico para en base a estos
análisis se pueda optimizar el funcionamiento de la BD. Para esto cada
DBMS permite una variedad de organizaciones de archivos y métodos de
accesos como:

-   Organizaciones de archivos: secuencial, *hashing*, organizados como
    árbol, ...

-   Índices: dinámicos, *bitmap*, ...

Esta diapositiva solo muestra título de subunidad Organizaciones de
Archivos, que se comenzará a estudiar. Se verán las siguientes: archivos
secuenciales, archivos hashing (directos), archivos organizados como
árbol, clusters y archivos particionados.

#### Diseño Físico -- Archivos secuenciales

Una de las organizaciones de archivos más tradicionales es la
secuencial. Se base en almacenar cada registro de un archivo o fila de
una tabla, una al lado de la otra. Se agrupan estas filas en bloques
para una mejor recuperación de los datos, en el sentido de no tener que
estar llevando a memoria principal de una fila en una, se transfieren
varias filas en conjunto (es decir, un bloque), de tal manera de ahorrar
tiempos. El tamaño del bloque (o cantidad de filas que puede tener) es
configurable para cada tabla a través del comando CREATE TABLE.

Ahora vemos 2 ejemplos de tipos de archivos
secuenciales que es común sean manejados en todos los DBMS (se
explicaran suponiendo una tabla de empleados con los atributos NOMBRE,
RUT, FECHA-NACIMIENTO, PUESTO, SUELDO, SEXO):

-   **Secuencial desordenado:** almacena las filas como van llegando,
    una detrás de la otra, y se van asignando los bloques en forma
    consecutiva. En el ejemplo de la diapositiva están almacenados en
    bloques de tamaño 4.

-   **Secuencial ordenado:** almacena las filas manteniendo un orden en
    base a algún atributo propio del archivo, y se van asignando los
    bloques en forma consecutiva. En el ejemplo de la diapositiva están
    almacenados ordenados en forma alfabética por NOMBRE, y en bloques
    de tamaño 4.

### Diseño Físico -- Archivos secuenciales

Estos archivos **secuenciales** son recomendados en aplicaciones con:

-   alta tasa de volatilidad de inserción (escrituras al final del
    archivo)

-   alta tasa de actividad

-   particularmente ordenados, en consultas de rango

Se entiende por tasa de volatilidad a la cantidad de registros o filas
que son cambiados (inserciones, eliminaciones y actualizaciones) en la
BD en una cierta unidad de tiempo.

Se entiende por tasa de actividad a la cantidad de registros o filas que
son leídos desde la BD en una cierta unidad de tiempo.

Para definir un archivo secuencial desordenado en SQL se tiene:

> CREATE TABLE CLIENTE
> ( RUT char(9) primary key,
>
> Nombre varchar(35) not null,
>
> Direccion varchar(50)
>
> Sexo char);

#### Diseño Físico -- Archivos hashing (directos)

Lo contrario a una organización secuencial sería una organización
directa que permita encontrar un registro o fila sin necesidad de tener
que pasar por los que están almacenados antes de él como ocurre en una
organización secuencial.

Esto se logra gracias a que el archivo o tabla tiene un atributo como
clave primaria (PK), al cual se le aplica algún algoritmo *hashing* que
es capaz de transformar el valor de ese atributo en una dirección de
algún bloque dentro del área de almacenamiento, de tal manera de llegar
en forma más directa al registro o fila buscada.

En la diapositiva se presentan 2 ejemplos de tipos de archivos hashing
que es común sean manejados en todos los DBMS (se explicarán suponiendo
una tabla de temas musicales con los atributos NOMBRE TEMA, NOMBRE
INTERPRETE, NRO-ESTRENO, siendo este último PK):

-   **Hashing estático:** almacena las filas como van llegando en algún
    bloque que el algoritmo hashing le indique. El algoritmo requiere
    tener definido el tamaño total del área de datos (expresada en
    bloques) para poder generar direcciones dentro de esa área,
    teniéndose un número estático de bloques asignados desde que se crea
    el archivo. En el ejemplo de la diapositiva el área de datos es de
    tamaño 10 y cada bloque de tamaño 2.

-   **Hashing con expansión dinámica:** requiere de un área de datos y
    un directorio o área de índices. Se almacenan las filas como van
    llegando en algún bloque que el algoritmo hashing le indique. El
    algoritmo a través de un árbol binario maneja el área de índices,
    que le permite encontrar la ubicación de los bloques donde está cada
    registro y va asignando en forma dinámica nuevos bloques cuando el
    archivo va creciendo. En el ejemplo de la diapositiva el área de
    datos es de tamaño 6 y cada bloque de tamaño 4, además del área de
    directorio o índice que tiene un árbol de 3 niveles.

Estos archivos **hashing** son recomendados en aplicaciones con:

-   alta tasa de volatilidad, es específico los archivos con expansión
    dinámica

-   baja tasa de actividad

-   no sirve, normalmente, para salidas ordenadas

Para definir un archivo **hashing** en SQL se tiene:

> **CREATE CLUSTER** cliente (codigo number)
> ( RUT char(9) primary key,
>
> Nombre varchar(35) not null,
>
> Direccion varchar(50),
>
> sexo char)
>
> size 512 **single table hashkeys** 500;

Estos archivos permiten mejorar el desempeño en la recuperación de
datos. Son una alternativa a archivos secuenciales o a tablas con un
índice o con índice cluster, en cuyos casos se crea un índice separado
en base a algún valor clave (PK).

Al usar Hashing se crea un cluster hash y allí se cargan las tablas para
ser recuperadas en base a una función hashing (por ejemplo, Oracle usa
una función hash que genera una distribución de valores numéricos a
partir de los valores claves de un cluster específico).

La clave de un cluster hash puede ser una columna simple o varias
columnas.

Para almacenar o buscar una fila en el cluster hash, el DBMS aplica la
función hash al valor clave del cluster de la fila. El valor hash
resultante corresponde al bloque de datos en el cluster que se desea
leer o escribir.

En el ejemplo, se crea un cluster para la tabla CLIENTE a la cual se le
aplica un algoritmo hashing que da un valor numérico (código number)
para indicar dónde guardar el registro o fila dentro de un bloque. El
tamaño del cluster es de 512KB y el valor numérico (código number) tiene
como máximo 500.

### Diseño Físico -- Archivos organizados como árbol

En la figura se muestra un árbol donde cada nodo almacena una fila de la
tabla, y tiene dos punteros, uno hacia el lado izquierdo del árbol y
otro hacia el lado derecho. Es importante en esta organización de
archivos tener algún atributo que sea clave primaria (PK), y que los
datos sean almacenados en forma ordenada por ese atributo, de tal manera
que, al recorrer el árbol en búsqueda de alguna fila, se escoge el lado
izquierdo o derecho dependiendo si el valor buscado es menor o mayor al
valor que existe para la clave en el nodo. Se utilizan arboles B para
esta organización.

En el ejemplo de la figura, como PK hay un atributo que contiene las
letras del alfabeto, por lo que si buscáramos la fila que tiene como PK
el valor *j*, se comienza recorriendo el árbol desde la raíz, se compara
*j* con *m* (valor de la raíz) y de ahí dado que *j* es menor que *m*,
se pasa al nodo indicado por el puntero izquierdo del nodo raíz, de ahí
se sigue la búsqueda comparando con las diferentes filas almacenadas en
ese nodo, se compara con *c*, y como *j* es mayor que *c* se sigue
recorriendo el nodo, se compara con *f* y como *j* es mayor que *f*, se
sigue recorriendo el nodo, y se compara con *i*, y como *j* es mayor que
*i* se toma el puntero del lado derecho del nodo para ir al siguiente y
último nivel del árbol, donde encuentra la fila cuya PK es *j*.

Los archivos **organizados como árboles** son recomendados en
aplicaciones con:

-   alta volatilidad

-   búsquedas directas

Para definir un archivo de este tipo en SQL se tiene:

> create table cliente
>
> ( RUT char(9) primary key,
>
> Nombre varchar(35) not null,
>
> Direccion varchar(50),
>
> sexo char)
>
> **organization index**;

La cláusula ORGANIZATION INDEX es la que en SQL permite que las filas
sean almacenadas en una estructura tipo árbol, ordenadas en base a su
PK. A estas tablas se les conoce como IOT (Index-Organization Table).

### Diseño Físico -- Archivos organizados como Clusters

Los clusters permiten almacenar 2 o más tablas lo más cercanas posibles,
de tal manera de aprovechar el acceso que se hace al medio de
almacenamiento y así responder más rápidamente aquellas consultas que
requieran mínimos tiempos de respuestas.

Son usadas principalmente para tablas de lectura, cuyas filas requieren
ser usadas en conjunto para responder una consulta.

Por ejemplo, si se tienen las siguientes tablas EMPLEADO y DEPTO, se
puede usar cluster para responder en forma más rápida, la consulta sobre
cuáles son los empleados por cada departamento. Esto se hace agregando a
cada creación de la tabla en SQL la cláusula cluster como se indica a
continuación:

**EMPLEADO**

|RUT     |      |NOMBRE |NRO-DEPTO|……    |
|--------|------|-------|---------|------|
|111-1   |      |José   |20       |……    |
|222-2   |      |Isabel |10       |……    |
|333-3   |      |Simón  |20       |……    |
|444-4   |      |Alberto|20       |……    |
|555-5   |      |Jorge  |10       |……    |

```sql
create table empleado 
( RUT         char(9)   primary key, Nombre    varchar(35) not null,
  …
  NroDepto number(3) references depto
  cluster emp_dept (nroDepto);
```

**DEPTO**

|NRO-DEPTO||NOMBRE-D|CIUDAD   |
|---------|------|--------|---------|
|10       |      |VENTAS  |BOSTON   |
|20       |      |GERENCIA|NEW YORK |

```sql
create table depto
       (nroDepto number(3) primary key,
        … 
        cluster emp_dept (nroDepto);
```

De esta forma internamente quedan almacenado en un mismo lugar o cluster
las filas de las tablas de la siguiente manera: la fila completa con los
datos del Depto.Nro 10, y a continuación, todas las filas de los
empleados que pertenecen a ese Depto. Y así sucesivamente, es decir
queda cada fila de la tabla Depto. con todas las filas de los empleados
que pertenecen a ese departamento almacenados en un mismo cluster.

#### Diseño Físico -- Archivos organizados en particiones

Un archivo o tabla puede ser particionado en forma horizontal o
vertical, para reducir el tiempo de recuperación de sus datos al estar
éstos repartidos en partes de menor tamaño, y así no recargar la
consulta con tiempos de input/outputs innecesarios, ya que solo se
trabaja con aquellos datos más útiles para responderla. Incluso pueden
estar almacenadas las particiones en distintos discos.

Hay que recordar que una partición o fragmentación horizontal,
seleccionas filas (o tuplas) de una tabla que cumplen con una
determinada condición. Por ejemplo, particionar a los Empleados de tabla
anterior en base al Departamento al cuál pertenecen, es decir, se
separaría la tabla empleados en tantos fragmentos como departamentos
existan con empleados.

Por otra parte, una partición o fragmentación vertical, selecciona
columnas o atributos de una tabla conservando su PK. Por ejemplo, si se
supone que la tabla Empleados tiene muchos atributos tanto personales
como de la situación de salud de ellos, se podría particionar la tabla
Empleados en dos, en una se dejan sus atributos personales, y en otra
aquellos referidos a su situación de salud.

En esta diapositiva se grafica un ejemplo de particionamiento horizontal
para una tabla de Ventas. Suponer que esta tabla antes de ser
particionada tiene la siguiente estructura:

> VENTAS ([IdVendedor], NomVendedor, CantVendida, Sucursal,
FechaVenta)

El particionamiento horizontal, se podría realizar en base a tres
criterios diferentes:

1.  **Lista de valores:** se escogen filas en base a la región donde
    está la sucursal en la cual fue realizada la venta, siendo cada
    región definida en base a un conjunto de ciudades. Por ejemplo, se
    definen 3 particiones: Ventas Región Este (conformada por las ventas
    en las ciudades de New York, Virginia y Florida), Ventas Región
    Oeste (conformada por las ventas en las ciudades de California,
    Oregon y Hawaii, Ventas Región Centro (conformada por las ventas en
    las ciudades de Illinois, Textas y Missouri).

2.  **Rango de valores:** se escogen filas en base a un rango de valores
    asociados a las ventas. Por ejemplo, se definen particiones para
    control de ventas bimensuales: Ventas Enero-Febrero, Ventas
    Marzo-Abril, Ventas Mayo-Junio, Ventas Julio-Agosto.

3.  **Hash:** se aplica un algoritmo hashing para seleccionar las filas
    y almacenarlas en particiones, ese algoritmo debe ser usado también
    para recuperar una partición. En el ejemplo graficado en esta
    dispositiva, la tabla Ventas se divide en 4 particiones, las cuales
    se puede direccionar a través de h1, h2, h3, h4, que sería la
    dirección generada por el algoritmo.

Para crear 4 particiones horizontales en SQL según lista de valores para
la tabla Ventas en Chile se tendría:

```sql
create table ventas_por_listas 
(idVendedor           number(5), 
  nombreVendedor  varchar(30), 
  cantidadVendida   number(10), 
 sucursal            varchar(20),
 fechaVenta          date
)
```
```sql
partition by list(sucursal)
(partition ventas_norte_grande VALUES(‘Arica', ‘Iquique'),
  partition ventas_norte_chico VALUES (‘Copiapo´, …),
  partition ventas_centro VALUES(…),
  partition  ventas_sur VALUES(DEFAULT)
);
```

Para crear 4 particiones horizontales en SQL según claves hashing para
la tabla Ventas en Chile se tendría:

```sql
create table ventas_por_hashing
( idVendedor           number(5), 
  nombreVendedor  varchar(30), 
  cantidadVendida   number(10), 
  fechaVenta         date
)
```

Ahora vemos un ejemplo con combinación de las
diferentes técnicas de particionamiento.

Para la tabla Ventas se muestra una combinación de particionamiento por
rango y hash, y otra por rango y lista.

En el caso de rango y hash, se particionan las filas de la tabla según
rango de fechas (dimensión tiempo de los datos) generando las 4
particiones para ventas bimensuales y a esas ventas, se le aplica
algoritmo hash generando 4 direcciones de particiones (h1 a h4). Se
forma una matriz de 4x4.

Por otro lado, en el caso de rango y lista, se particionan las filas de
la tabla según rango de fechas (dimensión tiempo de los datos) y dentro
de ellas se agrupan según las regiones en donde se han realizado las
ventas (dimensión geográfica de los datos).


También es posible crear para cada partición generada, una estructura de
índices que permita llegar en forma más rápida a los datos. Es decir,
los archivos particionados tienen la posibilidad de tener índices
particionados (un área de índices por cada partición).

En la figura de esta diapositiva se muestra cómo los índices son un área
aparte dentro de los archivos, si bien ocupan más espacio, su ventaja es
que facilitan el acceso a los datos como se verá en el siguiente tema a
abordar.

## Diseño Físico - Índices

Esta diapositiva solo muestra título de subunidad Índices, que se
comenzará a estudiar.


Un índice es una estructura de datos adicional que se le agrega a un
archivo para agilizar el acceso a los datos contenidos en él. En ese
sentido al crear un índice para una tabla, se aumenta el espacio de
almacenamiento requerido, y se generan dos áreas de almacenamiento: área
de índice (aquí van los valores del atributo que actúa como índice y
punteros al área de datos) y área de datos (aquí van las filas con sus
valores).

Una analogía que se presta para entender el concepto de índices es la de
esas antiguas libretas de contacto que se usaban para guardar los datos
de nuestras amistades. Las hojas de esas libretas eran referenciadas a
través de las letras del alfabeto, permitiendo llegar en forma más
directa y rápida a un contacto, sin necesidad de ir avanzando hoja por
hoja. En el caso de un índice para un archivo, el índice equivale a la
letra del alfabeto, y el contenido de cada hoja son las filas de la
tabla indexada.

Se pueden identificar 3 tipos de índices para un archivo o tabla, según
el atributo sobre el cual se construye el índice:

-   **Índice Primario (Principal):** basado en el atributo sobre el cual
    se organiza el archivo de datos, siendo también clave única.

-   **Índice de Grupos:** ídem que el anterior, pero siendo clave no
    único.

-   **Índice Secundario:** especificado sobre un campo que no es usado
    para organizar un archivo.

Los DBMS los implementan en alguna de las siguientes formas:

-   Índice Dinámico

-   Índice *Bitmap*

**[Diapositiva 25]: Diseño Físico -- Índices Dinámicos**

En esta diapositiva se grafica un índice primario dinámico para un
atributo único (PK son las letras del alfabeto) de un archivo o tabla,
mostrando las dos áreas propias de cualquier índice:

-   **Área de Índice:** contiene un árbol B donde se almacena una
    entrada por cada fila de la tabla almacenada en el área de datos.
    Cada valor en el nodo del árbol tiene punteros que permiten navegar
    por el lado izquierdo o derecho, según sea el valor mayor o menor a
    lo almacenado en el árbol.

-   **Área de Datos:** contiene los valores de las filas de una tabla,
    agrupados en bloques de tamaño 3 (3 filas por cada bloque).

Se sugiere crear estos índices dinámicos para atributos únicos (del tipo
PK). Esto se hace a través del comando SQL CREATE TABLE, por ejemplo,
para una tabla Cliente con PK RUT sería:

> create table cliente
>
> ( RUT char(9) primary key,
>
> ... );
>
> **create index** RC on cliente(RUT);

### Diseño Físico -- Índices Bitmap

En esta diapositiva se grafica un índice bitmap para un atributo del
tipo secundario, es decir, para atributos no únicos e idealmente para
rangos reducidos de valores.

Por ejemplo, para una tabla Cliente con PK RUT y donde se quiere tener
acceso a los datos en forma más rápida según el atributo Género del
Cliente, se tendría un índice secundario para Género con 2 posibles
valores: hombre y mujer (a eso se refiere rango reducido, que la
cantidad de opciones para el atributo no sea muy grande). En SQL para
generar un índice bitmap para esta situación tendríamos:

> create table cliente
>
> ( RUT char(9) primary key,
>
> ...
>
> sexo char);
>
> **create bitmap index** RC
>
> on cliente(genero);

Para graficar de mejor forma cómo esto queda almacenado, pensemos en que
tenemos un área de índices y un área de datos como la grafica al lado
izquierdo de esta diapositiva. Además, suponer que el atributo para el
cual se desea definir un índice secundario es la región del cliente,
existiendo solo 6 valores posibles para ella (del 1 al 6).

En el área de índices, se deben tener 6 entradas, una para cada valor
posible del atributo secundario, y por cada valor, un conjunto de bits
que indican si en el área de datos asociada, existe o no una fila con
ese valor (si existe habrá un 1, si no existe habrá un 0). El área de
datos es organizada en bloques (o páginas), en nuestro ejemplo tenemos 4
páginas, por lo cual en el índice se hace referencia a cada página, para
ello cada 0 o 1 del índice está haciendo referencia a cada página.

De esta manera, si pensamos que andamos en la búsqueda de los clientes
de la 5ta región, bastaría acceder al índice, recorrer secuencialmente
hasta encontrar el valor 5, y ahí analizar los bits, los cuales
indicarían que en el bloque 1 y 2 solamente, hay clientes de la 5ta
región, por lo cual solo se accedería a ellos.

Suponer un ejemplo más complejo, donde se quiere dejar en claro la
posibilidad que dan estos índices para mejora los tiempos de respuestas,
a veces sin necesidad de tener que ir al área de datos, es decir,
solamente analizando la situación al nivel del área de índices.

El ejemplo detalla los siguientes atributos para una tabla de clientes:

> CLIENTE ([Nro-Cliente], EstadoCivil, Region, Genero,
Nivel-Ingresos)

Su área de índices y datos serían:

  **AREA ÍNDICE**                                                                          
  |East |0      |0        |
|-----|-------|---------|
|Central|1      |0        |
|West |0      |1        |

**AREA DATOS**              
|Nro-Clte|EstadoCiv|Region |Genero   |Nivel-Ing|
|--------|---------|-------|---------|---------|
|101     |single   |east   |male     |bracket1 |
|102     |married  |central|female   |bracket4 |
|103     |married  |west   |female   |bracket2 |
|104     |divorced |west   |male     |bracket4 |
|105     |single   |central|female   |bracket2 |
|106     |married  |central|female   |bracket3 |


La siguiente consulta SQL podría ser respondida con solo acceder al área
de índices, lo cual reduce enormemente los tiempos de respuesta, y
demuestra las fortalezas de este tipo de índice:

```sql
SELECT COUNT(\*)
FROM CLIENTE
WHERE ESTADOCIV = *married* AND REGION IN (*central*,*west*);
```

En la siguiente tabla se pueden ver las combinaciones de 0 y 1 que se
evalúan en el índice para responder esta consulta:

![Índices usados](imagenes/indices1.png "Índices usados")

## Diseño Físico -- Método para BDR


Para sintetizar lo estudiado en este capítulo, en estas dos últimas
diapositivas, se entregan un conjunto de recomendaciones (o método) para
tener en cuenta cuando se deben tomar decisiones de diseño físico para
una base de datos relacional, pensando en mejorar los indicadores de
rendimiento, tiempo de respuesta y utilización de espacio de
almacenamiento.

El método por aplicar sugiere:

-   Escoger un atributo que sea frecuentemente usado para recuperar los
    registros en orden, o para realizar operaciones (Join), como el
    campo de ordenamiento del archivo, y crear un índice primario o de
    grupos, según corresponda.

-   En caso de que ningún atributo cumpla con las condiciones
    anteriores, mantener el archivo desordenado.

-   Por cada atributo (que no sea el campo de ordenamiento), que es
    usado en operaciones de búsqueda o Join, especificar un índice que
    sirva como índice secundario al archivo de datos.

-   Si el archivo va a ser actualizado frecuentemente, con operaciones
    de inserción y eliminación de registros (alta volatilidad), tratar
    de reducir el número de índices del archivo de datos.

-   Si un atributo es usado frecuentemente para operaciones de selección
    y Join, pero no para recuperar los registros en orden, un archivo
    tipo *hashing* puede ser usado, considerando:

    -   Estático puede ser usado para archivos cuyo tamaño no varíe
        mucho.

    -   Con Expansión Dinámica es requerido cuando el archivo sufrirá
        variaciones importantes en su volumen.

-   Si el archivo tiene pocos registros, y éstos son de tamaño pequeño,
    podría considerarse una organización de tipo árbol.
