# UNIDAD 3: Diseño lógico BDR -- Enfoque Top-Down

Para entender el enfoque top-down de diseño de BD relacionales, es
necesario tener en mente la diferencia entre top-down y bottom-up en el
diseño de BD. Esta diferencia reside en el punto de partida, es decir,
mientras que en el enfoque bottom-up se parte de los datos representados
en una forma muy cercana a cómo el usuario los maneja (vistas de
usuarios), en el top-down se parte de los datos ya modelados (desde algo
más general o top). El punto final para ambos enfoques es el mismo: un
modelo de datos lógico (o relacional).

Cabe mencionar que bottom-up es ir de abajo hacia arriba, desde de algo
detallado o bottom hacia arriba. En cambio, top-down es ir de arriba
hacia abajo, desde algo más general o top hacia el detalle.

En nuestro caso de diseño de BD relacionales, el enfoque top-down
implica partir desde un modelo de datos conceptual para llegar a un
modelo de datos lógico (o relacional). Para lograr esta transformación
es necesario la aplicación de un conjunto de reglas bien definidas, a
través de 8 pasos.

Para efectos del curso, se hará uso de un modelo conceptual expresado en
un diagrama de clases, para explicar el método, y reforzar los diagramas
de clases.

## Enfoque Top-Down -- Los 8 pasos

1.  Tipos de Entidades *Fuertes*

2.  Tipos de Entidades *Débiles*

3.  Asociaciones 1:1

4.  Asociaciones 1:N

5.  Asociaciones M:N

6.  Asociaciones n-arias (n ≥ 3)

7.  Herencia

8.  Categorización (Interfaces o Herencia Selectiva)

Cabe mencionar que, en un modelo de datos conceptual, no necesariamente
deben estar todos estos pasos presentes, todo depende de la realidad que
se está representando; así es que lo importante es entender bien la
realidad que está representada en el modelo de datos conceptual e ir
aplicando estos pasos. Por este motivo se dice que este enfoque top-down
conviene usarlo cuando se tiene un mayor conocimiento de la realidad a
modelar, en cambio el bottom-up ayuda más cuando no se conoce tan
profundamente la realidad.

Cada uno de estos pasos se explicarán en las siguientes líneas,
apoyándonos en el ejemplo del modelo de datos conceptual para el caso de
Los Bomberos estudiado en la Unidad 2 de esta asignatura.

### Paso 1: Tipos de entidades *Fuertes*

Se define a una clase de entidad fuerte como aquella que no depende su existencia de ninguna otra entidad, es decir, no tiene un padre en una relación padre-hijo.

Este paso implica observar el modelo conceptual y por cada tipo de entidad fuerte identificada, crear una relación o tabla que incluya
todos los atributos simples (o atómicos) de ella. En caso de existir un atributo compuesto, se debe transformar a atributos atómicos.

Además, este paso implica escoger uno de los atributos claves de esta entidad fuerte como clave primaria en la relación o tabla correspondiente. Si la clave escogida es compuesta, el conjunto de atributos simples que la conforman serán parte de la clave primaria de la relación o tabla.

Por ejemplo, si en el modelo de datos conceptual existe una entidad con 3 atributos, siendo uno de ellos su identificador, la transformación al modelo relacional es directa, quedan los 3 atributos y el identificador se convierte en la PK.

Si tomamos como ejemplo el modelo de datos conceptual para el caso de Los Bomberos, tendríamos como entidad fuerte a CAUSA y UBICACIÓN, quedando en un modelo de datos lógico relacional (donde PK va subrayada) como:

CAUSA ([ID-CAUSA], NOMBRE-CAUSA, TIPO-CAUSA)

UBICACIÓN ([ID-UBICACIÓN], NOMBRE, TIPO-UBICACIÓN)

### Paso 2: Tipos de entidades *Débiles*

En este enfoque se define a una clase de entidad débil como aquella que no tiene un identificador que por sí sólo le permita \*identificarse\* (no tiene lo necesario para diferenciar una entidad de otra).

Esto no es lo mismo que la semántica asociada a la \*debilidad\* por existencia (una entidad no puede existir sin la presencia de otra, lo cual se debe manejar con las cardinalidades mínimas 1); aquí de lo que se trata es una \*debilidad\* por identificador.

Este paso implica que, por cada tipo de entidad débil del modelo conceptual, dependiente de una entidad fuerte, se debe crear una relación o tabla que incluya los atributos simples, los atributos simples de los posibles atributos compuestos que existan, y los atributos que formen parte de la clave primaria de la entidad fuerte.

Además, se debe dejar como clave primaria de la relación o tabla, a la concatenación de la clave primaria de la entidad fuerte con el identificador de la entidad débil.

Por ejemplo, si en el modelo de datos conceptual existe una entidad fuerte y una entidad débil asociada a ella, cada una con su correspondiente identificador, la transformación al modelo relacional implica que solo se transforma la entidad débil, quedando con una PK compuesta del identificador de la entidad fuerte más el identificador de la entidad débil. Si recordamos el modelo de datos conceptual de Los Bomberos, tendríamos que no existe esta situación en ese modelo.

### Enfoque Top-Down -- Paso 3: Asociaciones 1:1

Ya sabemos lo que representa una asociación 1:1, al detectar una asociación de este tipo en el modelo de datos conceptual, la transformación que este paso propone es la siguiente:

Por cada asociación binaria 1:1, se deben identificar las relaciones o tablas del modelo relacional para lo cual se debe escoger una de las tablas e incluir su clave primaria como foránea en la otra relación. ¿Cuál escoger? Depende desde donde sea más frecuente el acceso para extraer sus datos.

Por ejemplo, si en el modelo de datos conceptual existe una asociación 1:1 entre entidades A y B, donde cada una tiene su propia PK, en el modelo relacional existen 2 alternativas para implementarla. En una se deja la PK de la entidad A como FK en la entidad B; y en la otra alternativa, se deja la PK de la entidad B como FK en la entidad A.

Si tomamos como ejemplo el modelo de datos conceptual para el caso de Los Bomberos, tendríamos una asociación 1:1 entre las entidades CASA y SEGURO-INCENDIO, quedando en un modelo de datos lógico relacional (donde
PK va subrayada y FK en cursiva) en alguna de las siguientes alternativas:

**[Alternativa 1:]**

> CASA ([ROL-PROPIEDAD], TIPO-CASA)

> SEGURO-INCENDIO ([ID-SEGURO], ASEGURADORA, VIGENCIA, COBERTURA,
*ROL-PROPIEDAD*)

**[Alternativa 2:]**

> CASA ([ROL-PROPIEDAD], TIPO-CASA, *ID-SEGURO*)

> SEGURO-INCENDIO ([ID-SEGURO], ASEGURADORA, VIGENCIA, COBERTURA)

¿Cuál escoger? Alternativa 2, suponiendo que los Bomberos la información
que más necesitan es saber si una casa tiene o no seguro, y con esta
alternativa basta acceder a CASA y con el valor que exista en ID-SEGURO,
se determina de inmediato la respuesta buscada (si es NULL no hay
seguro).

### Enfoque Top-Down -- Paso 4: Asociaciones 1:N

Ya sabemos lo que representa una asociación 1:N, al detectar una
asociación de este tipo en el modelo de datos conceptual, la
transformación que este paso propone es la siguiente:

Por cada asociación binaria 1:N (no débil), se debe identificar la
relación o tabla S que está en el lado de la cardinalidad *muchos*, e
incluir en ésta como clave foránea, la clave primaria de la entidad con
cardinalidad 1 (bajo esta asociación).

Por ejemplo, si en el modelo de datos conceptual existe una asociación
1:N entre entidades A y S, donde S está en el lado de la cardinalidad
*muchos*, en el modelo relacional se debe incluir en S como FK la PK de
la entidad A.

Si recordamos el modelo conceptual para el caso de Los Bomberos,
tendríamos asociación 1:N entre las entidades CAUSA e INCENDIO y entre
UBICACIÓN e INCENDIO, quedando la transformación a un modelo de datos
lógico relacional (donde PK va subrayada y FK en cursiva) como:

> INCENDIO ([ID-INCENDIO], HORA-INICIO, DESCRIPCION, *ID-CAUSA*,
*ID-UBICACIÓN*)

![Asociación 1..N](imagenes/asociacion_1N.png "Asociación 1..N")

### Enfoque Top-Down -- Paso 5: Asociaciones M:N

Ya sabemos lo que representa una asociación M:N, al detectar una
asociación de este tipo en el modelo de datos conceptual, la
transformación que este paso propone es la siguiente:

Por cada asociación binaria M:N, se debe crear una nueva relación o
tabla que la represente (tabla de intersección). Incluir como claves
foráneas a las claves primarias de las relaciones o tablas que
participan en la asociación. Además, incluir cualquier atributo simple
de la asociación.

Por ejemplo, si en el modelo de datos conceptual existe una asociación
M:N entre entidades A y B, siendo la asociación una entidad de
intersección llamada AB, en el modelo relacional se debe incluir una
tabla AB que tenga como FK las PK de las entidades A y B, y todos los
atributos de AB, si es que tiene).

Si recordamos el modelo conceptual para el caso de Los Bomberos,
tendríamos asociación M:N entre INCENDIO y CASA, entre CASA Y
DAMNIFICADO y entre INCENDIO y CAUSANTE, quedando la transformación a un
modelo de datos lógico relacional (donde PK va subrayada y FK en
cursiva) como:

> DAÑA ([ID-INCENDIO, ROL-PROPIEDAD])

> AYUDA ([ID-AYUDA], *ROL-PROPIEDAD, RUT-DAMNIFICADO*, TIPO-DAÑO)

> CASTIGO-JUDICIAL ([ID-CASTIGO], *ID-INCENDIO, RUT-CAUSANTE*, RESOLUCION)

### Enfoque Top-Down -- Paso 6: Asociaciones n-arias (n\>=3)

Ya sabemos lo que representa una asociación n-aria, donde lo más común
es que sea ternaria, al detectar una asociación de este tipo en el
modelo de datos conceptual, la transformación que este paso propone es
la siguiente:

Por cada asociación de grado 3 o más, crear una nueva relación o tabla
para representarla. Incluir como claves foráneas a las claves primarias
de las relaciones o tablas que representan a los tipos de entidades que
participan en dicha asociación; normalmente, la concatenación de estas
claves es la clave primaria de la nueva relación o tabla.

### Enfoque Top-Down -- Paso 6: Asociaciones n-arias (n\>=3) - Ejemplo

Por ejemplo, si en el modelo de datos conceptual existe una asociación
ternaria entre entidades A, B y C, siendo la asociación una entidad de
intersección llamada ABC, en el modelo relacional se debe incluir una
tabla ABC que tenga como FK las PK de las entidades A, B y C; siendo
además muy común, que estas FK en forma concatenada, formen la PK
compuesta de ABC. Se deben incluir además todos los atributos de ABC, si
es que tiene.

Si revisamos el modelo conceptual para el caso de Los Bomberos,
tendríamos que no exoste este tipo de asociación.

![Asociación N>3](imagenes/asociaciones_3N.png "Asociación N>3")

### Enfoque Top-Down -- Paso 7: Herencia

Ya sabemos lo que significa la semántica de herencia, y que existen
varias posibilidades al implementarla en un modelo relacional, por ello
se explicará a través de un ejemplo, las diferentes alternativas de
transformación que este paso plantea al detectar herencia en un modelo
conceptual.

Este ejemplo considera una herencia dada por la superclase A(IdA, A1)
donde IdA es su identificador, y las subclases B(IdB, B1) y C(IdC, C1)
donde IdB e IdC son sus identificadores respectivamente. 

**Alternativa 1:** crear una relación o tabla para la superclase con
todos los atributos identificados en el modelo conceptual. Crear una relación o tabla por cada subclase, con todos los atributos propios de la subclase más el identificador de la superclase, quedando éste como la
clave primaria de la relación.

Es decir, en el modelo relacional, quedan 3 tablas: A, B y C. La tabla A queda igual que en el modelo conceptual, y las tablas B y C, agregan como FK, la PK de A, para poder implementar la herencia.

![Asociación Herencia](imagenes/asociaciones_herencia.png "Asociación herencia")

Si consideramos el modelo conceptual para el caso de Los Bomberos,
tenemos herencia entre PERSONA con DAMNIFICADO y CAUSANTE. Esta
Alternativa 1 propone las siguientes tablas:

> PERSONA ([RUT], NOMBRE, DOMICILIO)

> DAMINIFICADO (*[RUT-DAMNIFICADO]*, TIPO-DAÑO)

> CAUSANTE ((*[RUT-CAUSANTE]*, TIPO-PARTICIPACION)


**Alternativa 2:** crear una relación o tabla por cada subclase,
incluyendo en cada una de ellas los atributos de la superclase y los
atributos propios de la subclase correspondiente, dejando al
identificador de la superclase como la clave primaria de la relación.

Es decir, en el modelo relacional, quedan 2 tablas: B y C. Los atributos
de la tabla A se incorporan a las tablas B y C, quedando como PK de
estas 2 tablas, el identificador que se tenía en A.

Si consideramos el modelo conceptual para el caso de Los Bomberos,
tenemos que la herencia entre PERSONA con DAMNIFICADO y CAUSANTE con la
alternativa 2 genera las siguientes tablas:

> DAMINIFICADO ([RUT-DAMNIFICADO], NOMBRE, DOMICILIO, TIPO-DAÑO)

> CAUSANTE ([RUT-CAUSANTE], NOMBRE, DOMICILIO, TIPO-PARTICIPACION)

**Alternativa 3:** crear una única relación o tabla que contenga todos
los atributos de la superclase y todos los atributos pertenecientes a
cada una de las subclases, más un atributo adicional que permita
determinar a qué subclase pertenece cierta entidad. La clave primaria es
el identificador de la superclase.

Es decir, en el modelo relacional, queda una sola tabla, que representa
a la superclase más todos los atributos de las subclases y un atributo
adicional que permite diferenciar a cada tipo de subclase.

Si consideramos el modelo conceptual para el caso de Los Bomberos,
tenemos que la herencia entre PERSONA con DAMNIFICADO y CAUSANTE con la
alternativa 3 genera la siguiente tabla:

> PERSONA ([RUT], NOMBRE, DOMICILIO, TIPO-DAÑO, TIPO-PARTICIPACION,
TIPO-PERSONA)

### Enfoque Top-Down -- Paso 7: Herencia

**Alternativa 4:** crear una única relación o tabla que contenga todos
los atributos de la superclase y todos los atributos pertenecientes a
cada una de las subclases, más un conjunto de atributos adicionales (de
tipo booleano) que permitan determinar a qué subclases pertenece cierta
entidad.

Es decir, en el modelo relacional, queda una sola tabla, que representa
a la superclase más todos los atributos de las subclases y un atributo
adicional (booleano) por cada subclase existente, que permite
diferenciar una de otra.

Si consideramos el modelo conceptual para el caso de Los Bomberos,
tenemos que la herencia entre PERSONA con DAMNIFICADO y CAUSANTE con la
alternativa 4 genera la siguiente tabla:

> PERSONA ([RUT], NOMBRE, DOMICILIO, TIPO-DAÑO, TIPO-PARTICIPACION,
TIPO-DAMNIFI,TIPO-CAUSANTE)

### Enfoque Top-Down -- Paso 8: Categorización (o Herencia Selectiva)

Para el caso de que las superclases tengan diferentes identificadores, se debe crear una clave primaria especial, que se denomina **clave sustituta**, para la relación o tabla que representa la categoría; a ésta se le incluyen los atributos definidos en la categoría en el modelo conceptual.

![Asociación herencia selectiva](imagenes/asociaciones_hs.png "Asociación herencia selectiva")

Finalmente, a cada relación o tabla asociada con una superclase de la categoría, se le agrega como clave foránea la clave sustituta.

Por ejemplo, si en el modelo de datos conceptual existe una categorización dada por B y C como superclases, donde cada una tiene su propia PK, y con A que representa la categoría; en el modelo relacional esto se implementa creando una clave primaria sustituta en A; y esa clave se agrega como FK a las superclases B y C.

Si recordamos el modelo de datos conceptual de Los Bomberos, tendríamos que no existe esta situación en ese modelo.

**RESUMEN MODELO DE DATOS LOGICO PARA CASO DE LOS BOMBEROS SEGÚN LOS 8
PASOS DEL ENFOQUE TOP-DOWN**

> CAUSA ([ID-CAUSA], NOMBRE-CAUSA, TIPO-CAUSA)

> UBICACIÓN ([ID-UBICACIÓN], NOMBRE, TIPO-UBICACIÓN)

> CASA ([ROL-PROPIEDAD], TIPO-CASA, *ID-SEGURO*)

> SEGURO-INCENDIO ([ID-SEGURO], ASEGURADORA, VIGENCIA, COBERTURA)

> INCENDIO ([ID-INCENDIO], HORA-INICIO, DESCRIPCION, *ID-CAUSA*,
*ID-UBICACIÓN*)

> DAÑA ([ID-INCENDIO, ROL-PROPIEDAD])

> AYUDA ([ID-AYUDA], *ROL-PROPIEDAD, RUT-DAMNIFICADO*, TIPO-DAÑO)

> CASTIGO-JUDICIAL ([ID-CASTIGO], *ID-INCENDIO, RUT-CAUSANTE*,
RESOLUCION)

Y para implementar la herencia se presentan 4 alternativas:

Alternativa1:

> PERSONA ([RUT], NOMBRE, DOMICILIO)

> DAMINIFICADO (*[RUT-DAMNIFICADO]*, TIPO-DAÑO)

> CAUSANTE ((*[RUT-CAUSANTE]*, TIPO-PARTICIPACION)

Alternativa2:

> DAMINIFICADO ([RUT-DAMNIFICADO], NOMBRE, DOMICILIO, TIPO-DAÑO)

> CAUSANTE ([RUT-CAUSANTE], NOMBRE, DOMICILIO, TIPO-PARTICIPACION)

Alternativa3:

> PERSONA ([RUT], NOMBRE, DOMICILIO, TIPO-DAÑO, TIPO-PARTICIPACION, TIPO-PERSONA)

Alternativa4:

> PERSONA ([RUT], NOMBRE, DOMICILIO, TIPO-DAÑO, TIPO-PARTICIPACION, TIPO-DAMNIFI,TIPO-CAUSANTE)

¿Cuál es la mejor? Todo depende de los requerimientos de información y
del equilibrio entre tiempo de respuesta y espacio de almacenamiento.


##  UNIDAD 3: Diseño lógico BDR -- Enfoque Top-Down: Ejemplo

En esta sesión se partirá haciendo un nuevo ejercicio sobre el enfoque top-down, se trabajará con el Caso ONEMI.

La ONEMI (Oficina Nacional de Emergencia del Ministerio del Interior) es el organismo técnico del Estado de Chile encargado de planificar, impulsar, articular y ejecutar acciones de prevención, respuesta y rehabilitación frente a catástrofes que ocurren en el país.

Suponer que tienen un modelo de datos conceptual, expresado como un diagrama de clases donde se representen todas las catástrofes y la lista de ciudades afectadas. Además, se clasifican las catástrofes según si son de origen natural o provocadas por la acción humana. Si son de origen natural, se registra la causa que la originó y el tipo de alerta que se generó. Si son provocadas por la acción humana, se registra cuántas personas fueron las que la provocaron y si se hizo o no la denuncia judicial respectiva; en caso de que se haya hecho la denuncia se necesitan los datos del juzgado donde se realizó. Se pide obtener el modelo de datos lógico (relacional) aplicando para ello los 8 pasos del enfoque top-down.


### Otras Formas Normales

Llegar a 3FN se considera como bastante razonable para lograr un diseño
de una BDR que minimice la redundancia y las anomalías de mantención.

Sin embargo, puede haber realidades que a pesar de estar en 3FN sigan
mostrando dependencias funcionales que generen redundancia y anomalías
de mantención. Por ello en la actualidad se reconocen hasta 8FN (6+2):
1FN, 2FN, 3FN, BCFN (Boyce/Codd), 4FN, 5FN, 6FN, DKFN (Domain/Key) y se
agrega el concepto de desnormalización.

Veremos adicionalmente a las 3 primeras ya estudiadas, las formas
normales de Boyce y Codd (nombre de los autores de esta regla) y la 4FN,
más que nada para dejar en claro que toda nueva FN sigue el mismo
comportamiento *dividir para reinar* para ir eliminando anomalías de
mantención. También profundizaremos en la desnormalización como un
concepto que ayuda a encontrar un equilibrio entre tiempos de respuestas
y minimizar redundancias y anomalías mantención.

Para estar en BC FN se debe estar en 3FN y, además, haber eliminado
anomalías que quedan por la existencia de dependencias funcionales no
triviales que se producen por tener más de una PK. Para estar en 4FN se
debe estar en BCFN y, además, haber eliminado las dependencias
multivaluadas.

#### Otras Formas Normales -- Boyce/Codd FN

Cuando una entidad tiene más de una **clave candidata**, es posible que
aún existan anomalías en 3FN (*overlapping de claves*). Es una situación
rara, pero puede ocurrir.

Una clave candidata, es un atributo o conjunto de atributos, que podría
ser considerado como PK, es decir, estamos frente a la situación que la
entidad tiene más de una alternativa para PK.

Boyce y Codd proponen para eliminar esa anomalía que se genera por la
existencia de **dependencias funcionales** no triviales, lo siguiente:
*Una Relación R está en BC FN, si y sólo si está en 3FN y cada **determinante** es clave candidata*.

Hay que recordar que relación es sinónimo de entidad o tabla, y que para
expresar una dependencia funcional se utiliza la siguiente escritura: A
→ B y se lee: *A determina a B* o *B es funcionalmente dependiente de A*
siendo A un determinante.

En síntesis, hay 3 conceptos esenciales para entender esta forma normal:

-   Clave candidata

-   Dependencias funcionales

-   Determinante

Los cuales en forma simple se relacionan así: para estar en BCFN se debe
estar en 3FN y todos los determinantes de las dependencias funcionales
deben ser posibles PK. 

### Otras Formas Normales -- Ejemplo Boyce/Codd FN

Pensemos en el caso de una Universidad, donde los alumnos inscriben
materias de su interés con connotados tutores, teniendo en cuenta los
siguientes supuestos (o reglas del negocio):

1.  Cada alumno puede tener varias materias de interés.

2.  Por cada materia, un alumno tiene sólo un tutor.

3.  Cada materia tiene varios tutores.

4.  Cada tutor orienta sólo en una materia.

5.  Cada tutor orienta a varios estudiantes en una materia.

Imaginemos que después de normalizar a 3FN, se llegó a la siguiente
tabla:

> TUTORÍAS (Nro-Alumno, Materia, Tutor)

¿Cuál(es) sería la PK? ¿Esta tabla está en BCFN?

Suponer que la tabla TUTORIAS tiene los siguientes valores:

|Nro-Alumno|Materia |Tutor   |
|----------|--------|--------|
|123       |Física  |Einstein|
|123       |Música  |Mozart  |
|456       |Biología|Darwin  |
|789       |Física  |Bohr    |
|999       |Física  |Einstein|


Para determinar si la tabla TUTORIAS está en B/C FN es importante
identificar los 3 conceptos esenciales indicados anteriormente:

-   **Clave candidata** a ser PK, en TUTORIAS existen 2 claves
    alternativas:

    a.  Nro-Alumno, Materia

    b.  Nro-Alumno, Tutor

-   **Dependencias funcionales**, en TUTORIAS existen las siguientes
    dependencias:

    a.  Nro-Alumno, Tutor → Materia

    b.  Nro-Alumno, Materia → Tutor

    c.  Tutor → Materia

-   **Determinante**, son los atributos del lado izquierdo de las
    expresiones anteriores, es decir:

    a.  Nro-Alumno, Tutor: en TUTORIAS está combinación de atributos
        puede ser PK.

    b.  Nro-Alumno, Materia: en TUTORIAS está combinación de atributos
        puede ser PK.

    c.  Tutor: en TUTORIAS este atributo no es posible sea PK.

Dado que existe un determinante que no es clave candidata, TUTORIAS no
está en B/C FN. Para que esté en B/CFN todos los determinantes deben ser
candidatos a ser PK en la tabla. Esto se resuelve dividiendo la tabla
TUTORIAS, de tal manera que la dependencia funcional no trivial que
queda dada por Tutor → Materia, no debe ir en TUTORIAS. Es decir, para
pasar TUTORIAS a BCFN se generaría la tabla ALUMNO-TUTOR con PK
compuesta, y con FK TUTOR, y la tabla TUTORES con PK Tutor:


**ALUMNO-TUTOR**
|Nro-Alumno|Tutor   |
|----------|--------|
|123       |Einstein|
|123       |Mozart  |
|456       |Darwin  |
|789       |Bohr    |
|999       |Einstein|


**TUTORES**
|Tutor|Materia |
|-----|--------|
|Einstein|Física  |
|Mozart|Música  |
|Darwin|Biología|
|Bohr |Física  |


#### Otras Formas Normales -- 4FN

Cuando una entidad está en B/C FN, puede tener anomalías con respecto a
las dependencias multivaluadas (DM).

Una DM existe cuando hay al menos tres atributos (A, B, C) en una
relación R, y para cada valor de A existe un conjunto de valores de B y
de C definidos, pero independientes entre sí.

Hay que recordar que relación es sinónimo de entidad o tabla, y precisar
que para estar en 4FN se deben eliminar las dependencias multivaluadas,
que se dan cuando existen 2 o más asociaciones M:N independientes entre
sí, dentro de una misma tabla, lo que causa redundancia y anomalías de
mantención. Es una situación poco frecuente, pero posible. 

#### Otras Formas Normales -- Ejemplo 4FN

Pensemos en el caso de una Universidad, donde se tiene una tabla en B/C
FN en la que se reflejan las asignaturas dictadas por los profesores y,
además, los libros o textos guías que están definidos en los programas
de las asignaturas, siendo éstos independientes del profesor, ya que
fueron definidos cuando el programa de la asignatura entró en vigencia.

Teniendo en cuenta los siguientes supuestos (o reglas del negocio):

1.  Cada asignatura puede tener varios profesores.

2.  Cada asignatura utiliza varios textos.

3.  El texto utilizado para una cierta asignatura es independiente del
    profesor.

Y que se cuenta con la siguiente tabla ASIGNATURAS que están en B/C FN:

> ASIGNATURAS (Nombre-Asignatura, Profesor, Libro) ¿Cuál(es) sería la PK?


Suponer que la vista no normalizada inicial tiene los siguientes valores:

|Nom-Asignatura|Profesor|Libro         |
|--------------|--------|--------------|
|Administración|White Green Black|Drucker Peters|
|Finanzas      |Gray    |Weston Gilford|


Al llevar la vista anterior a B/C FN se obtiene la siguiente tabla
ASIGNATURA:

**ASIGNATURA**
|Nom-Asignatura|Profesor|Libro         |
|--------------|--------|--------------|
|Administración|White   |Drucker       |
|Administración|White   |Peters        |
|Administración|Green   |Drucker       |
|Administración|Green   |Peters        |
|Administración|Black   |Drucker       |
|Administración|Black   |Peters        |
|Finanzas      |Gray    |Weston        |
|Finanzas      |Gray    |Gilford       |


En esta tabla de detectan dependencias multivaluadas, pues hay al menos
3 atributos y para cada valor de uno de ellos (NOM-ASIGNATURA) hay un
conjunto de valores en los otros 2 atributos (PROFESOR y LIBRO) bien
definidos, pero independientes entre sí. Es decir, por cada asignatura
hay un conjunto de profesores y un conjunto de libros asignados, pero
los profesores no deciden qué libro usar, ellos son independientes de
los libros.

Se puede visibilizar que en la tabla ASIGNATURA, existe redundancia y
anomalías de mantención: ¿qué pasa si se cambia el libro de Drucker que
se usa en la asignatura de Administración?

Para resolver estos problemas es necesario pasar la tabla ASIGNATURA de
B/C FN a 4FN, dividiéndola en: ASIGNATURA-PROFESOR y ASIGNATURA-LIBRO.

**ASIGNATURA-PROFESOR** 
|Nom-Asignatura|Profesor|
|--------------|--------|
|Administración|White   |
|Administración|Green   |
|Administración|Black   |
|Finanzas      |Gray    |

**ASIGNATURA-LIBRO**
|Nom-Asignatura|Libro   |
|--------------|--------|
|Administración|Drucker |
|Administración|Peters  |
|Finanzas      |Weston  |
|Finanzas      |Gilford |


Ahora ya se está en 4FN.

### Otras Formas Normales -- Desnormalización

Al ir normalizando, se va pasando por cada FN y se van obteniendo más y
más tablas, esto si bien resuelve los temas de redundancia y de
anomalías de mantención, aumenta los tiempos de respuesta, ya que para
obtener alguna información (como la contenida en las vistas del enfoque
bottom-up) es necesario realizar un acceso a varias tablas, incluyendo
JOINs de tablas, de tal manera de poder integrar todos los datos que se
requieren para obtener la información.

Para reducir los tiempos de respuestas, se utiliza la desnormalización,
o conjunto de técnicas destinadas a mejorar los tiempos de respuesta,
agregando redundancia a un modelo relacional. Normalmente, lo que se
busca es evitar una o más operaciones JOINs entre los archivos
requeridos para responder alguna consulta.


### Otras Formas Normales -- Desnormalización -- Valores derivados

Al almacenar atributos derivables en una BD se están mejorando los
tiempos de respuestas en el sentido que se ahorrará el acceso a otras
tablas para ir a calcular el valor en sí. En estricto rigor se prefiere
perder espacio, pero ahorra tiempo de búsqueda y de cálculo.

Esta estrategia consiste en agregar una columna extra para almacenar
datos derivados a partir de otros ya presentes en el modelo de datos.

Por ejemplo, si se considera el siguiente **modelo relacional normalizado**, donde PK va subrayada y FK en cursiva:

> FACTURA ([NRO-FACT], FECHA, *RUT-CLIENTE*)

> DETALLE ([*NRO-FACT*, *COD-PROD*], CANTIDAD, SUBTOTAL)

Un **modelo relacional desnormalizado** al almacenar valores derivados (TOTAL) sería:

> FACTURA ([NRO-FACT], FECHA, *RUT-CLIENTE*, TOTAL)

> DETALLE ([*NRO-FACT*, *COD-PROD*], CANTIDAD, SUBTOTAL)

Al agregar TOTAL se ahorra el tiempo de hacer un JOIN entre FACTURA y
DETALLE y obtener todos los subtotales asociados a una factura y
sumarlos. Ahora solo basta con el acceso a FACTURA para obtener el
TOTAL, solo se leerá una fila.

#### Otras Formas Normales -- Desnormalización -- Tablas prejoinizadas (volver a 2FN)

Las tablas prejoinizadas implican volver de 3FN a 2FN, es decir, se
ahorran JOINs asociados a las dependencias transitivas, y por ende se
reducen los tiempos de respuestas. Suelen implicar eliminar tablas de
códigos, donde sus columnas son un ID y algún atributo descriptivo del
código.

Por ejemplo, si se considera el siguiente **modelo relacional normalizado**, donde PK va subrayada y FK en cursiva:

> CARRERA ([CODIGO-CARRERA], NOMBRE-CARRERA)

> ALUMNO ([RUT], NOMBRE-ALUMNO, DOMICILIO, *CODIGO-CARRERA*)

Un **modelo relacional desnormalizado** por tablas prejoinizadas sería:

> ALUMNO ([RUT], NOMBRE-ALUMNO, DOMICILIO, NOMBRE-CARRERA)

### Otras Formas Normales -- Desnormalización -- Repitiendo detalle (volver a 1FN)

Esta estrategia de desnormalización, implica repetir algún atributo en
el archivo denominado maestro (aquel del lado *uno* de una asociación
1:N). Es decir, dada una asociación 1:N, se agrega una columna en la
tabla del extremo *uno* para registrar la ocurrencia más reciente del
extremo *muchos* (valor actual v/s valor histórico). Recordar semántica
de tiempo (time-stamps).

Por ejemplo, si se considera el siguiente **modelo relacional
normalizado**, donde PK va subrayada y FK en cursiva:

> PRODUCTO ([CODIGO-PRODUCTO], NOMBRE-PRODUCTO)

> PRECIO ([ID-PRECIO], VALOR, FECHA-INICIO, FECHA-TERMINO, *CODIGO-PRODUCTO*)

Un **modelo relacional desnormalizado** al aplicar esta estrategia
sería:

> PRODUCTO ([CODIGO-PRODUCTO], NOMBRE-PRODUCTO, PRECIO-ACTUAL)

> PRECIO ([ID-PRECIO], VALOR, FECHA-INICIO, FECHA-TERMINO, *CODIGO-PRODUCTO*)

Al agregar PRECIO-ACTUAL se ahorra el tiempo de hacer un JOIN entre
PRODUCTO y PRECIO para obtener el precio de venta actual del producto,
que será sin duda el usado con mayor frecuencia. Es decir, solo basta
con el acceso a PRODUCTO para obtener el precio actual, si se desea un
precio anterior (que de seguro será menos frecuente requerirlo), allí
recién se asume el costo de hacer un JOIN con la tabla PRECIO.

#### Otras Formas Normales -- Desnormalización -- Claves en *corto circuito*

Esta estrategia de desnormalización, implica crear una nueva clave
foránea desde el nivel de detalle más bajo al nivel más general.

Por ejemplo, si se considera el siguiente **modelo relacional normalizado**, donde PK va subrayada y FK en cursiva:

DEPARTAMENTO ([ID-DEPARTAMENTO,] NOMBRE-DEPARTAMENTO)

CARRERA ([CODIGO-CARRERA], NOMBRE-CARRERA, *ID- DEPARTAMENTO*)

ALUMNO ([RUT], NOMBRE-ALUMNO, DOMICILIO, *CODIGO-CARRERA*)

Un **modelo relacional desnormalizado** al aplicar esta estrategia
sería:

> DEPARTAMENTO ([ID-DEPARTAMENTO,] NOMBRE-DEPARTAMENTO)

> CARRERA ([CODIGO-CARRERA], NOMBRE-CARRERA, *ID- DEPARTAMENTO*)

> ALUMNO ([RUT], NOMBRE-ALUMNO, DOMICILIO, *CODIGO-CARRERA,* NOMBRE-DEPARTAMENTO)

Al agregar NOMBRE-DEPARTAMENTO en ALUMNO, se ahorra el tiempo de hacer
JOINs entre ALUMNO, CARRERA y DEPARTAMENTO para poder conocer el nombre
del departamento al que pertenece la carrera de un alumno, ya que
bastará con solo accesar la tabla ALUMNO.

#### Otras Formas Normales -- Costos Desnormalización

La desnormalización con sus diferentes estrategias obliga a incorporar
código adicional, para ayudar a mantener la integridad del diseño. Ese
es el costo que hay que asumir en pro de mejores tiempos de respuestas.

Por lo general, se requiere un método para cada una de las operaciones
DML típicas. Por ejemplo, si se toma la estrategia de desnormalizar
aalmacenando valores derivados, se tendría el siguiente **modelo
relacional desnormalizado:**

> FACTURA ([NRO-FACT], FECHA, *RUT-CLIENTE*, TOTAL)

> DETALLE ([*NRO-FACT*, *COD-PROD*], CANTIDAD, SUBTOTAL)

Habría que asumir los siguientes costos de desarrollo por incorporar
código adicional:

-   Si se agrega un nuevo Detalle, el Total de Factura debe aumentar en
    el valor del Subtotal correspondiente.

-   Si se modifica el Subtotal de un Detalle, en la misma cantidad debe
    variar el valor del Total.

-   Si se elimina un Detalle, Total debe disminuir en la cantidad
    presente en el Subtotal que desaparece.
