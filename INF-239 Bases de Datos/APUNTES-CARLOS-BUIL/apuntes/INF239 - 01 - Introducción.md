# INF239 - Bases de datos - Introducción 


## Analogía inicial: una repisa como una base de datos

Podemos considerar el diseño de una base de datos como el proceso de construir una gran repisa con innumerables casilleros donde puedo poner en forma ordenada diferentes tipos de cosas, y dependiendo del lugar donde la ponga, me será más o menos fácil alcanzar el objeto que allí esté. Esto es una base de datos, un lugar donde almacenamos datos y dependiendo de cómo sea esa repisa podremos llegar a unos datos antes que a otros. No solo eso,  en esta repisa se pueden almacenar diferentes tipos de datos (texto, imágenes, relaciones, etc.) que podremos utilizar en los distintos procesos que se realizan en una organización o en las distintas actividades que realizamos como personas. Necesitamos esos datos para tomar decisiones que permitan mejorar los procesos o incluso nuestra calidad de vida.

![Repisas y bases de datos](imagenes/repisa.png "Repisa Base de datos")

Sin embargo, antes de construir la repisa necesitamos diseñarla, decidir qué objetos van a caber en cada estante de la repisa. En la base de datos sucede lo mismo, necesitamos el diseño o plano de la base de datos, el modelo de la base de datos y luego tenemos que construirla (a través del software denominado Sistema Administrador (o gestor) de Bases de Datos (SGBD) o DBMS por sus siglas en inglés Data Base Management System). El DBMS es a la base de datos como el sistema operativo es al computador, es decir, es un software que permite administrar todos los recursos disponibles para almacenar y recuperar los datos, y quien define la estructura de datos con la cual se trabajará.

Volviendo a la analogía de la repisa, si tengo una repisa sólo con estantes rectangulares o cuadrados implicaría el colocar objetos de un tamaño similar, que tuviesen propiedades parecidas. Sin embargo, si tenemos una repisa con forma de triángulo, podríamos colocar en la parte más alta de la repisa algún objeto que fuese más importante que el resto. Esto es a las bases de datos la forma de organización de los datos. Por ejemplo, tener una base de datos relacional (celdas de una tabla en excel) o una de grafos (una red de amigos), y eso depende del DBMS que se utilice (si se usa el DBMS Oracle la estructura de datos que posee para almacenamiento es una tabla, si se usa el DBMS Amazon Neptune) la estructura de datos es un grafo.

El tipo de estructura de datos del DBMS, no solo organiza el almacenamiento, sino que también define los mecanismos de acceso o de recuperación de los datos. Si yo dejo un objeto en la parte alta de la repisa, me costará más acceder a él que si lo almaceno a mi altura. Es importante entonces diseñar la repisa de forma adecuada para que su dueño pueda poner y sacar cada uno de los objetos que a él le interese exhibir. Es primordial diseñar una base de datos para que un usuario pueda almacenar y posteriormente recuperar, oportunamente, cada uno de los datos que sean de utilidad para labor que realiza. 
**Entre el diseño y el uso de la base de datos estaremos navegando en este curso.**


**En esta unidad veremos los siguientes 4 puntos**:
 * Definición de Bases de Datos
 * Enfoque Tradicional de Archivos versus Enfoque de Base de Datos
 * Tipos de Bases de Datos
 * Proceso de Diseño de Bases de Datos
