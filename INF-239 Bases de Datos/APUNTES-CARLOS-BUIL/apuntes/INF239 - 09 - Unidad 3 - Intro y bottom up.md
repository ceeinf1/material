
# UNIDAD 3: Diseño lógico de bases de datos relacionales

Los 4 puntos a ver en esta Unidad son:
1. Características de los Modelos de Datos Lógicos
2. Enfoque Metodológico para diseñar BDR 
   1. Enfoque Bottom-up
   2. Enfoque Top-down
3. Otras Consideraciones de Diseño Lógico de BDR
   1. Ejercicios de Diseño Lógico BDR (en clases)

## Etapas para Diseño de BD

Recordar esta figura con las etapas para el diseño de BD vistas en el capítulo 1.

![Etapas de vida de la Base de datos](imagenes/etapas_bd.png "Etapas de vida de la Base de datos")

Planteamos que existen diferentes metodologías para el desarrollo de software, las cuales tienen etapas muy claras donde se identifica el cómo y cuándo se debe profundizar en el diseño de la BD. 

Las etapas principales de estas metodologías se pueden sintetizar en el esquema que aparece en esta imagen, donde se identifican 2 enfoques, uno orientado a los procesos (lado izquierdo del esquema) y otro a los datos (lado derecho del esquema), en el cual estamos profundizando este semestre.
Con la materia vista para el *Certamen 1*, hemos cubierto las etapas de: 

 1. Recolección y Análisis de Requisitos, permite obtener los requisitos o requerimientos que se le piden a la BD, es decir, qué información debemos poder extraer de ella (requerimientos de información). 
 2. Diseño Conceptual, permite obtener un modelo de datos conceptual que satisfaga los requisitos identificados en etapa anterior (equivale a la maqueta de un arquitecto).
 3. Elección del Software o DBMS que se utilizará.
   
Ahora nos corresponde cubrir las siguientes etapas:

 1. Diseño Lógico, permite traducir el modelo de datos conceptual en algo entendible por el DBMS, es decir, en un modelo lógico (equivale al plano de un arquitecto).
 2. Diseño Físico, permite agregar características físicas al modelo que optimicen el funcionamiento de la BD.
 3. Implementación de la BD o creación de la BD 

### Modelo de datos lógico
Un modelo de datos lógico es aquel modelo de datos que se genera al tomar uno conceptual e incorporarle las características propias del tipo de software (DBMS) sobre el cual se realizará la posterior implementación.
Ejemplos de modelos de datos lógicos existentes podrían ser:
 * Jerárquico (árbol)
 * Reticular (grafo)
 * Relacional (relación o tabla)
 * Orientado al Objeto (objeto)
 * Multidimensional (hipercubo o tabla multidimensional)

En nuestro caso profundizaremos en el modelo de datos lógico del tipo relacional.

#### Características del modelo relacional
Este modelo se caracteriza por:

 * Independencia (física) de los datos: los valores quedan almacenados en forma independiente de los metados (diccionario de datos).
 * Uso de claves lógicas: las asociaciones se implementan a través de atributos que actúan como claves lógicas (PK y FK), en lugar de punteros como sucede en otros tipos de modelos lógicos.
 * Estrategia de diseño basada en la Teoría de la Normalización (Formas Normales): que se basa en dividir una estructura de datos compleja en partes más pequeñas, pero asociadas entre sí. La división se realiza aplicando teoremas o criterios denominados Formas Normales. Existen definidas varias formas normales, de la 1FN a la 6FN, más otras 2 adicionales, siendo las más utilizadas las tres primeras: 1FN, 2FN y 3FN.
 * Lenguaje de consultas de alto nivel (SQL): contar con un lenguaje no procedural, sencillo y de muy alto nivel, donde el foco está en plantear consulta sobre qué dato se necesita, no cómo se debe navegar en la BD para obtenerlo.
 * Relación o tabla: es la estructura de datos en base a la cual se organizan los datos. Una relación corresponde a un conjunto de datos organizados en una tabla bidimensional formada por filas (tuplas o registros) y columnas (atributos o campos). No olvidar que existe una diferencia entre relation (conjunto o tabla) y relationship (asociación), puede haber confusión por temas de traducción.

Otras características para tener presente son las siguientes: 
 * En una relación el orden de las filas y de las columnas es irrelevante.
 * Cada columna contiene valores del mismo atributo, los que deben ser del mismo tipo y atómicos. Al conjunto de valores posibles para una columna se le llama Dominio.
 * Cada fila es única y la unicidad está dada al manejar una columna (o conjunto de columnas) que no soporte valores repetidos, denominada clave primaria (PRIMARY KEY). 
 * Cardinalidad es el número de filas de una relación. Cambia cuando se insertan nuevas tuplas o se borran algunas de las existentes.
 * Grado es el número de columnas de una relación. Es decir, cada fila debe contener tantos valores como indique su grado (suele no cambiar).


## Diseño lógico de una Base de Datos Relacional (BDR)

Para diseñar una BDR hay dos tipos de enfoques metodológicos que es importante tener en cuenta:
 * Enfoque Bottom-Up: mediante la integración de modelos parciales, obtenidos a partir de la normalización de las vistas (salidas, reportes) que componen un sistema.
 * Enfoque Top-Down: construir un modelo conceptual y luego convertirlo en uno relacional, aplicando reglas de transformación formales.

Se sugiere usar el primer enfoque en casos en que, como diseñador de BD, no se tiene gran conocimiento del contexto asociado a la realidad a modelar, pues es un enfoque que va de lo particular o más detallado (los datos en las vistas de usuarios) a lo más general (modelo de datos relacional).
En cambio, el segundo enfoque parte de un modelo de datos conceptual donde ya queda reflejado que se tiene un conocimiento de la realidad a modelar. Va de lo más general a lo más particular.


### Enfoque Metodológico Bottom-Up.

En esta imagen se muestra un esquema para explicar el enfoque bottom-up, haciendo una analogía entre el diseño de una casa que realiza un arquitecto con el diseño de una BD que realiza un Informático.

![Enfoque Metodológico Bottom-Up](imagenes/enfoque_bottom_up.png "Enfoque Metodológico Bottom-Up")

Imaginemos que llega donde un arquitecto una familia que quiere le construyan una casa. Se produce una entrevista donde cada miembro de la familia plantea con mayor énfasis las características de aquellos lugares de la casa que siente usará más o que conoce más: living, comedor, cocina, etc. (modelos externos o vistas de usuarios). El arquitecto se tomará algunos días para integrar esas características en el diseño de una maqueta donde visualmente la familia pueda revisar rápidamente que sus requerimientos para la casa fueron considerados por el arquitecto. De seguro le plantearán algunos cambios, e iterarán hasta poder estar de acuerdo en que la maqueta (modelo lógico) representa la casa que ellos desean. Una vez que se cuenta con la aprobación de la maqueta, viene el arquitecto y considerando el presupuesto disponible, genera un plano (modelo físico) de la casa, donde se detalla con mayor precisión cada parte de la construcción a realizar; este plano va dirigido ahora no solo a la familia, sino más que nada a quienes, como profesionales de la construcción, llevarán a cabo la construcción misma de la casa (realidad).
En el caso de un Informático tenemos que se reúne con un grupo de usuarios que requieren se les construya una base de datos (mejor dicho, un sistema de información para ser más precisos). Cada usuario sabe qué datos necesita para realizar mejor su trabajo (modelos externos o vistas de usuarios). El Informático se tomará algunos días para integrar estas distintas vistas en un modelo de datos comprensible para el usuario, basado en la consolidación (respeto de las distintas visiones, eliminando redundancias y reconociendo sinónimos y homónimos), la consistencia (representando la realidad lo mejor posible apegado a las semánticas de los datos) y la completitud (sin perder ningún dato de los requeridos por los usuarios, incluidos los atributos derivables). Esto se logra aplicando de la 1FN a la 3FN, las que permiten obtener un conjunto de tablas en 3FN (modelo lógico o modelo relacional), que aseguran reducir redundancia y anomalías de mantención. A este modelo ahora se le deben incorporar características propias del DBMS con el cual se va a trabajar, de tal manera de velar por la eficiencia, seguridad e integridad de la BD a construir, incorporando, por ejemplo, recursos provistos por el DBMS como índices, perfiles de usuario, estrategias de respaldos, etc., lo cual permite obtener el modelo físico de la BD que finalmente quedará construida a través de los comandos DDL y los programas de aplicación asociados para asegurar el acceso a los datos allí almacenados (realidad).
Nuevamente, como diseñadores de BD, viajamos de lo más abstracto (las visiones del usuario sobre lo que debiera ser la BD) a lo menos abstracto (la BD con los programas de aplicación que debemos construir).


Para aplicar el enfoque bottom-up de diseño de BD, es necesario ser ordenados y precisos, de tal manera de capturar la realidad del usuario de la mejor forma posible. Para ello es bueno seguir los siguientes pasos o etapas que permiten obtener los 3 tipos de modelos planteados anteriormente:
 * Etapa 1: Formulación y Análisis de Requerimientos (o requisitos), que tomando como “input” las vistas de usuarios (o requerimientos de información) y los requerimientos de procesamiento (o requerimientos no funcionales), obtiene como como “output” una especificación (o documentación) de la información que el usuario espera sea obtenida desde la BD a construir. 
 * Etapa 2: Diseño Lógico de la BDR, tomando como “input” la especificación de requerimientos de la etapa anterior y las vistas de usuario, aplica la teoría de las formas normales para obtener como “output”, el modelo de datos lógico.
 * Etapa 3: Diseño Físico de la BDR, tomando como “input” el modelo de datos lógico de la etapa anterior, y los requerimientos de procesamiento, incorpora las características propias del DBMS escogido, generando como “output” el modelo de datos físico, que luego será implementado a través de los comandos DDL y los programas de aplicación respectivos.
A continuación, detallaremos cada una de estas etapas.

![Etapas Enfoque Metodológico Bottom-Up](imagenes/etapas_bottom_up.png "Etapas Enfoque Metodológico Bottom-Up")

#### Enfoque Bottom-Up - Etapa 1
La primera etapa, denominada Formulación y Análisis de Requerimientos (o requisitos), implica llevar a cabo los siguientes pasos:

 * Identificar el ámbito o área de la organización, incluyendo sus actores o usuarios. Es relevante clarificar dentro de la organización cuál(es) son las áreas que se verán impactadas con la BD a construir, pueden ser funciones, procesos o actividades, o cargos, en los distintos niveles de la organización, lo importante es poder identificar a todos los actores o usuarios involucrados, sean internos (contador, vendedor, gerente, etc.) como externos (clientes, proveedores, etc.) a la organización, pues será a ellos a los que se debe entrevistar.
 * Recolectar los requerimientos de información o vistas de usuarios (documentos formales o informales), que utiliza el usuario para realizar su trabajo. A través de entrevistas, idealmente en terreno para observar cómo trabaja el usuario, se debe recolectar todos los documentos (vistas de usuarios) que maneja. Ejemplos de vistas de usuarios serían, una factura para el usuario contador, un reporte para el usuario gerente, una lista de curso para el usuario profesor, una llamada telefónica para el usuario vendedor, etc.
 * Analizar las vistas para proponer mejoras a ellas y/o crear nuevas vistas. Las vistas deben observarse en forma crítica, de tal manera de proponer mejoras si es que se estima pertinente. No necesariamente automatizar lo que existe tal cual es la mejor solución
 * Establecer requerimientos de procesamiento o no funcionales. Las vistas representan los datos que el usuario requiere para realizar su trabajo, pero ellas deben ser complementadas con aspectos generales asociados al comportamiento que debe tener la BD cuando esté en operación. Por ejemplo, una factura debe ser emitida online 

Cabe aquí precisar la diferencia entre requerimientos funcionales y no funcionales, sobre todo pensando más allá del diseño de una BD, sino en el desarrollo de software. Un requerimiento funcional representa lo que el usuario quiere se automatice para poder realizar mejor su trabajo (se convierten en componentes del software y de la BD a construir), en cambio, los requerimientos no funcionales, son criterios que permitirán evaluar la operación misma de la automatización que se realice (se convierten en pruebas de usabilidad y seguridad, por ejemplo). En palabras simples, los requerimientos funcionales se enfocan al “qué” hace el software y la BD, en cambio, los no funcionales, al “cómo” lo hace.


Aquí se muestra el ejemplo de la vista de usuario factura. En este caso la vista muestra cómo ve la BD, por ejemplo, el usuario cajero o el usuario cliente.

![Ejemplo de factura](imagenes/factura.png "Ejemplo de factura")

Aquí se describen los tipos de requerimientos de procesamiento o no funcionales, orientados al diseño de una BD, los cuales deben ser considerados al momento de recolectar cada vista de usuario, es decir, al momento de entrevistar a los usuarios, por ello se sugiere una pregunta asociada a cada requerimiento para poder guiar la entrevista. Para explicar cada requerimiento de procesamiento, se tomará como ejemplo, la vista factura en una multitienda:

 * Privacidad: ¿qué perfiles de accesos se necesitan para la vista? Ejemplo: solo de lectura para todos los usuarios de la Factura, salvo el usuario contador que puede modificar sus datos.
 * Integridad: ¿qué reglas de validación y de integridad referencial se debieran considerar para la vista? Ejemplo: toda Factura requiere tener un cliente asociado, es decir, no puede existir la factura si no existe el cliente, por lo que al insertar una Factura se debe chequear que exista el Cliente, y al eliminar el Cliente, no se puede eliminar si es que tiene Facturas (DELETE RESTRICTIVE).
 * Tiempo de Respuesta: ¿la vista requiere ser obtenida en tiempo real? Ejemplo: la Factura debe obtenerse online en el momento en que está el cliente en la caja, versus, la Factura puede ser enviada por correo en un plazo de 30 días.
 * Respaldo: ¿cada cuánto tiempo conviene respaldar los datos de la vista? Ejemplo: dado el alto volumen de Facturas emitidas en el día (1 millón de facturas), se debe hacer un respaldo diario.
 * Recuperación: ¿cuánto tiempo se puede esperar para recuperar los datos de la vista, en caso de que se pierdan? Ejemplo: si se cae la BD, no se podrán emitir Facturas, por lo cual el negocio no podrá funcionar, es decir, el tiempo de espera para recuperar la BD tiende a cero.
 * Archivamiento (archiving): ¿cuándo un dato pasa a ser histórico? Ejemplo: para las Facturas, el Servicio de Impuestos Internos suele hacer consultas de datos con a lo más 2 años de antigüedad, por lo que después de 2 años, pueden pasar los datos de esta vista a archivos históricos.
 * Proyecciones Crecimiento: ¿cuál es el crecimiento proyectado para la vista? Ejemplo: se visualiza abrir 10 nuevos locales para la multitienda el próximo año, lo cual se estima aumentará el número de facturas emitidas en un 50%. 

Estos requerimientos de procesamiento o no funcionales son orientados al diseño de una BD, en los próximos cursos del área de software verán requerimientos de este tipo orientados al software. 

#### Enfoque Bottom-Up – Etapa 2 - Pasos
La etapa 2 de Diseño Lógico de la BDR, toma como “input” los requerimientos de la etapa anterior (vistas de usuario), y les aplica la teoría de las formas normales para obtener como “output”, el modelo de datos lógico. Por lo que el objetivo principal de esta etapa es la obtención del modelo de datos lógico, o modelo de datos normalizado como se le puede llamar, en el sentido que es capaz de “normalizar” las distintas vistas de usuario en una sola visión.
Esta etapa implica llevar a cabo los siguientes pasos:
1. Normalización 
2. Integración de resultados de la normalización
3. Generación del modelo de datos lógico
4. Revisión del modelo diseñado

Estos pasos serán detallados a continuación, y si bien se aconseja realizarlos en forma secuencial, es muy importante iterar varias veces por ellos, hasta lograr un modelo que llegue al justo equilibrio entre satisfacer los requerimientos del usuario y ajustarse a los recursos con que se cuenta.


### Enfoque Bottom-Up – Etapa 2 – Teoría de la Normalización

Su objetivo es agrupar atributos en relaciones (tablas) que se encuentren bien estructuradas, a través de un análisis de las llamadas dependencias funcionales entre dichos atributos.
Se entiende por relación bien estructurada a aquella relación (tabla) que minimiza la redundancia de información, evitando inconsistencias y anomalías de mantención, en las típicas operaciones de inserción, eliminación y actualización sobre una base de datos.
El resultado de la normalización son relaciones (tablas) con dependencia funcional de la clave primaria respecto del resto de los atributos que la componen.

La dependencia funcional se obtiene cuando el valor de un atributo permite conocer en forma única, no ambigua y exacta, el valor de otro atributo. 
Es una asociación entre dos atributos que se define como:
*"Para una relación R, el atributo B es funcionalmente dependiente del atributo A, si para cada instancia de A, el valor de A determina en forma única al valor de B"*.

Se escribe como: A -> B y se lee: "A determina a B” o “B es funcionalmente dependiente de A".

Por ejemplo, en un conjunto de datos asociados al contexto de un cliente que compra productos a través de facturas, tenemos las siguientes dependencias funcionales:   

RUT -> nombre, dirección, celular

NroFactura -> fecha, total

NroFactura, CodProd -> cantidad

CodProd -> nombre-prod, precio-actual

Si vemos a cada atributo como un conjunto de datos, por ejemplo, el conjunto de Nombres de Alumnos y el conjunto de Carreras, y los analizamos a nivel de valores tendríamos que por cada nombre de alumno habría una única carrera a la que un alumno pertenece. En cambio, si analizamos desde carreras a alumnos, tendríamos que para una carrera habría varios alumnos relacionados con ella. Por lo que como se ve en tablas siguientes, existe una dependencia funcional entre Nombre_Alumno y Carrera, pero no entre Carrera y Nombre_Alumno.

        Nombre_Alumno -> Carrera

|  NOMBRE_ALUMNO | CARRERA  | 
|---|---|
|  Benjamín Aros | Informática  |   
|  Francisco Aspe | Informática  |   
|  Diego Quezada |  Telemática |   
|  Oscar Rojas | Electrónica  |   
|  Camila Norambuena |  Matemática |   



		Carrera -> Nombre_Alumno



La Teoría de la Normalización plantea un conjunto de pasos o Formas Normales, que permiten ir dividiendo una vista de usuarios no normalizada, en subconjuntos más pequeños donde van quedando aquellos atributos que tienen una dependencia funcional con respecto a la clave primaria. Es decir, van quedando juntos aquellos atributos que tienen un valor que permite detectar en forma inequívoca el valor de otro atributo.
Para ello se debe partir de una vista no normalizada, luego ir analizando diferentes dependencias funcionales que generan anomalías entre los datos, las cuales se van eliminando al ir pasando de una Forma Normal (FN) a otra.
Existen varias FN, pero aplicar hasta 3FN es, por lo general, suficiente para los casos reales, aunque no garantiza que todas las anomalías se hayan eliminado.
La frase típica aquí es “dividir para reinar”.
El proceso de normalización en sí consta de la siguiente secuencia de pasos:

 1. Tomar la vista de usuario, y convertirla en una vista no normalizada, expresada a través del nombre de la vista y entre paréntesis el conjunto de atributos que la componen, identificando entre paréntesis de llave aquellos atributos que se repiten un número “n” de veces, a los cuales se les denomina grupos repetitivos. Identificar además si existen atributos derivables, los que no necesitan pasar por el proceso de normalización, pues ellos no serán almacenados en la BD; si se requiere dejarlos documentados para posteriormente, desarrollar el código que permitirá calcularlos.
 2. Eliminar de la vista no normalizada los grupos repetitivos para poder así estar en 1FN. Para eliminarlos se divide la vista en subconjuntos más pequeños, pero asociados entre sí a través de algún atributo en común (PK y FK). En este paso es fundamental identificar bien las PK de cada subconjunto resultante. Si en vista no normalizada no existen grupos repetitivos, ya se está en 1FN.
 3. Revisar cada vista resultante del paso anterior, y eliminar las dependencias parciales para poder así estar en 2FN. Una dependencia parcial se da cuando hay una clave primaria compuesta, y existen atributos no primarios que tienen una dependencia con solo parte de la PK. Para eliminar las dependencias parciales, se divide la vista con esta anomalía en subconjuntos donde los atributos dependan de la PK completamente.
 4. Revisar cada vista resultante del paso anterior, y eliminar las dependencias transitivas para poder así estar en 3FN, obteniéndose un conjunto de tablas o relaciones de un modelo relacional (equivalentes a entidades con PK y FK). Una dependencia transitiva se da cuando hay atributos no primarios que tienen entre sí una dependencia funcional, y que, a través de alguno de ellos, se relacionan en forma transitiva con la PK. Para eliminar las dependencias transitivas, se divide la vista con esta anomalía en subconjuntos donde los atributos quedan dependiendo de quién dependen más directamente, y enlazados, con los demás, a través de una FK.





