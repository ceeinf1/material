# INF239 - Bases de Datos - Archivos y Bases de Datos (1)

## Archivos y Bases de Datos

Los datos se pueden guardar, principalmente en archivos planos o bases de datos.
Un archivo plano almacena datos que comparten una misma estructura y/o comportamiento similar. Por lo general, los datos de un archivo se refieren a un mismo tipo de entidad del mundo real. Por ejemplo, los Alumnos de la USM. Cada entidad representada por el archivo se guarda en un registro que se describe a través de sus atributos.  En la siguiente tabla se ve un archivo que tiene la forma de una tabla bidimensional donde cada fila representa los datos de un alumno de la USM y cada columna sus atributos o características que lo describen como su RUT, Nombre, Género y película en la que aparecen.

| RUT | Nombre | Sexo | Película | 
|-----|--------|---|---|
| 18.345.678-9 | Natasha Romanov | F | End Game | 
| 18.223.344-5 | Thor | M | Thor | 
| 1-2 | Steve Rogers | M | Capitán América | 
| 13.579.246-8 | Tony Stark | M | Iron Man | 
| 12.121.212-2 | Peter Quill | M | Guardianes de la Galaxia |   
<br/>

Una base de datos es un conjunto de archivos (tablas) relacionados entre sí mediante alguna asociación lógica. Ya sabemos lo que es un archivo, ahora profundicemos en la característica diferenciadora de una base de datos, esto es que sean archivos relacionados entre sí.

De todo esto es muy importante tener en cuenta que las operaciones que se realizan sobre una base de datos son operaciones de conjuntos (de datos): uniones, intersecciones, diferencias, etc. En la siguiente imagen quiero resaltar esto, todos los datos almacenados en la tabla se tratan como si fuesen elementos/datos pertenecientes al mismo conjunto, el conjunto de los súper héroes.


### Enfoque de Archivos

El enfoque utilizado en el desarrollo de sistemas de información (SI) para el tratamiento de los datos en la década de los 70, se relacionaba con el procesamiento de datos por departamento (o unidad organizacional), es decir, los SI respondían a requerimientos de usuarios por aplicaciones individuales como remuneraciones, cuentas corrientes, contabilidad, control inventario, etc. Cada sistema desarrollado era diseñado, entonces, para satisfacer las necesidades de un departamento o grupo de usuarios, no existiendo una planificación corporativa o un modelo que guiara el desarrollo de aplicaciones. Este enfoque también se le conoce como Enfoque por Agregación.

![Enfoque de Archivos](imagenes/enfoque_archivos_2.png "Enfoque de Archivos")

En la figura se puede visualizar la esencia del Enfoque por Agregación. La figura muestra el organigrama de una organización en el cual diferentes funciones requieren de un SI para apoyar sus decisiones, cada SI (marcado por un óvalo) utiliza datos de la organización, los cuales son parte del área marcada en la figura. La superposición de áreas indica la utilización del mismo tipo de datos por uno o más SI; no implica compartir recursos sino más bien duplicar recursos. El nombre por agregación representa a un proceso evolutivo que se presenta al ir acoplando a un SI nuevas funciones y, por ende, nuevos requerimientos que no habían sido considerados en el momento del diseño inicial del sistema.


Esto llevaba a que cada nueva aplicación era diseñada con su propio conjunto de archivos de datos. Muchos de esos datos podían ya existir en archivos de otras aplicaciones, pero como para ser usados en la nueva aplicación requerían a veces de alguna reestructuración, lo cual era muy complejo (implicaba revisar todos los programas que usaban esos archivos, e incluso a veces, reescribir completamente los programas), entonces, la mayoría de las veces era más simple diseñar nuevos archivos para cada nueva aplicación.

![Enfoque de Archivos](imagenes/enfoque_archivos.png "Enfoque de Archivos")

En la figura que se muestra se ilustra este enfoque desde una perspectiva computacional. Programas de aplicación pueden acceder, según la figura, a uno o más archivos de datos, por lo cual deben contener cada uno de ellos las definiciones de los archivos que utilizan y las correspondientes instrucciones que permiten gestionarlos. Cada programa es dueño de sus archivos de datos y la lógica del programa es dependiente de los formatos y descripciones de esos datos.

### Enfoque de Archivos y sus desventajas 

Las desventajas del enfoque de archivos de procesamiento de datos se resumen en:
* Redundancia no controlada: al tener cada aplicación sus propios archivos, existe un alto grado de redundancia, lo que con lleva a pérdida de espacio, ingreso repetidamente del dato para actualizar todos los archivos donde él está e inconsistencias (o varias versiones del dato), lo que requiere de tiempo para corregirlas. En general algo de redundancia es útil, pero debe ser muy bien controlada.
Dependencia de los datos de los programas de aplicación: la definición de los datos que conforman un archivo iba incorporada dentro del programa de aplicación, generando programas con una gran cantidad de líneas de código. El lenguaje de programación COBOL es una muestra de lo fuerte que era esta dependencia, ya que exigía que se describiera en forma muy detallada cada uno de los elementos de datos que se usaban en el programa.

* Pobre estandarización: al desarrollar SI, se requieren estándares básicamente para los nombres de datos, formatos y restricciones de acceso. Estos estándares son difíciles de tener en un enfoque de archivos, principalmente porque la responsabilidad por el diseño y operación del sistema es descentralizada. Esto puede traer dos tipos de inconsistencias: sinónimos (uso de nombres diferentes para un mismo ítem de datos, ej.: #ESTUDIANTE y ROL ALUMNO) y homónimos (uso de un mismo nombre para ítems de datos distintos, ej.: NOTA usado para indicar la calificación de un alumno en un ramo y NOTA usado para almacenar información narrativa sobre una orden de compra). La estandarización es más difícil en grandes organizaciones sin control centralizado, ya que cada unidad puede tener sus propias aplicaciones con sus nombres y formatos particulares. La pobre estandarización dificulta las mantenciones de la aplicación.

* Inconsistencia de datos: se produce cuando el dato es almacenado en distintas partes y no se modifica en todas ellas al realizarse una actualización (UPDATE). Es la fuente más común de errores en las aplicaciones, lleva a documentos y reportes inconsistentes y hace disminuir la confianza del usuario en la calidad de la BD (DataBase Quality).

* Problemas con el cliente: no se puede responder con facilidad a nuevos requerimientos de información del cliente/usuario (reportes, documentos, etc.) que no hayan sido considerados en el diseño original. Esto origina frustración en los usuarios al no poder comprender por qué el sistema no puede darle la información que necesitan en el nuevo formato requerido, a pesar de que se cuenta con los archivos respectivos.
Escasa posibilidad de compartir datos: como cada aplicación tiene sus propios archivos, existe poca oportunidad para los usuarios de compartir datos. Esto trae como consecuencia que el mismo dato tenga que ser ingresado varias veces para actualizar los archivos con datos duplicados. Otra consecuencia, es que al desarrollarse nuevas aplicaciones no es posible a veces, explotar los datos contenidos en archivos que ya existen, teniendo que crearse nuevos archivos con la consiguiente duplicación de datos.
* Baja productividad del desarrollador: en general, debe diseñar cada archivo usado en una nueva aplicación y luego codificar las definiciones en el programa (en algunos casos esto se simplifica pues se usan descripciones de datos estándares que existen en bibliotecas). También debe escribir las instrucciones de Input/Output requeridas por el método de acceso seleccionado. Por lo tanto, se requiere de un mayor esfuerzo de desarrollo, lo que lleva a una baja productividad y por ende aumentan los costos del software. 