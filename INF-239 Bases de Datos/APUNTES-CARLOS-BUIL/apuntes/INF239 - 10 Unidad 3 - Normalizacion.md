# UNIDAD 3: Diseño lógico de bases de datos
relacionales

Enfoque Bottom-Up -- Etapa 2 --
Teoría de la Normalización

1.  Vista no normalizada
1.  1FN: para estar en 1FN se deben haber eliminado los **grupos repetitivos** de la vista inicial

2.  2FN: para estar en 2FN se debe estar en 1FN y haber eliminado las **dependencias parciales**

1.  3FN: para estar en 3FN se debe estar en 2FN y haber eliminado las **dependencias transitivas**

## Enfoque Bottom-Up -- Etapa 2 -- Ejercicio de Normalización Caso Hospital

Vamos a suponer el caso de un Hospital donde se han identificado 4
vistas de usuarios:

-   Vista 1: Factura, la cual fue recolectada en la entrevista con el
    usuario cajero.

-   Vista 2: Informe Utilización Piezas, la cual fue recolectada en la
    entrevista con el usuario enfermera.

-   Vista 3: Pantalla Datos Pacientes, la cual fue recolectada en la
    entrevista con el usuario recepcionista.

-   Vista 4: Informe para Visita Médicos, la cual fue recolectada en la
    entrevista con el usuario médico.

Antes de partir con la normalización, y pensando en que estamos en la
Etapa 1 de Formulación y Análisis de Requerimientos, vamos a plantearnos
2 interrogantes:

1.  ¿Qué mejoras se pueden realizar a las vistas existentes? A la Vista
    > 1 le falta el Número de la Factura y la Fecha.

2.  ¿Qué nuevas vistas se pueden crear? Una vista que permita mostrar
    > indicadores con la cantidad de pacientes mensuales atendidos en el
    > hospital.

Para explicar la normalización con este ejemplo, se usará la siguiente
notación:

-   Atributos derivables aparecerán tachados

-   PK aparecerá subrayada

-   FK aparecerá en cursiva

-   Grupos repetitivos aparecerán entre paréntesis de llave

### Enfoque Bottom-Up -- Etapa 2 -- Ejercicio de Normalización Caso Hospital -- Vista 1

**Mejoras:** se agregan los atributos Número y Fecha.

**Supuestos:** se considerará atributos derivables a SubTotal y %Impuesto.

**Vista no normalizada**

Vista1 (NRO-F, FECHA-F, NOM-P, NRO-P, DIR-P, CIUDAD-P,
COD-SISTEMA-SALUD,

{NRO-I, NOM-I, VALOR-I}, ~~SUB-TOTAL~~, ~~%IMPUESTO~~, TOTAL)

**1FN**

Vista1.1 ([NRO-F], FECHA-F, NOM-P, NRO-P, DIR-P, CIUDAD-P,
COD-SISTEMA-SALUD, TOTAL)

Vista1.2 ([*NRO-F*, NRO-I], NOM-I, VALOR-I)

**2FN**

Vista1.1 OK

Vista1.2.1 ([*NRO-F*, *NRO-I*], VALOR-I)

Vista1.2.2 ([NRO-I], NOM-I)

**3FN**

Vista1.1.1 ([NRO-F], FECHA-F, *NRO-P*, TOTAL)

Vista1.1.2 ([NRO-P], NOM-P, DIR-P, CIUDAD-P, COD-SISTEMA-SALUD)

Vista1.2.1 OK

Vista1.2.2 OK

### Enfoque Bottom-Up -- Etapa 2 -- Ejercicio de Normalización Caso Hospital -- Vista 2

**Supuesto:** se considerará atributo derivable a la Fecha, suponiendo
que corresponde a la fecha del día en que se saca el informe por lo cual
se extraerá desde el calendario del computador.

**Vista no normalizada**

Vista2 (~~FECHA,~~ {NRO-CAMA, NRO-PIEZA, TIPO-CAMA, NOM-P, NRO-P,
FECHA-ALTA-ESPERADA})

**1FN**

Vista2.1 ([NRO-CAMA], NRO-PIEZA, TIPO-CAMA, NOM-P, NRO-P,
FECHA-ALTA-ESPERADA)

**2FN** Vista2.1 OK

**3FN**

Vista2.1.1 ([NRO-CAMA], NRO-PIEZA, TIPO-CAMA, *NRO-P*)

Vista2.1.2 ([NRO-P], NOM-P, FECHA-ALTA-ESPERADA)

### Enfoque Bottom-Up -- Etapa 2 -- Ejercicio de Normalización Caso Hospital -- Vista 3

**Supuesto:** se considera el atributo Ubicación, desagregado en
Nro.Cama y Nro.Pieza como sale en Vista 4.

**Vista no normalizada**

Vista3 ([NRO-P], NOM-P, DIR-P, TELEFONO-P, CELULAR-P, NRO-CAMA,
NRO-PIEZA, CITOFONO

NOM-SISTEMA-SALUD, FECHA-ADMISION, FECHA-ALTA)

**1FN**

Vista3.1 OK

**2FN**

Vista3.1 OK

**3FN**

Vista3.1 ([NRO-P], NOM-P, DIR-P, TELEFONO-P, CELULAR-P, *NRO-CAMA*,
NOM-SISTEMA-SALUD,

FECHA-ADMISION, FECHA-ALTA)

Vista3.2 ([NRO-CAMA], NRO-PIEZA, CITOFONO)

### Enfoque Bottom-Up -- Etapa 2 -- Ejercicio de Normalización Caso Hospital -- Vista 4

**Supuesto:** se considerará atributo derivable a la Fecha, suponiendo
que corresponde a la fecha del día en que se saca el informe por lo cual
se extraerá desde el calendario del computador.

**Mejora:** se agrega celular.

**Vista no normalizada**

Vista4 (NRO-M, NOM-M, DIR-M, TELEFONO-M, CELULAR-M, ESPECIALIDAD,

{NRO-P, NOM-P, NRO-CAMA, NRO-PIEZA, DIAG})

**1FN**

Vista4.1 ([NRO-M], NOM-M, DIR-M, TELEFONO-M, CELULAR-M,
ESPECIALIDAD)

Vista4.2 ([*NRO-M*, NRO-P], NOM-P, NRO-CAMA, NRO-PIEZA, DIAG)

**2FN**

Vista4.1 OK

Vista4.2.1 ([*NRO-M*, *NRO-P*], DIAG)

Vista4.2.1 ([NRO-P], NOM-P, NRO-CAMA, NRO-PIEZA)

**3FN**

Vista4.1 OK

Vista4.2.1 ([*NRO-M*, *NRO-P*], DIAG)

Vista4.2.1.1 ([NRO-P], NOM-P, *NRO-CAMA*)

Vista4.2.1.2 ([NRO-CAMA], NRO-PIEZA)


## Enfoque Bottom-Up -- Etapa 2 - Pasos

La etapa de Diseño Lógico de la BDR, implica llevar a cabo los
siguientes pasos:

1.  Normalización

2.  Integración de resultados de la normalización

3.  Generación del modelo de datos lógico

4.  Revisión del modelo diseñado

**PASO 1:**

Implicó normalizar cada vista a 3FN, logrando como resultado para el
Caso El Hospital el siguiente conjunto de vistas más pequeñas:

**VISTA1**

Vista1.1.1 ([NRO-F], FECHA-F, *NRO-P*, TOTAL)

Vista1.1.2 ([NRO-P], NOM-P, DIR-P, CIUDAD-P, COD-SISTEMA-SALUD)

Vista1.2.1 ([*NRO-F*, *NRO-I*], VALOR-I)

Vista1.2.2 ([NRO-I], NOM-I)

**VISTA2**

Vista2.1.1 ([NRO-CAMA], NRO-PIEZA, TIPO-CAMA, *NRO-P*)

Vista2.1.2 ([NRO-P], NOM-P, FECHA-ALTA-ESPERADA)

**VISTA3**

Vista3.1 ([NRO-P], NOM-P, DIR-P, TELEFONO-P, CELULAR-P, *NRO-CAMA*,
NOM-SISTEMA-SALUD,

FECHA-ADMISION, FECHA-ALTA)

Vista3.2 ([NRO-CAMA], NRO-PIEZA, CITOFONO)

**VISTA4**

Vista4.1 ([NRO-M], NOM-M, DIR-M, TELEFONO-M, CELULAR-M,
ESPECIALIDAD)

Vista4.2.1 ([*NRO-M*, *NRO-P*], DIAG)

Vista4.2.1.1 ([NRO-P], NOM-P, *NRO-CAMA*)

Vista4.2.1.2 ([NRO-CAMA], NRO-PIEZA)

**PASO 2:**

Implica integrar el resultado anterior, juntando aquellas vistas que
tienen igual PK, de tal manera de dejar juntos aquellos atributos que se
refieren a una misma entidad. Al hacer este paso de integración, puede
surgir un problema: esto es que se generen dependencias transitivas.

En el caso del hospital la integración genera el siguiente conjunto de
vistas o tablas:

FACTURA ([NRO-F], FECHA-F, *NRO-P*, TOTAL)

PACIENTE ([NRO-P], NOM-P, DIR-P, CIUDAD-P, COD-SISTEMA-SALUD,
FECHA-ALTA-ESPERADA,

> TELEFONO-P, CELULAR-P, *NRO-CAMA*, NOM-SISTEMA-SALUD, FECHA-ADMISION,
>
> FECHA-ALTA) (1)

DETALLE-FACTURA ([*NRO-F*, *NRO-I*], VALOR-I)

ITEM ([NRO-I], NOM-I)

CAMA ([NRO-CAMA], NRO-PIEZA, TIPO-CAMA, *NRO-P,* CITOFONO) (2)

MEDICO ([NRO-M], NOM-M, DIR-M, TELEFONO-M, CELULAR-M, ESPECIALIDAD)

PACIENTES-MEDICOS ([*NRO-M*, *NRO-P*], DIAG)

(1) Se logra de integrar Vista1.1.2, Vista2.1.2, Vista3.1, Vista4.2.1.1

(2) Se logra de integrar Vista2.1.1, Vista3.2, Vista4.2.1.2

Si se analiza cada tabla resultante, se detecta que, salvo una de ellas,
todas las demás están en 3FN. La tabla PACIENTE al juntar atributos que
venían de distintas vistas, quedó en 2FN, pues se generó una dependencia
transitiva entre COD-SISTEMA-SALUD NOM-SISTEMA-SALUD, por lo que hay que
llevarla a 3FN, quedando como:

PACIENTE ([NRO-P], NOM-P, DIR-P, CIUDAD-P, *COD-SISTEMA-SALUD*,
FECHA-ALTA-ESPERADA,

> TELEFONO-P, CELULAR-P, *NRO-CAMA*, FECHA-ADMISION, FECHA-ALTA)

SISTEMA-SALUD ([COD-SISTEMA-SALUD,] NOM-SISTEMA-SALUD)

**PASO 3:**

El modelo de datos lógicos requerido para obtener las 4 vistas del Caso
El Hospital, corresponde al siguiente conjunto de tablas:

FACTURA ([NRO-F], FECHA-F, *NRO-P*, TOTAL)

PACIENTE ([NRO-P], NOM-P, DIR-P, CIUDAD-P, *COD-SISTEMA-SALUD*,
FECHA-ALTA-ESPERADA,

> TELEFONO-P, CELULAR-P, *NRO-CAMA*, FECHA-ADMISION, FECHA-ALTA)

SISTEMA-SALUD ([COD-SISTEMA-SALUD,] NOM-SISTEMA-SALUD)

DETALLE-FACTURA ([*NRO-F*, *NRO-I*], VALOR-I)

ITEM ([NRO-I], NOM-I)

CAMA ([NRO-CAMA], NRO-PIEZA, TIPO-CAMA, *NRO-P,* CITOFONO)

MEDICO ([NRO-M], NOM-M, DIR-M, TELEFONO-M, CELULAR-M, ESPECIALIDAD)

PACIENTES-MEDICOS ([*NRO-M*, *NRO-P*], DIAG)

**PASO 4:**

Es posible durante los diferentes pasos de esta etapa ir haciendo
revisiones al modelo que se está generando, y realizar las mejoras
pertinentes, por ejemplo, la posibilidad que se produzcan dependencias
transitivas al integrar vuelve la tabla a 2FN, y para mejorarlas se
lleva a 3FN.

Adicionalmente, algunas otras mejoras que se podrían proponer se pueden
relacionar con la semántica de tiempo:

¿Cómo podemos registrar la historia de los Pacientes en el Hospital?

Agregando un Histórico de Pacientes con "time-stamps" formado por la
FECHA-ADMISION, quedando:

PACIENTE ([NRO-P], NOM-P, DIR-P, CIUDAD-P,
*COD-SISTEMA-SALUD-ACTUAL,* TELEFONO-P, CELULAR-P,

> NRO-CAMA-ACTUAL)

HISTORICO-PACIENTE ([NRO-P, FECHA-ADMISION], *COD-SISTEMA-SALUD*,
FECHA-ALTA-ESPERADA, *NRO-CAMA*,

> FECHA-ALTA)

Y esto también podría tener un efecto en PACIENTES-MEDICOS ([*NRO-M*,
*NRO-P*], DIAG), ya que para guardar la historia conviene mejor
tener: PACIENTES-MEDICOS ([*NRO-M*, *NRO-P*],
[*FECHA-ADMISION*,] DIAG)

Pero esta mejora no la incorporaremos, más que nada porque necesitamos
avanzar en la materia, y es una mejora similar a la de Paciente.

Finalmente tenemos como modelo de datos relacional para el Caso El
Hospital a:

FACTURA ([NRO-F], FECHA-F, *NRO-P*, TOTAL)

PACIENTE ([NRO-P], NOM-P, DIR-P, CIUDAD-P,
*COD-SISTEMA-SALUD-ACTUAL,* TELEFONO-P, CELULAR-P,

> NRO-CAMA-ACTUAL)

HISTORICO-PACIENTE ([NRO-P, FECHA-ADMISION], *COD-SISTEMA-SALUD*,
FECHA-ALTA-ESPERADA, *NRO-CAMA*,

> FECHA-ALTA)

SISTEMA-SALUD ([COD-SISTEMA-SALUD,] NOM-SISTEMA-SALUD)

DETALLE-FACTURA ([*NRO-F*, *NRO-I*], VALOR-I)

ITEM ([NRO-I], NOM-I)

CAMA ([NRO-CAMA], NRO-PIEZA, TIPO-CAMA, *NRO-P,* CITOFONO)

MEDICO ([NRO-M], NOM-M, DIR-M, TELEFONO-M, CELULAR-M, ESPECIALIDAD)

PACIENTES-MEDICOS ([*NRO-M*, *NRO-P*], DIAG)

Este modelo puede ser ahora graficado con alguna de las notaciones
aprendidas en Unidad II.

Mayores detalles ver Planilla Excel de apoyo utilizada en clases
(Vistas-Caso-Hospital-Cap3).

### Enfoque Bottom-Up -- Etapa 2 -- Ejercicio de Normalización Caso Hospital -- MDL

Más que nada lo que se busca presentar es cómo poder estimar el volumen que ocupará la BD una vez que esté implementada.

Para ello se agrega a cada entidad la cantidad de ocurrencias que tendría en la actualidad, y en las asociaciones el promedio de ocurrencias de la otra entidad que estaría vinculada en la relación. Por ejemplo, suponiendo que en este Hospital en la actualidad existes 100 camas, 30 médicos y 500 ítems en la farmacia, la estimación del volumen de datos se logra:

1.  Para cada entidad del tipo padre "puro" del modelo (entidad que no depende de otra) se pone lo que existe en la realidad. En este caso     son entidades de este tipo: CAMA, MEDICO e ITEMS, y según la realidad actual del Hospital se tendría tabla CAMA con 100 filas, tabla MEDICO con 30 filas y tabla ITEMS con 500 filas.

2.  Para el resto de las entidades, se cuantifica cómo es su asociación con las otras entidades. En este caso, tenemos las siguientes  posibilidades:

    - Si en promedio un paciente está 3 días ocupando una cama, y si este modelo está pensado para un horizonte de tiempo de 1 mes
        (30 días), se tiene que una cama se puede reusar en ese mes 10
        veces (30/3=10), por ello hay un 10 entre CAMA y PACIENTE.
        Además, eso lleva a que en 1 mes se pueden tener 1.000 pacientes
        hospitalizados (100 camas \* 10 pacientes en un mes = 1.000).

    -   Si en promedio un paciente consume 10 ítems durante su estadía en el hospital, se tiene que la tabla GASTOS puede llegar a
        tener 10.000 filas (1.000 pacientes \* 10 items en promedio = 10.000).

    -   Si en promedio un paciente tiene 3 tratamientos diferentes cada
        vez que se hospitaliza, se tiene que la tabla TRATAMIENTO puede
        llegar a tener 3.000 filas (1.000 pacientes \* 3 tratamientos en
        promedio = 3.000).

    -   La asociación entre MEDICO y TRATAMIENTO se cuantifica
        considerando 3.000/30, es decir, existen 3.000 tratamientos y 30
        médicos, por lo que en promedio un médico da 100 tratamientos.

    -   La asociación entre ITEMS y GASTOS se cuantifica considerando
        10.000/500, es decir, existen 10.000 gastos y 500 ítems, por lo
        que en promedio un ítem aparece como consumido en 20 gastos.

> Todo esto permite lograr la estimación que se entrega en siguiente tabla:

|TABLA      |Nro. filas|Caracteres|Tamaño|
|-----------|----------|----------|------|
|CAMA       |100       |30        |3     |
|MEDICO     |30        |200       |6     |
|ITEMS      |500       |80        |40    |
|PACIENTE   |1         |200       |200   |
|GASTOS     |10        |50        |500   |
|TRATAMIENTO|3         |100       |300   |
|Total caracteres|          |          |1.049.000|
|Total Mbytes|          |          |1     |
|40%crecimiento|          |          |0,4   |
|Estimación volumen BD Hospital|          |          |1,4Mb |

## Enfoque Bottom-Up -- Etapa 2 -- ¿Por qué normalizar?

Hay 2 motivos principales del por qué es útil normalizar al menos a 3FN:

1.  Para reducir la redundancia de datos

2.  Para reducir las anomalías de mantención, esto es las operaciones de
    inserción, eliminación y actualización sobre una base de datos.

La aplicación del resto de las formas normales soluciona el problema de
anomalías restantes, pero genera un número mayor de relaciones o tablas,
por lo que reconstruir la vista original puede ser muy costoso.

Por ende, un equilibrio entre normalización y eficiencia se logra
aplicando el proceso hasta la 3FN. A través del siguiente ejemplo de
normalización, se precisará esto.

En esta imagen se muestra la vista que servirá de ejemplo. Se trata de una Factura simplificada, con los atributos básicos, y donde se hará un supuesto fuerte de que el precio del producto no cambia en el tiempo, más que nada porque el objetivo de este ejemplo no es cubrir semántica de tiempo, si no que aprender por qué normalizar.

![Vista Ejemplo](imagenes/vista_ejemplo.png "Vista Ejemplo")

### Enfoque Bottom-Up -- Etapa 2 -- ¿Por qué normalizar a 1FN?**

La Factura expresada como una vista no normalizada es:

Factura ([\#factura], fecha, RUT-cliente, nombre-cliente,
teléfono-cliente,

{[código-producto], nombre-producto, precio, cantidad})

Si se considera que ésta es una relación, tabla o archivo, a simple
vista \#factura sería clave única (PK), pero no lo es pues habrá que
tener tantas filas como detalles de factura existan en ésta, debiéndose
repetir dicha clave.

Esto genera un alto grado de redundancia pues habrá datos que se
guardarán muchas veces como, por ejemplo, el nombre y precio de un
producto muy apetecido se repetirá en todas las facturas asociadas a la
venta de él. Además, se generan anomalías de mantención, como sería que
al insertar un nuevo producto es necesario tener una factura, o al
querer cambiar el precio, va a ser necesario recorrer todas las filas
donde ese producto haya sido vendido, o al eliminar una factura, se
podría perder información de algún producto.

El problema anterior se soluciona pasando a 1FN, es decir eliminando los
llamados "grupos repetitivos". Es así como se define que una vista se
encuentra en **primera forma normal (1FN) si no presenta grupos
repetitivos.**

Para la vista del ejemplo en desarrollo, su 1FN queda como:

Factura ([\#factura], fecha, RUT-cliente, nombre-cliente,
teléfono-cliente)

Detalle ([*\#factura*, código-producto], nombre-producto, precio,
cantidad)

Es decir, se divide la vista no normalizada, quedando en una tabla los
atributos no repetitivos y en otra los repetitivos, y a través de las PK
y FK se genera la asociación entre ellas. La tabla que registra los
grupos repetitivos debe tener cómo FK la PK de la otra tabla, y en la
mayoría de los casos, su PK es compuesta (la excepción se da cuando el
grupo repetitivo tiene un atributo que por sí solo es PK).

Hay que recordar que se utiliza para PK subrayar el atributo y para FK ponerlo en cursiva. Además, en esta imagen se grafica el modelo que representa el resultado de estar en 1FN.

![Relación 1N normalizado](imagenes/1N_normalizado.png "1N normalizado")

### Enfoque Bottom-Up -- Etapa 2 -- ¿Por qué normalizar a 2FN?

No obstante, aunque ya se está en 1FN, esta vista aún presenta algunos
problemas:

-   **Redundancia de datos**: los datos de cada producto aparecen tantas
    veces según la cantidad de detalles a los cuales pertenezcan.

-   **Anomalía de Inserción**: si se desea insertar los datos de un
    nuevo producto, no se podrá hacer hasta que no se haya vendido al
    menos una unidad, y pueda ser incluido en algún detalle.

-   **Anomalía de Eliminación**: si hubiera sólo un detalle asociado a
    cierto producto, y este detalle se elimina (archiva), entonces
    también se borran los únicos datos del producto.

-   **Anomalía de Actualización**: al actualizar el precio de un
    producto, deberá modificarse en todos los detalles asociados.

Los problemas anteriores se solucionan eliminando las llamadas
"**dependencias parciales**".

Una dependencia parcial se puede representar como: [a, b] c

y además, se debe dar una de las dependencias funcionales siguientes:
[b] c y/o [a] c

Luego, una vista se encuentra en **segunda forma normal (2FN)** si está
en primera forma normal (1FN) y no presenta dependencias parciales.

Para la vista del ejemplo en desarrollo, las dependencias parciales
identificadas son:

[\#factura], [código-producto] nombre-producto

[código-producto] nombre-producto

[\#factura], [código-producto] precio

[código-producto] precio

Luego la vista en desarrollo en 2FN queda como:

Factura ([\#factura], fecha, RUT-cliente, nombre-cliente,
teléfono-cliente)

Detalle ([\#*factura*], *[código-producto]*, cantidad)

Producto ([código-producto], nombre, precio)

Hay que recordar que se utiliza para PK subrayar el atributo y para FK ponerlo en cursiva. Además, en esta imagen se grafica el modelo que representa el resultado de estar en 2FN.

![Relación 2N normalizado](imagenes/2FN_normalizado.png "2N normalizado")

### Enfoque Bottom-Up -- Etapa 2 -- ¿Por qué normalizar a 3FN?

No obstante, aunque ya se está en 2FN, esta vista aún presenta algunos
problemas:

-   **Redundancia de datos**: los datos de cada cliente aparecen tantas
    veces según la cantidad de facturas asociadas.

-   **Anomalía de Inserción**: si se desea insertar los datos de un
    nuevo cliente, no se podrá hacer hasta que no tenga asociada al
    menos una factura que lo incluya.

-   **Anomalía de Eliminación**: si hubiera sólo una factura asociada a
    cierto cliente, y ésta se elimina (archiva), entonces también se
    borran los datos del cliente.

-   **Anomalía de Actualización**: al actualizar el teléfono de un
    cliente, deberá modificarse en todas las facturas asociadas.

Los problemas anteriores se solucionan eliminando las llamadas
"**dependencias transitivas**".

Una dependencia transitiva se puede representar como:

a b a c b c

donde una de las dependencias funcionales es entre atributos no clave
(PK).

Luego, una vista se encuentra en **tercera forma normal (3FN)** si está
en segunda forma normal (2FN) y no presenta dependencias transitivas.

Para la vista del ejemplo en desarrollo, las dependencias parciales
identificadas son:

[\#factura] RUT-cliente

[\#factura] nombre-cliente

RUT-cliente nombre-cliente

[\#factura] RUT-cliente

[\#factura] teléfono-cliente

RUT-cliente teléfono-cliente

Luego la vista en desarrollo en 3FN queda como:

Cliente ([RUT], nombre, teléfono)

Factura ([\#factura], fecha, *RUT*)

Detalle ([\#factura], *[código-producto]*, cantidad)

Producto ([código-producto], nombre, precio)

Hay que recordar que se utiliza para PK subrayar el atributo y para FK ponerlo en cursiva. Además, en esta imagen se grafica el modelo que representa el resultado de estar en 3FN.

![Relación 3N normalizado](imagenes/3FN_normalizado.png "3N normalizado")

### Enfoque Bottom-Up -- Etapa 2 -- Consideraciones en la integración

La integración busca concentrar todas las relaciones (o tablas)
obtenidas de la normalización de las vistas en un único modelo. Se
realiza juntando relaciones (o tablas) que tienen igualdad de PK.

Es importante al integrar tener en cuenta:

-   Sinónimos y homónimos.

-   Posibilidad que se produzcan dependencias transitivas

-   Uso de generalización

A través del siguiente ejemplo se precisarán estos 3 aspectos.

Dado que las vistas recogen visiones de diferentes usuarios sobre los
datos, es factible que se detecte que algunos se refieren de forma
diferente a un mismo atributo (sinónimos) o de la misma manera a
atributos que son distintos (homónimos). Así podemos tener:

-   **Sinónimos:** un mismo atributo es referenciado con nombres
    diferentes; por ejemplo, RUT y CódigoPersona.

-   **Homónimos:** atributos diferentes son referenciados con el mismo
    nombre; por ejemplo, Nombre y Nombre-Completo.

Otra consideración para tener en cuenta al integrar el resultado de
normalizar cada vista es que se generen **dependencias transitivas**,
dado que una vista puede aportar con atributos que dependan de algún
otro que estaba en la vista con la cual se está integrando.

Por ejemplo, si suponemos que de dos vistas diferentes se llegó a las
siguientes tablas:

TRABAJADOR ([RUT], nombre, código_cargo)

TRABAJADOR ([RUT], nombre, nombre_cargo)

La integración de ambas tablas resulta en:

TRABAJADOR ([RUT], nombre, código_cargo, nombre_cargo)

donde:

[RUT] código_cargo

[RUT] nombre_cargo

código_cargo nombre_cargo

Situación que deja la tabla en 2FN, y que debe ser corregida llevándola
a 3FN, quedando como:

TRABAJADOR ([RUT], nombre, *código_cargo*)

CARGO ([código-cargo,] nombre_cargo)

Hay que recordar que se utiliza para PK subrayar el atributo y para FK
ponerlo en cursiva.

Otra consideración para tener en cuenta al integrar el resultado de
normalizar cada vista es que se genere una tabla que está representando
a tipos de entidades diferentes, por lo que es posible aplicar en este
caso semántica de **generalización o herencia**.

Por ejemplo, si suponemos que de dos vistas diferentes se llegó a las
siguientes tablas:

PACIENTE ([\#paciente], nombre, ..., fecha_atención,
diagnósticoConsulta)

PACIENTE ([\#paciente], nombre, ..., fecha_atención, \#pieza)

En principio, al integrar, deberían conformar una misma tabla:

PACIENTE ([\#paciente], nombre, ..., fecha_atención,
diagnósticoConsulta, \#pieza)

Pero se pueden confundir los pacientes ambulatorios de los que se
hospitalizan, es aquí donde la generalización ayuda a representar de
mejor forma esta realidad.

Las alternativas posibles para implementar la generalización en el
ejemplo anterior serían:

**Alternativa 1:**

Paciente ([\#paciente], nombre, ..., fecha_atención,
diagnósticoConsulta, \#pieza, tipoPaciente)

**Alternativa 2:**

Paciente ([\#paciente], nombre, ..., fecha_atención)

PacienteAmbulatorio ([\#paciente], diagnósticoConsulta)

PacienteInternado ([\#paciente], \#pieza)

**Alternativa 3:**

PacienteAmbulatario ([\#paciente], nombre, ..., fecha_atención,
diagnósticoConsulta)

PacienteInternado ([\#paciente], nombre, ..., fecha_atención,
\#pieza)

¿Cuál es mejor? La que permita un mejor equilibrio entre tiempos de
respuestas, rendimiento y uso del espacio de almacenamiento (criterios
de diseño físico que se verán en próximo capítulo).

UNIDAD 3: Diseño lógico BDR - Caso
Universidad**

Esta sesión será completamente de ejercicios sobre normalización, se
trabajará con el Caso Universidad, donde vamos a suponer que se han
identificado 3 vistas de usuarios:

-   Vista 1: Informe Académico, fue recolectada en la entrevista con el
    usuario estudiante.

-   Vista 2: Lista de Alumnos, fue recolectada en la entrevista con el
    usuario profesor.

-   Vista 3: Programa Asignatura, fue recolectada en la entrevista con
    el usuario exalumno.

Antes de partir con la normalización, y pensando en que estamos en la
Etapa 1 de Formulación y Análisis de Requerimientos, vamos a plantearnos
2 mejoras:

1.  ¿Qué mejoras se pueden realizar a las vistas existentes? A la Vista
    > 3 la vamos a proyectar hacia un sistema de Biblioteca donde se
    > almacenen la distinta bibliografía asociada a una asignatura,
    > teniendo en cuenta que una asignatura puede utilizar varias
    > bibliografías y que una bibliografía puede ser usada por varias
    > asignaturas

2.  ¿Qué nuevas vistas se pueden crear? Una vista que permita al jefe de
    > carrera de cada campus tener un gráfico con el % de reprobación de
    > las asignaturas dictadas por el DI en cada semestre.
