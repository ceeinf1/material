# UNIDAD 2: Modelos de datos conceptuales

En esta unidad veremos cómo diseñar modelos de datos usando la notación Crow's Perch. Por lo tanto el objetivo de aprendizaje es que el alumno Diseña modelos de datos para diferentes realidades o sistemas utilizando notaciones estándar de representación (como UML, Crow's Perch, E-R, E-R-E, entre otras).

En concreto, el temario de esta unidad consta de:
1. Características de los Datos y sus Asociaciones (relaciones)
2. Tipos de Modelos de Datos
3. Semántica de los Datos
4. Notaciones para Modelos de Datos
5. Ejercicios de Modelos de Datos Conceptual


## Características de los datos y sus asociaciones (relaciones)

Para describir qué es un dato, deben considerarse tres niveles de abstracción o estados en que se puede encontrar el dato:
* Realidad constituida por personas, objetos, lugares y eventos.
* Diccionario de Datos donde se almacenan los metadatos o definiciones sobre los datos.
* Base de Datos donde se almacenan los valores.
Estos niveles van desde lo más abstracto (realidad) o lo más alejado de nuestro objetivo final que es contar con una BD implementada, hasta lo menos abstracto que es la BD misma. 

En esta unidad profundizaremos en el dato en su estado dentro de la realidad a la que queremos diseñar y construirle una BD.

### Niveles de abstracción del dato

Estos niveles van desde lo más abstracto (realidad) o lo menos abstracto (valores en la BD). Desde el punto de vista de los datos, en la Realidad éstos se encuentran como tipos de entidades o clases que pueden representar personas, objetos, lugares y eventos del mundo real; luego pasan a un nivel menos abstracto cuando apoyados en el DBMS, se convierten en metadatos (definiciones acerca de los datos generadas con el DDL del DBMS) que quedan almacenados en el Diccionario de Datos; y finalmente, se transforman en la BD misma cuando se ingresan sus valores a nivel de ocurrencias de registros o filas dentro de una tabla.
Profundizando en el nivel de la realidad, debemos precisar que los tipos de entidades identificados tienen características que permiten describirlas, a los que se les llama atributos y participan de asociaciones lógicas con otras entidades que también son importantes de representar. 
Una precisión que haremos en cuanto al lenguaje usado es que para facilitar la comunicación hablaremos indistintamente de entidad o de tipo de entidad, siendo que en rigor a este nivel de manejan tipos de entidades que agrupan entidades que tienen las mismas características. 

### Niveles de abstracción del dato - Realidad
La realidad comprende el mundo real (sistema, organización), con sus componentes y el medio ambiente en el cual opera.
* Una Entidad es una persona, objeto (cosa, concepto), lugar y evento, sobre el cual la organización decide coleccionar y almacenar datos.
* Un Tipo (Clase) de Entidades es un conjunto de entidades que poseen características similares, por ejemplo, Clientes, Estudiantes, Pacientes.
* Un Atributo es una propiedad de una entidad que se desea registrar. Para cada tipo de entidades, existe un conjunto de atributos de interés para la organización.
* Cada entidad, dentro de una clase de entidades, debe tener al menos un atributo (o una combinación de atributos) que la distinga de otras entidades dentro de su clase, a lo cual se le denomina Clave Primaria (PRIMARY KEY).

La siguiente imagen muestra ejemplos de entidades.

![Ejemplos de entidades](imagenes/entidades.png "Ejemplos de entidades")

### Niveles de abstracción del dato – Diccionario de Datos

Continuando con el análisis de los distintos niveles de abstracción en que podemos encontrar al dato, luego del nivel denominado Realidad donde lo encontramos como entidad con sus correspondientes atributos, pasamos a un nivel de abstracción menor, donde entra el juego el DBMS, en este nivel el dato está como metadato en el Diccionario de Datos.
En un Diccionario de Datos, el DBMS guarda información (metadatos) acerca de los ítems de datos de una organización.
Un ítem de dato es la unidad de datos más pequeña en una base de datos y, por lo tanto, la unidad de dato más pequeña con significado para el usuario; se refiere a lo que a nivel de la realidad a modelar llamamos atributo.
Por cada ítem de datos, el diccionario de datos guarda:
* Nombre, largo, tipo, formato.
* Breve descripción narrativa
* Rango(s), Dominio, ¿Es valor único (Unicidad)?, ¿Es valor Nulo?
* Posibles sinónimos
* Fuente (origen)

Tomando como ejemplo, las entidades de la imagen anterior, para la realidad Universidad, los atributos para la entidad ALUMNOS serían: RUT, NOMBRE, ROL, DIRECCION, REGION. A nivel del diccionario de datos tendríamos para la tabla ALUMNOS, una descripción de metadatos como los siguientes:

|COLUMN NAME|DATA TYPE   |PK |NN |UQ |…  |DEFAULT|COMMENT|
|-----------|------------|---|---|---|---|-------|-------|
|RUT        |VARCHAR (10)|√  |√  |   |   |       |       |
|NOMBRE     |VARCHAR (40)|   |√  |√  |   |       |       |
|FECHANAC   |DATE        |   |√  |   |   |       |       |
|DIRECCION  |VARCHAR (40)|   |√  |   |   |       |       |
|REGION     |INTEGER     |   |   |   |   |       |1..16  |
|CELULAR    |INTEGER     |   |   |   |   |NULL   |       |


Además, en el Diccionario de Datos podría haber una descripción general del conjunto de itemes de datos a nivel de una descripción de un tipo de registro o clase de entidad.
Un Registro es un conjunto de ítemes de datos y/o datos agregados, y corresponde a la definición de una clase de entidades.
De un registro de datos, el diccionario de datos guarda:
* Nombre, descripción, tamaño
* Sus ítems de datos
* Sus datos agregados

Por ejemplo, de la tabla ALUMNO, podría guardar que cada fila es de 120 bytes, que DIRECCION se desagrega en CALLE, NRO, CIUDAD.

### Niveles de abstracción del dato – Base de Datos
De la Realidad (entidades y asociaciones) pasamos a un nivel de abstracción menor denominado Diccionario de Datos (metadatos) y de ahí llegamos al nivel de abstracción menor donde está la BD (valores).
La Base de Datos corresponde al nivel donde físicamente se guardan los datos.
* Existe un registro por cada objeto o entidad del mundo real.
* Se almacenan ocurrencias de datos incluidas sus relaciones o asociaciones.
* Conjuntos de archivos asociados lógicamente entre sí.

### Características de las Relaciones (o asociaciones)
En la realidad encontramos entidades (con sus atributos) y asociaciones que permiten relacionar lógicamente a las entidades entre sí.
Una relación es una asociación entre tipos de entidades de una realidad. Pueden ser de los siguientes tipos:
* Una
* Muchas
* Condicional
* En Ambos Sentidos (las más comunes)
* Recursivas
* Múltiples asociaciones entre entidades

Existen diversas notaciones para representarlas. Tenemos flechas, líneas, símbolos especiales, todo depende de la simbología que usemos. Lo más común es que la entidad se represente con un rectángulo, dentro del cual se escriban los atributos, y las asociaciones a través de líneas que indiquen hacia alguna dirección, parecidas a flechas, mostrando la entidad origen y la entidad destino de la asociación.

### Ejemplos de asociaciones
**Asociación de tipo UNA**: en un Hospital encontramos Pacientes que están asignados a Camas. Un paciente ocupa UNA cama. Si hay que tener en cuenta, que en esta realidad nuestros pacientes no podrían por ejemplo cambiarse de camas durante su estadía en el Hospital, o si llegaran a cambiarse, sólo nos quedaríamos con los datos de la última cama que utilizó; además, todos los pacientes quedan asignados a una cama, por lo que en este Hospital no podría haber pacientes ambulatorios.

**Asociación de tipo MUCHAS**: en una Empresa encontramos Empleados que tienen Cargas Familiares. Un Empleado puede tener MUCHAS Cargas Familiares. Si hay que tener en cuenta que en esta realidad estamos representando que todos los empleados tienen cargas familiares, lo cual es una restricción muy fuerte. 
Como podemos ver ambas asociaciones tienen problemas, son muy restrictivas, no dejan representar bien la realidad a modelar, sigamos avanzando para encontrar asociaciones más inclusivas.


**Asociación de tipo CONDICIONAL**: se representa con un cero sobre la línea, veamos dos ejemplos:
* En un Hospital encontramos Pacientes que pueden están asignados a Camas (pacientes hospitalizados) o no necesitan ser asignados a Camas (pacientes ambulatorios).
* En un Empresa encontramos Empleados que pueden tener cero, una o más Cargas Familiares. 

Asociaciones en ambos sentidos: estas son las asociaciones más comunes, las más inclusivas, pues permiten representar de mejor forma una realidad. Son con las cuales trabajaremos siempre. No olvidar “revisar siempre ambos sentidos”. Hay tres tipos de estas asociaciones:
* 1:1 (UNO a UNO)
* 1:M (UNO a MUCHOS)
* M:N (MUCHOS a MUCHOS)

**Una asociación 1:1**, la explicaremos con el caso del Hospital: Un Paciente ocupa una o ninguna Cama (se muestra en la figura con la flecha que sale de Paciente hacia Cama), pero además podemos decir que una Cama tiene un solo Paciente o está desocupada (se muestra en la figura con la flecha que sale de Cama a Paciente). Los ceros en ambos lados de la asociación representan que puede haber pacientes ambulatorios (cero al lado derecho) y camas desocupadas (cero al lado izquierdo).

**Una asociación 1:M**, la explicaremos con el caso de la Empresa: Un Empleado tiene cero, una o muchas Cargas Familiares (se muestra en la figura con la flecha que sale de Empleado hacia Carga), pero además podemos decir que una Carga tiene un solo Empleado como responsable de ella, incluso esto por Ley de la República es así (se muestra en la figura con la flecha que sale de Carga a Empleado). El cero en el lado de Cargas representa que puede haber Empleados que no tienen cargas familiares; por otro lado, si miramos de Carga a Empleado, es importante destacar, que no es posible poner un cero porque si existe una Carga, debe haber un Empleado responsable de ella (esto es una asociación obligatoria, es decir, la asociación siempre debe existir). Las asociaciones 1:M son las más comunes de encontrar en cualquier realidad.

**Una asociación N:M**, la explicaremos con el caso de una Universidad: Un Alumno puede tomar cero, una o más Asignaturas en un semestre (se muestra en la figura con flecha que sale de Alumno hacia Asignatura), pero además sabemos que una Asignatura puede ser cursada por cero, uno o más Alumnos (se muestra en la figura con flecha de Asignatura hacia Alumno). Estas asociaciones son las más complejas, no pueden transformarse directamente a un modelo lógico, requieren de un paso intermedio para reducir la complejidad antes de llegar a las tablas del modelo relacional.

Para entender mejor estos tres tipos de asociaciones, analicemos cómo estos modelos de datos conceptual quedarían posteriormente implementados en una tabla. Se usa la notación de subrayar con línea continua clave primaria y línea punteada clave foránea.

**Tablas para la asociación 1:N, Personas y Cargas**

|RUT      |NOMBRE      |DIRECCION|
|---------|------------|---------|
|111-1    |José        |Viña     |
|222-2    |Isabel      |Santiago |
|333-3    |Simón       |Quilpué  |
|444-4    |Alberto     |Viña     |
|555-5    |Jorge       |Santiago |


|RUT-C    |NOMBRE-C    |F-NACIM|RUT-E|
|---------|------------|-------|-----|
|91111-9  |Pepita      |10/12/2010|111-1|
|92222-2  |Pepito      |15/04/2012|111-1|
|94323-2  |Gustavo     |25/08/2018|111-1|
|93452-5  |Isabelita   |18/09/2012|222-2|
|94569-1  |Luciano     |12/02/2019|222-2|
|98765-0  |Jorguito    |12/05/2015|555-5|
|96734-9  |María       |12/02/2020|555-5|


La asociación 1:M entre Empleado y Carga familiar, queda representada por la clave foránea que se agregan en tabla Cargas. El hecho que hay empleados sin carga familiar queda reflejado en que hay RUT de empleados que no están en los valores de la columna RUT-E de tabla Cargas, por ejemplo, Simón y Jorge no tienen cargas familiares, para lo cual es necesario hacer un JOIN entre tablas a través del atributo RUT. ¿Cómo podría reducirse los tiempos de respuesta para determinar si un empleado tiene o no cargas familiares? Agregando un atributo Cantidad-de-Cargas es tabla Empleado, sería un atributo que podría ser derivable, pero es preferible generar la redundancia en pro de mejores tiempos de respuesta.

Es importante al crear las tablas con SQL, definir las claves primarias de ambas tablas y la foránea en tabla Cargas la que en este caso no debe permitir el valor NULL (pues por Ley de la República toda carga familiar debe tener una persona que se haga responsable de ella) y si puede permitir duplicados (NOT UNIQUE, que se asume por defecto). Recordar, además, que la clave primaria por definición no puede tener valores NULL.


**Tablas para asociación M:N**

Estas asociaciones por su complejidad no pueden implementarse directamente en una base de datos relacional, requieren ser primero descompuestas en asociaciones 1:M y generar entre ellas una tabla de intersección (o también conocida como tabla NUB) que almacene atributos que forman parte de la asociación misma. Por ejemplo, en el caso de la Universidad, para guardar la Nota que un Alumno ha obtenido en una Asignatura se requiere crear esta entidad de intersección, que estaría compuesta de la clave primaria de Alumno + clave primaria de Asignatura.
Tendríamos entonces las siguientes tablas:

**Tabla alumnos**

|RUT      |NOMBRE      |DIRECCION|
|---------|------------|---------|
|111-1    |José        |Viña     |
|222-2    |Isabel      |Santiago |
|333-3    |Simón       |Quilpué  |
|444-4    |Alberto     |Viña     |
|555-5    |Jorge       |Santiago |

**Tabla Asignaturas**

|SIGLA    |NOMBRE-A    |CREDITOS|AÑOCREACION|
|---------|------------|--------|-----------|
|INF239   |Bases de Datos|3       |2014       |
|IWI131   |Programación|3       |2013       |
|INF325   |BD Avanzada |3       |2019       |
|MAT021   |Mate 1      |5       |2000       |
|MAT022   |Mate 2      |5       |2000       |
|INF134   |Est. de Datos|3       |2014       |
|INF270   |OSI         |3       |2016       |

**TABLA INTERSECCION ALUMNOS-ASIGNATURAS**

|RUT      |SIGLA       |SEM-AÑO|VTR |NOTA|
|---------|------------|-------|----|----|
|111-1    |INF239      |1/2019 |1   |80  |
|111-1    |MAT022      |1/2020 |1   |NULL|
|111-1    |INF270      |1/2020 |1   |NULL|
|333-3    |INF239      |1/2020 |1   |NULL|
|333-3    |INF270      |1/2020 |1   |NULL|
|555-5    |MAT022      |1/2019 |1   |50  |
|555-5    |MAT022      |1/2020 |2   |NULL|

*¿Cuál sería la clave primaria en la tabla de intersección?* Es una clave primaria compuesta de RUT, SIGLA, SEM-AÑO. También podría ser RUT, SIGLA, VTR. Ambas no permiten filas duplicadas en esta tabla, así es que cualquiera de ellas es una buena clave primaria.

Es importante tener en cuenta que la mayoría de las veces en este tipo de tablas de intersección la clave primaria es compuesta de las PK de cada una de las entidades que participan en la asociación, pero a veces además necesitan de algún atributo adicional para lograr la unicidad. Por ejemplo, en este caso si solo hubiese sido PK la combinación RUT y SIGLA, no se hubiera podido guardar la historia de las notas de cada alumno en una asignatura, solo se guardaría la nota de la última vez que fue cursada (se “pisa” o sobre escribe el registro con la nueva nota).

#### Ejemplos de asociaciones recursivas o loop (1:1 y 1:M)

Una asociación recursiva es la que puede tener una entidad consigo misma. Esta asociación puede ser 1:1, 1:M o M:N.
Para el caso de 1:1. por ejemplo, pensemos que tenemos una entidad con los datos de Personas y queremos dejar reflejado que una persona está “casada-con” otra cuyos datos forman también parte de esta entidad, esto se grafica con un loop donde la asociación puede ser condicional en ambos sentidos, ya que en la realidad existen personas solteras. En una tabla de una BD relacional esto quedaría como sigue:

*Tabla personas*
|RUT      |NOMBRE      |DIRECCION|CASADO-CON|
|---------|------------|---------|----------|
|111-1    |José        |Viña     |444-4     |
|222-2    |Isabel      |Santiago |555-5     |
|333-3    |Simón       |Quilpué  |NULL      |
|444-4    |Angela      |Viña     |111-1     |
|555-5    |Jorge       |Santiago |222-2     |

Es necesario agregar una clave foránea a la tabla Persona que permita guardar los RUT de quién está casado con quién. Es decir, una columna CASADO-CON donde los posibles valores que allí puede haber son todos los RUT que están en la primera columna de la tabla. La columna CASADO-CON debe ser definida como foránea, sin duplicados (UNIQUE) y que permita los valores NULL (para que pueda reflejarse que hay personas solteras).

Para el caso de recursiva de 1:M, por ejemplo, pensemos que tenemos una entidad con los datos de los Empleados y queremos dejar reflejado quién es el “jefe-de” cada empleado, teniendo en cuenta que una persona puede ser “jefe-de” cero, uno o más empleados y que todo empleado tiene un jefe directo, salvo la dueña de la empresa , esto se grafica con un loop donde la asociación puede ser condicional en un solo sentido (para representar que el dueño no tiene jefe), y que cualquier otro empleado tiene un solo jefe.

*Tabla empleados*
|RUT      |NOMBRE      |DIRECCION|SU-JEFE-ES|
|---------|------------|---------|----------|
|111-1    |José        |Viña     |444-4     |
|222-2    |Isabel      |Santiago |555-5     |
|333-3    |Simón       |Quilpué  |444-4     |
|444-4    |Angela      |Viña     |NULL      |
|555-5    |Jorge       |Santiago |444-4     |


Es importante al crear las tablas con SQL, definir la clave primaria y la foránea la que en este caso debe permitir el valor NULL y si puede permitir duplicados (NOT UNIQUE, que se asume por defecto). Recordar, además, que la clave primaria por definición no puede tener valores NULL.

#### Ejemplos de asociaciones recursivas o loop (M:N)

Para el caso de recursiva de M:N, por ejemplo, pensemos que tenemos una entidad con los datos de los Productos que se venden en una Fábrica. Dentro de estos productos hay algunos que están compuestos de otros, es decir, se venden productos completos o también se venden las piezas o componentes de esos productos a parte. Esto se grafica con un loop sobre la entidad Producto que indica que un producto puede estar formado por cero, uno o más piezas (que también son productos); y que cualquiera de esas piezas, puede formar parte de cero, uno o más productos.

Por ser una M:N para poder implementarla en una BD relacional, es necesario descomponerla, creando una entidad de intersección que contendrá como PK una clave primaria compuesta. Así se llega a dos tablas:

*Tabla Producto*

|#PROD    |NOMBRE      |ETC.|
|---------|------------|----|
|111-1    |Puerta      |….  |
|222-2    |Chapa       |….  |
|333-3    |Tornillos   |….  |
|444-4    |Manilla     |….  |
|555-5    |Vidrios     |….  |

*Tabla Componentes*

|#PROD    |#COMP       |CANT-USADA|
|---------|------------|----------|
|111-1    |222-2       |1         |
|111-1    |333-3       |500       |
|111-1    |444-4       |2         |
|111-1    |555-5       |1         |
|222-2    |333-3       |10        |
|444-4    |333-3       |5         |
|….       |….          |….        |

Es importante al crear las tablas con SQL, definir la clave primaria en ambas tablas, teniendo en cuenta que en la tabla Componentes, la PK es compuesta de #PROD y #COMP y que, además, cada una de estas columnas en forma separada son foráneas hacia Producto. Todas estas claves no pueden tener valores NULL.

#### Ejemplos de múltiples asociaciones entre entidades

Este tipo de asociaciones busca simplificar los modelos de datos, reduciendo la cantidad de entidades, para lo cual, dentro de lo posible, agrupa entidades de un mismo tipo. Por ejemplo, si se piensa en una Compañía de Seguros que vende pólizas de seguro de vida, donde cada póliza tiene a una persona que asegura su vida y a otra persona que será su beneficiario, en caso de que la primera fallezca.  

Una alternativa para modelar esta situación es definir una entidad para el Asegurado y otra para el Beneficiario, ambas con los mismos atributos. Pero otra alternativa, que disminuye el número de entidades, es tener una sola entidad Persona donde se agrupen tanto a quienes son asegurados como beneficiarios, debiendo agregar un atributo que refleje el rol de cada uno, e identificando en la asociación explícitamente ese rol, generándose dos asociaciones entre las mismas entidades.

Para la primera alternativa se necesitan 3 tablas: ASEGURADO, BENEFICIARIO y POLIZA, donde cada una debe tener su PK y en POLIZA se deben definir, además, 2 claves foráneas, una hacia Beneficiario y otra hacia Asegurado.

Para la segunda alternativa se necesitan 2 tablas: PERSONA y POLIZA, donde cada una debe tener su propia PK y en PERSONA se debe agregar un atributo que indique el tipo de persona, y en POLIZA, 2 claves foráneas ambas hacia Persona, pero una de ellas hacia quién cumple el rol de Asegurado y la otra hacia quien cumple el rol de Beneficiario.


### Modelo de datos

Un modelo es una representación o abstracción de la realidad: Por ejemplo: modelos de aviones, modelos matemáticos, maquetas, modelos de pasarela, etc. Un modelo de datos es una representación de entidades del mundo real (personas, objetos, lugares y eventos) y sus asociaciones, es decir, es una abstracción de la realidad a partir de sus datos. En un modelo de datos se tiene una descripción (abstracta) de todos los datos relacionados con las diversas actividades que en una organización se necesita, para que ésta pueda funcionar adecuadamente. Un modelo de datos se representa a través de diferentes notaciones o simbologías. En la imagen de ven “modelos de pasarela” y modelos de datos expresados en simbología de Bachman y de Entidad-Relacionamiento.

* El propósito de un modelo de datos es doble:
  * Representar una realidad a través de sus datos.
  * Hacer que los datos sean comprensibles a través de una **notación** estándar.
* Se pueden identificar distintos **tipos de modelos de datos** según su nivel de abstracción: conceptual, lógico, físico.
* Cada modelo de datos tiene sus restricciones en cuanto a su capacidad de representar una realidad, dependiendo de los tipos de **semánticas** que puede abarcar.


## Semántica de los datos

La semántica de los datos se define como el significado que tienen los datos en sí mismos y entre sí. Son metadatos que quedan muchas veces almacenados en el diccionario de datos.
A nivel conceptual, se representa generalmente por símbolos definidos como convención o norma; nosotros veremos la simbología o notación de los siguientes autores:
* Charles Bachman 
* Peter Chen (Entidad-Relacionamiento: E-R) 
* Grady Booch, Ivar Jacobson y Jim Rumbaugh (UML)
A continuación, se describen 8 tipos de semánticas, que se sugiere tener en mente como una guía cada vez que se modela una BD.

1.	  Cardinalidad (o conectividad o multiciplidad)
1.	  Grado
1.	  Dependencia existencial
1.	  Tiempo
1.	  Unicidad
1.	  Herencia (clase-subclase o generalización) 
1.	 Categorización (interfaces, herencia selectiva)
1.	 Agregación

### Cardinalidad
Se refiere al número de entidades involucradas en una asociación, es decir, al número de entidades con que otra entidad se relaciona. También se define como multiplicidades, y se distinguen los siguientes tipos ya vistos:
*  1 : 1 (Uno a Uno)
*  1 : N (Uno a Muchos)
* M : N (Muchos a Muchos)

#### Cardinalidad 1:1

**Asociación 1:1:** sucede cuando una entidad X se relaciona sólo con otra entidad, bajo determinada asociación, y esta última sólo lo hace con X.
El ejemplo es el de matrimonio entre dos personas, donde se identifica la asociación 1:1 entre Mago y Varita, es decir, un Mago puede tener una sola Varita, y la Varita puede pertenecer a un sólo Mago. 

![Asociación 1:1](imagenes/asoc11.png "Asociación 1:1")

#### Cardinalidad 1:M

**Asociación 1:M**: se presenta cuando una entidad X se relaciona con varias entidades, bajo determinada asociación, pero cada una de éstas sólo lo hace con X.
Por ejemplo, en la imagen se identifica la asociación 1:M entre Persona y Hechizo, es decir, una Persona ha lanzado cero, uno o más Hechizos; y un Hechizo es de propiedad de una sola persona (ha sido lanzado). 

![Asociación 1:M](imagenes/asoc1m.png "Asociación 1:M")

#### Semántica de los datos – Cardinalidad M: N

**Asociación M:N**: ocurre cuando una entidad X se relaciona con varias entidades, bajo determinada asociación, y cada una de éstas, a su vez, se relaciona con X y, probablemente, otras entidades más del mismo tipo de X.
En el ejemplo de la imagen se identifica la asociación M:N entre Mago y Hechizo, es decir. Un Mago puede lanzar cero, uno o más Hechizoa; y un Hechizo puede haber lanzado cero, una o más Hechizos. 

![Asociación M:N](imagenes/asocmn.png "Asociación M:N")

#### Cardinalidad con condicional
Al definir la cardinalidad sea de cualquiera de los tipos anteriores, ya vimos la necesidad de precisar si la asociación existe siempre o a veces no existe, es decir, es posible indicar que la asociación es obligatoria (o mandatoria) o es opcional.
En general se asume que es opcional, pero en algunas simbologías se precisa esto agregando un cero al definir la cardinalidad.
Por ejemplo, tomando los modelos expuestos en las 3 imágenes anteriores:

* Un Hechizo puede no haber sido lanzado, para lo cual se pone un cero al lado izquierdo de la asociación. Es importante detectar que, si existe una Hechizo, debe existir el Mago que lo lanza, por lo cual la condicional no se da de Hechizo a Persona, esa asociación es mandatoria.

* Un Mago puede lanzar cero, uno o muchos Hechizos, el cero al lado derecho estaría representando la condicional, pero en notación Crow's Perch, al no poner nada se asume que implica cero, uno o más. Es importante detectar que en el otro sentido la asociación es mandatoria, todo hechizo debe ser lanzado por un Mago.

* Una Asignatura debe tener al menos un Alumno o muchos, esto se representa en E-R con 1..*. En este caso, se da la misma cardinalidad en ambos sentidos.

### Grado
La semántica de grado se define como el número de tipos o clases de entidades que participan en una asociación. Se identifican los siguientes tipos de asociaciones según el grado:
* Unaria
* Binaria
* Ternaria
* N-aria

#### Grado Unaria
Asociación Unaria: asociación que considera un solo tipo de entidades que se relaciona consigo misma. Corresponden a las ya presentadas como asociaciones recursivas o “loop”, que pueden ser de cardinalidad: 1:1, 1:M o M:N.
En asociaciones unarias es conveniente aclarar los roles de los vínculos para evitar ambigüedades, poniendo un nombre sobre la asociación misma.

* 1:1 “Casado-con”: Una Persona está casada con otra Persona o no lo está.
* 1:M “Jefe-de”: Un Empleado es jefe de 0, una o más personas, todas las personas tienen un jefe.
* M:N “Componentes”: Un Producto está formado por cero, uno o más componentes, los cuales también son Productos.

#### Grado Binaria

**Asociación Binaria**: relación conformada por dos tipos de entidades asociadas entre sí. Son las que más comúnmente pueden encontrar en una realidad a modelar. Y pueden tener cardinalidad 1:1, 1:M y M:N. Ya vimos ejemplos de ellas cuando planteamos modelos para un Hospital, una Empresa y una Universidad.

####  Grado Ternario
**Asociación Ternaria**: relación conformada por tres tipos de entidades asociadas entre sí. Su cardinalidad suele ser de muchos a muchos, lo cual se expresa como M:N:O.

Por ejemplo, si pensamos en una fábrica, en la figura existe una asociación ternaria llamada Orden de Compra, que representa que a los Proveedores se les compra Materias Primas las cuales deben ser entregadas en las distintas Bodegas de la fábrica. Es decir, hay 3 entidades involucradas en la asociación: Proveedores, Materias Primas y Bodegas.

![Ejemplo grado ternario](imagenes/grado3.png "Ejemplo grado ternario")

En la figura también existe una asociación binaria, llamada Inventario, que involucra a solo 2 de las entidades antes mencionadas: Materias Primas y Bodegas. Esta asociación es redundante, como generalmente lo es todo ciclo cerrado que se produce en un modelo, pero cuando eso sucede se debe analizar si conviene mantener la redundancia, en pro de chequear cuadratura de datos y/o mejorar tiempos de respuesta. En este caso, se calcularía mucho más rápido el inventario, si solo se suma cuánto se tiene de cada producto en cada bodega, que si se sumara todas las ordenes de compras emitidas para cada materia prima y entregadas en cada bodega; además que serviría para detectar si hubo alguna merma entre lo ordenado al proveedor y lo almacenado en las bodegas.


Dado que la asociación ternaria anterior es del tipo muchos a muchos, es necesario descomponerla en asociaciones 1:M para poder llegar a un modelo relacional. También sucede lo mismo con la binaria que aparece en el modelo.
Es así como surgen las entidades de intersección Orden de Compra para la ternaria, la cual está formada por una clave primaria compuesta de Nro-Materia-Prima, Nro-Bodega y Nro-Proveedor; además del atributo cantidad que almacena cuántas unidades de cada materia prima ha sido solicitada a cada proveedor para que deje en cada bodega. Y la entidad de intersección Inventario para la binaria, la cual está formada por una clave primaria compuesta por Nro-Materia-Prima y Nro-Bodega; además del atributo cantidad que almacena cuántas unidades de cada materia prima hay en cada bodega. La sumatoria de todas las cantidades a ordenar de una materia prima debiera coincidir (o cuadrar) con la cantidad que hay en inventario.

### Dependencia existencial

Se refiere al hecho de que la ocurrencia de una entidad depende de la presencia de otra entidad. Se presenta cuando en una asociación existe una entidad dependiente de otra, que podría ser equivalente a una relación de “padres” a “hijos”, donde el “hijo” depende de la existencia de los padres. También se les denomina entidades fuertes y débiles.
Dentro de este contexto aparece el concepto de **integridad referencial**, que se ha convertido en una de las grandes fortalezas de las BD relacionales, pues permite asegurar la calidad de los datos frente a operaciones de inserción y de eliminación. Queda reflejado en las posibilidades que otorga el DBMS para enfrentar las siguientes situaciones:

* Una entidad no puede existir sin que previamente exista otra (ej.: una Factura no puede existir sin que el Cliente aún no existe). Esto se produce cuando se inserta una nueva entidad “hija” dentro de una asociación mandatoria hacia la entidad “padre” (ej: cuando se inserta una nueva Factura, el DBMS chequea que exista el Cliente).
* Una entidad no puede existir si el que le da sentido es eliminado (ej.: si un Cliente es borrado, deben eliminarse todas las Facturas asociadas a él). Esto se produce cuando se elimina una entidad “padre” dentro de una asociación mandatoria hacia la entidad “padre” (ej: cuando se elimina un Cliente, el DBMS da alternativas para que no quede una Factura referenciando a un Cliente que no existe).

* Beneficios:
    * Integridad referencial asegura calidad de los datos.
    * Acceso fácil a la entidad dependiente.
* En un modelo relacional está presente gracias a:
    * Claves foráneas
    * Claves primarias compuestas dentro de “tipos de entidades débiles” (o entidades de intersección, también conocidas como NUB)


Ahora vemos un ejemplo de dependencia existencial o de integridad referencial por clave foránea. Se muestra a nivel de tablas para una mayor claridad de lo que puede suceder con los datos.
En este ejemplo hay 2 tablas, la “fuerte” o (la “padre”) es la tabla Cliente y la “débil” (o la “hija”) es la tabla Factura, ya que una factura por ley no puede existir si no existe un cliente. 
Pensemos en que se desea insertar una nueva Factura que contiene los siguientes datos: “34567-1, 30-04-2020, 7.723.511-8“, y por otra parte, se desea eliminar al Cliente con RUT: 7.434.000-1. 


**Tabla Cliente**
|RUT      |NOMBRE      |
|---------|------------|
|7.434.000-1|Mario Aldunate|
|8.434.900-6|Cecilia Bustos|
|8.674.008-1|Felipe Miranda|
|9.484.567-9|Andrea Gómez|

**Tabla Factura**
|NRO-FACT |FECHA-FACT  |RUT-C      |
|---------|------------|-----------|
|25678-1  |12-12-2008  |7.434.000-1|
|28978-3  |15-12-2008  |8.434.900-6|
|29077-5  |15-12-2008  |7.434.000-1|
|29979-1  |22-12-2008  |9.484.567-9|

Dado los valores de nuestras tablas, no se podrá insertar la nueva factura Nro 34567-1, pues no existe el cliente con RUT 7.723.511-8. Y si se elimina al Cliente 7.434.000-1, se eliminarán las facturas 25678-1 y 29077-5. Pero el DBMS entrega algunas estrategias para dar mayor flexibilidad a las problemáticas que esto pueda generar.

En la siguiente figura se presenta un ejemplo de dependencia existencial o de integridad referencial por clave primaria compuesta. Se toma como ejemplo nuestro modelo de datos de Universidad, donde la entidad de intersección que se genera al descomponer la asociación M:N es una entidad débil (Notas), que tiene dos entidades fuertes hacia ella Alumno y Asignatura. Todo registro que se inserte en la entidad Notas, requiere que exista el alumno y la asignatura que cursó. Si se elimina un alumno o una asignatura, deben eliminarse todas las filas de la entidad Notas que hacen referencia a dicho alumno o asignatura.

![Integridad Referencial](imagenes/existencial.png "Integridad Referencial")