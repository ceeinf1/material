# INF239 - Bases de Datos - Definiciones en Bases de Datos

## Definiciones en Bases de Datos

Primero vamos a ver varias definiciones para asentar los conceptos más importantes que veremos en la asignatura.

### El Dato como un Recurso
En las organizaciones se ha reconocido la necesidad de incorporar al dato como un recurso más, lo que se conoce como un *first class citizen* o *ciudadano de primera clase* (así como tradicionalmente lo han sido los recursos humanos, financieros y materiales o tecnológicos); por lo cual, el dato debe ser planificado, administrado y controlado, y tratado como un activo más de la empresa, de tal manera de poder con él apoyar el logro de los objetivos organizacionales.
El dato, si bien tiene un rol diferente al resto de los recursos de una empresa, tiene con ellos una característica común importante: tiene un costo y un valor asociado. Siendo por ello de vital importancia un eficiente y efectivo tratamiento del recurso dato (o información). Su obtención (recuperación), almacenamiento y control, involucran gastos, pero si se considera el aporte que realiza por el valor agregado que entrega a una organización en apoyar sus decisiones en forma oportuna, ese gasto pasa a ser una inversión.

Un **dato** representa algo, hechos relacionados con personas, objetos, eventos u otras entidades del mundo real (empresa, sistema, etc.). Pueden ser cuantitativos (financieros) o cualitativos (descriptivos), internos o externos, históricos o predictivos. Los datos provienen de diversas fuentes dentro de una organización: Finanzas, Producción, Ventas, Personal, etc.

La **información** son datos que han sido organizados o preparados en una forma adecuada para apoyar la toma de decisiones. Por ejemplo, una lista de productos y su stock sin ningún orden son datos, pero una lista de productos ordenados por stock (de menor a mayor) representa información para el encargado de compras de un supermercado quién debe tomar decisiones del tipo: cuándo y en cuánto, reponer el stock de cada producto.

Una **bases de datos** es un conjunto de archivos de datos relacionados entre sí donde se almacenan datos relevantes para la organización y que posteriormente serán recuperados para transformarlos en información. 

![Relación entre elementos de una Base de datos](imagenes/arquitectura.png "Relación entre elementos de una Base de datos")

La figura muestra la relación entre estos 3 elementos, a través de una caja negra que tiene como entrada el dato, el cual es transformado por esta caja negra en información, gracias al apoyo que le entrega una base de datos. La caja negra es el computador, que para nuestro caso tiene dos componentes de software importantes: el DBMS y el programa de aplicación que desarrollemos para realizar una transformación del dato en la información que sea de real interés para el usuario. El usuario debe plantear sus requerimientos de información, y nosotros como diseñadores y desarrolladores debemos definir el contenido de la base de datos y las funcionalidades que el programa de aplicación debe ejecutar.


### Definición técnica de base de datos

En la figura de esta diapositiva la base de datos aparece como un gran repositorio donde hay un conjunto de archivos relacionados entre sí que pueden ser accedidos por numerosos usuarios, a través de distintos medios como, por ejemplo: programas de aplicación, directamente a través de una estación de trabajo o vía una aplicación móvil.

![Definición de una Base de datos](imagenes/definicion_BD.png "Definición de una Base de datos")

En el repositorio se distinguen 3 archivos: Bodega, Insumo y Proveedor. Para entender a qué nos referimos con archivos relacionados entre sí, pensemos la siguiente realidad organizacional: Una fábrica que almacena en las distintas Bodegas que posee a lo largo del país, diferentes Insumos que son suministrados por sus Proveedores. Los tipos de entidades de nuestra realidad son los sustantivos de esta frase: Bodega, Insumo y Proveedor; y las relaciones o asociaciones, son los verbos que enlazan a estas entidades. Una base de datos debe representar tanto a las entidades del mundo real como a sus asociaciones.

En esta figura, también quedan reflejadas dos propiedades importantes de las BD: integrar y compartir; la integración significa que los diferentes archivos de datos han sido lógicamente organizados para reducir la redundancia de datos y facilitar el acceso a ellos; el compartir significa que todos los usuarios calificados tienen acceso a los mismos datos, para usarlos en diferentes actividades.

### Definición organizacional de Bases de Datos

Desde una perspectiva organizacional, una BD se puede definir como un conjunto de datos operacionales relevantes para la toma de decisiones involucrada en algún nivel de la organización, y que van a permitir satisfacer diversos requerimientos de información (por datos operacionales se entiende a aquellos datos que usa la organización para su normal funcionamiento). 

![Definición Organizacional de una Base de datos](imagenes/definicion_organizacional.png "Definición Organizacional de una Base de datos")

Esta definición de BD queda representada en una figura donde se visualiza a la organización como una pirámide, que se puede dividir en 3 niveles horizontales: operacional, táctico y estratégico, donde cada función que se hace en la organización, por ejemplo: Producción, Finanzas, Personal y Marketing, es llevada a cabo por personas que, en cada uno de esos niveles para realizar su trabajo, necesita de información que extrae de la BD. 

Si pensamos en una multitienda, donde identificamos la función Ventas, podemos distinguir en el nivel operacional a las cajeras, en el nivel táctico al jefe del local y en el nivel estratégico a la gerenta general de la multitienda. Lo interesante que todos ellos se apoyan en la base de datos para realizar su trabajo, pero de distinta manera. La cajera es quien alimenta la BD al ir registrando (almacenando) los productos que compra el cliente; el jefe de local al final del día necesita información para detectar si las cajas cuadran o no con respecto al dinero recibido versus lo vendido; la gerenta general, al final del año necesita información para decidir si abrir una nueva sucursal o cerrar una existente.

Cuando diseñemos una BD necesitamos identificar a los usuarios de estos 3 niveles, que van a interactuar con la BD a través de algún programa de aplicación, de 4 formas diferentes: insertando datos (CREATE o INSERT), leyendo o seleccionado datos (READ o SELECT), actualizando datos que cambian en el tiempo (UPDATE) o borrando datos que quedan obsoletos (DELETE). Se utiliza el acrónimo CRUD, para representar que estas son las 4 operaciones básicas que se realizan sobre una base de datos, de tal manera de asegurar la calidad de la data en el tiempo. El lenguaje SQL usado por los DBMS relacional proveen de 4 comandos para esto: INSERT, SELECT, UPDATE y DELETE.