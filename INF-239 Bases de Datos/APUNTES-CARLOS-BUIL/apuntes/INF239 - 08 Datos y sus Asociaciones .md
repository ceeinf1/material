# UNIDAD 2: Modelos de datos conceptuales

El resultado de aprendizaje que se busca alcanzar con esta unidad es:
que los alumnos sean capaces de diseñar modelos de datos para diferentes realidades o sistemas utilizando notaciones estándar de representación (como UML, Bachman, E-R, E-R-E, entre otras).

Más en detalle, el temario de la unidad constará de los siguientes puntos:

1. Características de los Datos y sus Asociaciones (relaciones)
2. Tipos de Modelos de Datos
3. Semántica de los Datos
4. Notaciones para Modelos de Datos
5. Ejercicios de Modelos de Datos Conceptual


## Características del dato
Para describir qué es un dato, deben considerarse tres niveles de abstracción o estados en que se puede encontrar el dato:
* Realidad constituida por personas, objetos, lugares y eventos.
* Diccionario de Datos donde se almacenan los metadatos o definiciones sobre los datos.
* Base de Datos donde se almacenan los valores.
Estos niveles van desde lo más abstracto (realidad) o lo más alejado de nuestro objetivo final que es contar con una BD implementada, hasta lo menos abstracto que es la BD misma. 
En esta unidad profundizaremos en el dato en su estado dentro de la realidad a la que queremos diseñar y construirle una BD.

## Niveles de abstracción del dato
Estos niveles van desde lo más abstracto (realidad) o lo menos abstracto (valores en la BD). Desde el punto de vista de los datos, en la Realidad éstos se encuentran como tipos de entidades o clases que pueden representar personas, objetos, lugares y eventos del mundo real; luego pasan a un nivel menos abstracto cuando apoyados en el DBMS, se convierten en metadatos (definiciones acerca de los datos generadas con el DDL del DBMS) que quedan almacenados en el Diccionario de Datos; y finalmente, se transforman en la BD misma cuando se ingresan sus valores a nivel de ocurrencias de registros o filas dentro de una tabla.
Profundizando en el nivel de la realidad, debemos precisar que los tipos de entidades identificados tienen características que permiten describirlas, a los que se les llama atributos y participan de asociaciones lógicas con otras entidades que también son importantes de representar. 

Una precisión que haremos en cuanto al lenguaje usado es que para facilitar la comunicación hablaremos indistintamente de entidad o de tipo de entidad, siendo que en rigor a este nivel de manejan tipos de entidades que agrupan entidades que tienen las mismas características. 

### Niveles de abstracción del dato - Realidad
La realidad comprende el mundo real (sistema, organización), con sus componentes y el medio ambiente en el cual opera.
* Una Entidad es una persona, objeto (cosa, concepto), lugar y evento, sobre el cual la organización decide coleccionar y almacenar datos.
* Un Tipo (Clase) de Entidades es un conjunto de entidades que poseen características similares, por ejemplo, Clientes, Estudiantes, Pacientes.
* Un Atributo es una propiedad de una entidad que se desea registrar. Para cada tipo de entidades, existe un conjunto de atributos de interés para la organización.
* Cada entidad, dentro de una clase de entidades, debe tener al menos un atributo (o una combinación de atributos) que la distinga de otras entidades dentro de su clase, a lo cual se le denomina Clave Primaria (PRIMARY KEY).

### Niveles de abstracción del dato - Diccionario de Datos

Continuando con el análisis de los distintos niveles de abstracción en que podemos encontrar al dato, luego del nivel denominado Realidad donde lo encontramos como entidad con sus correspondientes atributos, pasamos a un nivel de abstracción menor, donde entra el juego el DBMS, en este nivel el dato está como metadato en el Diccionario de Datos.

En un Diccionario de Datos, el DBMS guarda información (metadatos) acerca de los ítems de datos de una organización.

Un **ítem de dato** es la unidad de datos más pequeña en una base de datos y, por lo tanto, la unidad de dato más pequeña con significado para el usuario; se refiere a lo que a nivel de la realidad a modelar llamamos **atributo**.

Por cada ítem de datos, el diccionario de datos guarda:

-   Nombre, largo, tipo, formato.
-   Breve descripción narrativa
-   Rango(s), Dominio, ¿Es valor único (Unicidad)?, ¿Es valor Nulo?
-   Posibles sinónimos
-   Fuente (origen)

Tomando como ejemplo, las entidades vistas al final de la clase anterior, para la realidad Universidad, los atributos para la entidad *ALUMNOS* serían: *RUT, NOMBRE, ROL, DIRECCION, REGION*. A nivel del diccionario de datos tendríamos para la tabla ALUMNOS, una descripción de metadatos como los siguientes:

  **COLUMN NAME**   **DATA TYPE**   **PK**   **NN**   **UQ**   **...**   **DEFAULT**   **COMMENT**
  ----------------- --------------- -------- -------- -------- --------- ------------- -------------
  RUT               VARCHAR (10)    √        √                                          
  NOMBRE            VARCHAR (40)             √        √                                 
  FECHANAC          DATE                     √                                          
  DIRECCION         VARCHAR (40)             √                                          
  REGION            INTEGER                                                            1..16
  CELULAR           INTEGER                                              NULL           


Además, en el Diccionario de Datos podría haber una descripción general del conjunto de itemes de datos a nivel de una descripción de un tipo de registro o clase de entidad. 

Un **Registro** es un conjunto de ítemes de datos y/o datos agregados, y
corresponde a la definición de una clase de entidades.

De un registro de datos, el diccionario de datos guarda:

-   Nombre, descripción, tamaño

-   Sus ítems de datos

-   Sus datos agregados

Por ejemplo, de la tabla ALUMNO, podría guardar que cada fila es de 120
bytes, que DIRECCION se desagrega en CALLE, NRO, CIUDAD.

### Niveles de abstracción del dato -- Base de Datos

De la Realidad (entidades y asociaciones) pasamos a un nivel de abstracción menor denominado Diccionario de Datos (metadatos) y de ahí llegamos al nivel de abstracción menor donde está la BD (valores).

La **Base de Datos** corresponde al nivel donde físicamente se guardan
los datos.

-   Existe un registro por cada objeto o entidad del mundo real.

-   Se almacenan ocurrencias de datos incluidas sus **relaciones o asociaciones**.

-   Conjuntos de archivos **asociados lógicamente** entre sí.

### Características de las Relaciones (o asociaciones)

En la realidad encontramos entidades (con sus atributos) y asociaciones
que permiten relacionar lógicamente a las entidades entre sí.

Una relación es una asociación entre tipos de entidades de una realidad.
Pueden ser de los siguientes tipos:

-   Una

-   Muchas

-   Condicional

-   **En Ambos Sentidos (las más comunes)**

-   Recursivas

-   Múltiples asociaciones entre entidades

Existen diversas notaciones para representarlas. Tenemos flechas,
líneas, símbolos especiales, todo depende de la simbología que usemos.
Lo más común es que la entidad se represente con un rectángulo, dentro
del cual se escriban los atributos, y las asociaciones a través de
líneas que indiquen hacia alguna dirección, parecidas a flechas,
mostrando la entidad origen y la entidad destino de la asociación.

### Ejemplos de asociaciones

**Asociación de tipo UNA**: en un Hospital encontramos Pacientes que
están asignados a Camas. Un paciente ocupa UNA cama. Si hay que tener en
cuenta, que en esta realidad nuestros pacientes no podrían por ejemplo
cambiarse de camas durante su estadía en el Hospital, o si llegaran a
cambiarse, sólo nos quedaríamos con los datos de la última cama que
utilizó; además, todos los pacientes quedan asignados a una cama, por lo
que en este Hospital no podría haber pacientes ambulatorios.

**Asociación de tipo MUCHAS**: en una Empresa encontramos Empleados que
tienen Cargas Familiares. Un Empleado puede tener MUCHAS Cargas
Familiares. Si hay que tener en cuenta que en esta realidad estamos
representando que todos los empleados tienen cargas familiares, lo cual
es una restricción muy fuerte.

Como podemos ver ambas asociaciones tienen problemas, son muy
restrictivas, no dejan representar bien la realidad a modelar, sigamos
avanzando para encontrar asociaciones más inclusivas.

**Asociación de tipo CONDICIONAL:** se representa con un cero sobre la
línea, veamos dos ejemplos:

-   En un Hospital encontramos Pacientes que pueden están asignados a
    Camas (pacientes hospitalizados) o no necesitan ser asignados a
    Camas (pacientes ambulatorios).

-   En un Empresa encontramos Empleados que pueden tener cero, una o más
    Cargas Familiares.

**Asociaciones en ambos sentidos:** estas son las asociaciones más
comunes, las más inclusivas, pues permiten representar de mejor forma
una realidad. Son con las cuales trabajaremos siempre. No olvidar
"revisar siempre ambos sentidos". Hay tres tipos de estas asociaciones:

-   1:1 (UNO a UNO)

-   1:M (UNO a MUCHOS)

-   M:N (MUCHOS a MUCHOS)

Una **asociación 1:1**, la explicaremos con el caso del Hospital: Un
Paciente ocupa una o ninguna Cama (se muestra en la figura con la flecha
que sale de Paciente hacia Cama), pero además podemos decir que una Cama
tiene un solo Paciente o está desocupada (se muestra en la figura con la
flecha que sale de Cama a Paciente). Los ceros en ambos lados de la
asociación representan que puede haber pacientes ambulatorios (cero al
lado derecho) y camas desocupadas (cero al lado izquierdo).

Una **asociación 1:M**, la explicaremos con el caso de la Empresa: Un
Empleado tiene cero, una o muchas Cargas Familiares (se muestra en la
figura con la flecha que sale de Empleado hacia Carga), pero además
podemos decir que una Carga tiene un solo Empleado como responsable de
ella, incluso esto por Ley de la República es así (se muestra en la
figura con la flecha que sale de Carga a Empleado). El cero en el lado
de Cargas representa que puede haber Empleados que no tienen cargas
familiares; por otro lado, si miramos de Carga a Empleado, es importante
destacar, que no es posible poner un cero porque si existe una Carga,
debe haber un Empleado responsable de ella (esto es una asociación
mandatoria, es decir, la asociación siempre debe existir). Las
asociaciones 1:M son las más comunes de encontrar en cualquier realidad.

Una **asociación N:M**, la explicaremos con el caso de una Universidad:
Un Alumno puede tomar cero, una o más Asignaturas en un semestre (se
muestra en la figura con flecha que sale de Alumno hacia Asignatura),
pero además sabemos que una Asignatura puede ser cursada por cero, uno o
más Alumnos (se muestra en la figura con flecha de Asignatura hacia
Alumno). Estas asociaciones son las más complejas, no pueden
transformarse directamente a un modelo lógico, requieren de un paso
intermedio para reducir la complejidad antes de llegar a las tablas del
modelo relacional.

Para entender mejor estos tres tipos de asociaciones, analicemos cómo
estos modelos de datos conceptual quedarían posteriormente implementados
en una tabla. Se usa la notación de subrayar con línea continua clave
primaria y línea punteada clave foránea.

**Tablas para asociación 1:1**

  **PACIENTE**             **CAMA**                                                   
  -------------- --------- ----------- --------- -- ---------------- ---------------- -------
  [RUT]{.ul}     NOMBRE    DIRECCION   ID-CAMA      [ID-CAMA]{.ul}   TIPO-CAMA        RUT-P
  111-1          José      Viña        NULL         1                Normal           222-2
  222-2          Isabel    Santiago    1            2                Normal           NULL
  333-3          Simón     Quilpué     5            3                UCI              444-4
  444-4          Alberto   Viña        3            4                Traumatológica   NULL
  555-5          Jorge     Santiago    7            5                Normal           333-3
                                                    6                UCI              NULL
                                                    7                Traumatológica   555-5
                                                    8                Normal           NULL

La asociación 1:1 entre Paciente y Cama, queda representada por las
claves foránea que se agregan en tablas Cama y Paciente. El hecho que
hay pacientes ambulatorios queda representado porque en la clave foránea
RUT-P, no existe el valor del RUT del Paciente que es ambulatorio;
además en este caso, es factible detectar rápidamente si el paciente es
ambulatorio, con el solo hecho de analizar si en la columna clave
foránea ID-CAMA hay un valor NULL. Si hay camas disponibles basta
revisar si la columna foránea RUT-P tiene el valor NULL.

Es importante al crear las tablas en SQL, definir las claves primarias y
foráneas, e indicar las características de valor NULL en ambas claves
foráneas y que no permitan duplicados (UNIQUE). Recordar, además, que la
clave primaria por definición no puede tener valores NULL.

**Tablas para asociación 1:M**

  EMPLEADO               CARGAS                                                 
  ------------ --------- ----------- -- -------------- ----------- ------------ -------
  [RUT]{.ul}   NOMBRE    DIRECCION      [RUT-C]{.ul}   NOMBRE-C    F-NACIM      RUT-E
  111-1        José      Viña           91111-9        Pepita      10/12/2010   111-1
  222-2        Isabel    Santiago       92222-2        Pepito      15/04/2012   111-1
  333-3        Simón     Quilpué        94323-2        Gustavo     25/08/2018   111-1
  444-4        Alberto   Viña           93452-5        Isabelita   18/09/2012   222-2
  555-5        Jorge     Santiago       94569-1        Luciano     12/02/2019   222-2
                                        98765-0        Jorguito    12/05/2015   555-5
                                        96734-9        María       12/02/2020   555-5

La asociación 1:M entre Empleado y Carga familiar, queda representada
por la clave foránea que se agregan en tabla Cargas. El hecho que hay
empleados sin carga familiar queda reflejado en que hay RUT de empleados
que no están en los valores de la columna RUT-E de tabla Cargas, por
ejemplo, Simón y Jorge no tienen cargas familiares, para lo cual es
necesario hacer un JOIN entre tablas a través del atributo RUT. ¿Cómo
podría reducirse los tiempos de respuesta para determinar si un empleado
tiene o no cargas familiares? Agregando un atributo Cantidad-de-Cargas
es tabla Empleado, sería un atributo que podría ser derivable, pero es
preferible generar la redundancia en pro de mejores tiempos de
respuesta.

Es importante al crear las tablas con SQL, definir las claves primarias
de ambas tablas y la foránea en tabla Cargas la que en este caso no debe
permitir el valor NULL (pues por Ley de la República toda carga familiar
debe tener una persona que se haga responsable de ella) y si puede
permitir duplicados (NOT UNIQUE, que se asume por defecto). Recordar,
además, que la clave primaria por definición no puede tener valores
NULL.

Es importante mencionar las diferentes simbologías o notaciones que tenemos para
graficar nuestros modelos de datos. Aquí hemos visto la creada por
Charles Bachmann en los años 80, hoy en día ya no se utiliza. Dentro de
las más utilizada actualmente está la simbología Entidad-Relacionamiento
de Peter Chen y los Diagramas de Clases de UML.


**Tablas para asociación M:N**

Estas asociaciones por su complejidad no pueden implementarse
directamente en una base de datos relacional, requieren ser primero
descompuestas en asociaciones 1:M y generar entre ellas una tabla de
intersección (o también conocida como tabla NUB) que almacene atributos
que forman parte de la asociación misma. Por ejemplo, en el caso de la
Universidad, para guardar la Nota que un Alumno ha obtenido en una
Asignatura se requiere crear esta entidad de intersección, que estaría
compuesta de la clave primaria de Alumno + clave primaria de Asignatura.

Tendríamos entonces las siguientes tablas:

  ALUMNOS                ASIGNATURAS                                                 
  ------------ --------- ------------- -- -------------- ---------------- ---------- -------------
  [RUT]{.ul}   NOMBRE    DIRECCION        [SIGLA]{.ul}   NOMBRE-A         CREDITOS   AÑOCREACION
  111-1        José      Viña             INF239         Bases de Datos   3          2014
  222-2        Isabel    Santiago         IWI131         Programación     3          2013
  333-3        Simón     Quilpué          INF325         BD Avanzada      3          2019
  444-4        Alberto   Viña             MAT021         Mate 1           5          2000
  555-5        Jorge     Santiago         MAT022         Mate 2           5          2000
                                          INF134         Est. de Datos    3          2014
                                          INF270         OSI              3          2016

  TABLA INTERSECCION ALUMNOS-ASIGNATURAS                            
  ---------------------------------------- -------- --------- ----- ------
  RUT                                      SIGLA    SEM-AÑO   VTR   NOTA
  111-1                                    INF239   1/2019    1     80
  111-1                                    MAT022   1/2020    1     NULL
  111-1                                    INF270   1/2020    1     NULL
  333-3                                    INF239   1/2020    1     NULL
  333-3                                    INF270   1/2020    1     NULL
  555-5                                    MAT022   1/2019    1     50
  555-5                                    MAT022   1/2020    2     NULL

¿Cuál sería la clave primaria en la tabla de intersección? Es una clave
primaria compuesta de RUT, SIGLA, SEM-AÑO. También podría ser RUT,
SIGLA, VTR. Ambas no permiten filas duplicadas en esta tabla, así es que
cualquiera de ellas es una buena clave primaria.

Es importante tener en cuenta que la mayoría de las veces en este tipo
de tablas de intersección la clave primaria es compuesta de las PK de
cada una de las entidades que participan en la asociación, pero a veces
además necesitan de algún atributo adicional para lograr la unicidad.
Por ejemplo, en este caso si solo hubiese sido PK la combinación RUT y
SIGLA, no se hubiera podido guardar la historia de las notas de cada
alumno en una asignatura, solo se guardaría la nota de la última vez que
fue cursada (se "pisa" o sobre escribe el registro con la nueva nota).

### Ejemplos de asociaciones recursivas o loop (1:1 y 1:M)

Una asociación recursiva es la que puede tener una entidad consigo
misma. Esta asociación puede ser 1:1, 1:M o M:N.

Para el caso de 1:1. por ejemplo, pensemos que tenemos una entidad con
los datos de Personas y queremos dejar reflejado que una persona está
"casada-con" otra cuyos datos forman también parte de esta entidad, esto
se grafica con un loop donde la asociación puede ser condicional en
ambos sentidos, ya que en la realidad existen personas solteras. En una
tabla de una BD relacional esto quedaría como sigue:

  PERSONAS                          
  ------------ -------- ----------- ------------
  [RUT]{.ul}   NOMBRE   DIRECCION   CASADO-CON
  111-1        José     Viña        444-4
  222-2        Isabel   Santiago    555-5
  333-3        Simón    Quilpué     NULL
  444-4        Angela   Viña        111-1
  555-5        Jorge    Santiago    222-2
                                    

Es necesario agregar una clave foránea a la tabla Persona que permita
guardar los RUT de quién está casado con quién. Es decir, una columna
CASADO-CON donde los posibles valores que allí puede haber son todos los
RUT que están en la primera columna de la tabla. La columna CASADO-CON
debe ser definida como foránea, sin duplicados (UNIQUE) y que permita
los valores NULL (para que pueda reflejarse que hay personas solteras).

Para el caso de recursiva de 1:M, por ejemplo, pensemos que tenemos una
entidad con los datos de los Empleados y queremos dejar reflejado quién
es el "jefe-de" cada empleado, teniendo en cuenta que una persona puede
ser "jefe-de" cero, uno o más empleados y que todo empleado tiene un
jefe directo, salvo la dueña de la empresa , esto se grafica con un loop
donde la asociación puede ser condicional en un solo sentido (para
representar que el dueño no tiene jefe), y que cualquier otro empleado
tiene un solo jefe.

  EMPLEADOS                         
  ------------ -------- ----------- ------------
  [RUT]{.ul}   NOMBRE   DIRECCION   SU-JEFE-ES
  111-1        José     Viña        444-4
  222-2        Isabel   Santiago    555-5
  333-3        Simón    Quilpué     444-4
  444-4        Angela   Viña        NULL
  555-5        Jorge    Santiago    444-4
                                    

Es importante al crear las tablas con SQL, definir la clave primaria y
la foránea la que en este caso debe permitir el valor NULL y si puede
permitir duplicados (NOT UNIQUE, que se asume por defecto). Recordar,
además, que la clave primaria por definición no puede tener valores
NULL.

### Ejemplos de asociaciones recursivas o loop (M:N)

Para el caso de recursiva de M:N, por ejemplo, pensemos que tenemos una
entidad con los datos de los Productos que se venden en una Fábrica.
Dentro de estos productos hay algunos que están compuestos de otros, es
decir, se venden productos completos o también se venden las piezas o
componentes de esos productos a parte. Esto se grafica con un loop sobre
la entidad Producto que indica que un producto puede estar formado por
cero, uno o más piezas (que también son productos); y que cualquiera de
esas piezas, puede formar parte de cero, uno o más productos.

Por ser una M:N para poder implementarla en una BD relacional, es
necesario descomponerla, creando una entidad de intersección que
contendrá como PK una clave primaria compuesta. Así se llega a dos
tablas:

  PRODUCTO                    COMPONENTES                                      
  --------------- ----------- ------------- -- --------------- --------------- ------------
  [\#PROD]{.ul}   NOMBRE      ETC.             [\#PROD]{.ul}   [\#COMP]{.ul}   CANT-USADA
  111-1           Puerta      ....             111-1           222-2           1
  222-2           Chapa       ....             111-1           333-3           500
  333-3           Tornillos   ....             111-1           444-4           2
  444-4           Manilla     ....             111-1           555-5           1
  555-5           Vidrios     ....             222-2           333-3           10
                                               444-4           333-3           5
                                               ....            ....            ....

Es importante al crear las tablas con SQL, definir la clave primaria en
ambas tablas, teniendo en cuenta que en la tabla Componentes, la PK es
compuesta de \#PROD y \#COMP y que, además, cada una de estas columnas
en forma separada son foráneas hacia Producto. Todas estas claves no
pueden tener valores NULL.

### Ejemplos de múltiples asociaciones entre entidades

Este tipo de asociaciones busca simplificar los modelos de datos,
reduciendo la cantidad de entidades, para lo cual, dentro de lo posible,
agrupa entidades de un mismo tipo.

Por ejemplo, si se piensa en una Compañía de Seguros que vende pólizas
de seguro de vida, donde cada póliza tiene a una persona que asegura su
vida y a otra persona que será su beneficiario, en caso de que la
primera fallezca.

Una alternativa para modelar esta situación es definir una entidad para
el Asegurado y otra para el Beneficiario, ambas con los mismos
atributos. Pero otra alternativa, que disminuye el número de entidades,
es tener una sola entidad Persona donde se agrupen tanto a quienes son
asegurados como beneficiarios, debiendo agregar un atributo que refleje
el rol de cada uno, e identificando en la asociación explícitamente ese
rol, generándose dos asociaciones entre las mismas entidades.

Para la primera alternativa se necesitan 3 tablas: ASEGURADO,
BENEFICIARIO y POLIZA, donde cada una debe tener su PK y en POLIZA se
deben definir, además, 2 claves foráneas, una hacia Beneficiario y otra
hacia Asegurado.

Para la segunda alternativa se necesitan 2 tablas: PERSONA y POLIZA,
donde cada una debe tener su propia PK y en PERSONA se debe agregar un
atributo que indique el tipo de persona, y en POLIZA, 2 claves foráneas
ambas hacia Persona, pero una de ellas hacia quién cumple el rol de
Asegurado y la otra hacia quien cumple el rol de Beneficiario.

### Ejemplo modelando nuestra sociedad

-   Se pide identificar entidades y asociaciones más relevantes

-   Aprendizajes buscados:

    -   Saber identificar entidades y asociaciones de todos los tipos

    -   Importancia del Cliente/Usuario

    -   Definición del horizonte de tiempo

    -   Importancia del nivel de abstracción del modelo de datos
