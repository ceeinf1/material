# INF239 - Bases de Datos -  Proceso de diseño de BD

La BD es un elemento esencial dentro de los software que desarrollemos. Existen diferentes metodologías para el desarrollo de software, las cuales tienen etapas muy claras donde se identifica el cómo y cuándo se debe profundizar en el diseño de la BD. 
Las etapas principales de estas metodologías se pueden sintetizar en el esquema que aparece en la siguiente figura, donde se identifican 2 enfoques, uno orientado a los procesos (lado izquierdo del esquema) y otro a los datos (lado derecho del esquema). 

![Etapas de vida de la Base de datos](imagenes/etapas_bd.png "Etapas de vida de la Base de datos")

Este semestre profundizaremos en el enfoque orientado a los datos, donde se identifican las siguientes etapas que debemos seguir para diseñar una BD:
1.	Recolección y Análisis de Requisitos, permite obtener los requisitos o requerimientos que se le piden a la BD, es decir, qué información debemos poder extraer de ella (requerimientos de información). 
2.	Diseño Conceptual, permite obtener un modelo de datos conceptual que satisfaga los requisitos identificados en etapa anterior (equivale a la maqueta de un arquitecto).
3.	Elección del Software o DBMS que se utilizará.
4.	Diseño Lógico, permite traducir el modelo de datos conceptual en algo entendible por el DBMS, es decir, en un modelo lógico (equivale al plano de un arquitecto).
5.	Diseño Físico, permite agregar características físicas al modelo que optimicen el funcionamiento de la BD.
6.	Implementación de la BD o creación de la BD 

A continuación, se detallará cada una de estas etapas.

1. Recolección y Análisis de Requisitos
 * Objetivo: identificar las necesidades de información de los usuarios (requisitos, requerimientos o vistas). 
 * Pasos:
    * Identificación de las áreas de aplicación y grupos de usuarios. Elección de los actores o participantes principales.
    * Análisis y estudio de la documentación existente en el sistema actual, incluye aplicaciones, manuales de políticas, reportes, diagramas organizacionales, etc.
    * Estudio del actual ambiente operativo y uso de la información. Incluye un análisis de los tipos de transacciones y sus frecuencias, y del flujo de información en el sistema.
    * Respuestas de cuestionarios obtenidas desde los potenciales usuarios, permiten identificar prioridades.
    * Formalización de requisitos

2. Diseño Conceptual
 * Objetivo: construir un esquema conceptual que represente los datos necesarios para el sistema de información, que sea independiente del motor de datos a utilizar.
 * El modelo conceptual sirve como:
   * Medio de comunicación entre usuarios y especialistas; por ende, debe ser expresivo, simple, mínimo, formal, diagramático.
   * Mecanismo para validar entendimiento alcanzado del problema, por parte del especialista.
   * Descripción estable del contenido.
Se representa a través de notaciones conocidas internacionalmente, por ejemplo, cajas o rectángulos para identificar a las entidades y líneas para representar a las asociaciones.

3. Elección del Software
 * Objetivo: seleccionar aquel tipo de software (DBMS) que mejor se adecúe a las necesidades del sistema a construir.
 * Criterios a considerar:
   * Costos: adquisición de hardware y software; operación y mantención; migración,...
   * Requisitos del sistema: funcionales y no funcionales.
   * Estructuración de los datos
Existen diversos tipos de DBMS, como por ejemplo Oracle, Teradata, PostgreSQL, MySQL, DB2, SYBASE, Microsoft SQL Server, MongoDB, LUCID DB, MariaDB.

4. Diseño Lógico
 * Objetivo: generar un esquema basado en el modelo de datos soportado por el software escogido.
 * Pasos:
    * Transformación independiente del sistema a un modelo relacional, orientado al objeto u otro.
    * Conversión de los esquemas a un software de bases de datos específico.

![Modelos conceptuales y modelos lógicos](imagenes/modelos_conceptuales_logicos.png "Modelos conceptuales y modelos lógicos")

2. Diseño Lógico (ejemplo)
Consideremos la siguiente situación a modelar. Tenemos una fábrica con muchos clientes a quienes, a través de facturas, les vende sus productos.
En el modelo de datos conceptual podemos identificar las siguientes entidades con sus atributos:
 * Cliente (RUT, Razón Social, Dirección Legal)
 * Factura (NroFactura, Fecha)
 * Producto (CódigoProducto, Nombre, Precio)
Y las asociaciones:
 * Tiene: un Cliente tiene una o más Facturas, una Factura tiene un solo Cliente (1:M)
 * Considera: una Factura considera uno o más Productos, un Producto considera una o más Facturas (M:N)   
En el modelo de datos lógico, se produce la transformación del modelo de datos conceptual para que pueda ser entendible por el DBMS. Aquí aparecen en el caso de haber seleccionado un DBMS relacional, las tablas con sus claves primarias y foráneas, pues cada entidad del modelo conceptual es una tabla en el modelo lógico, y cada asociación es una clave foránea, que permite relacionarse con otra tabla, o es una nueva tabla que permite descomponer asociaciones complejas como las M:N. Aquí podríamos decir que un modelo de datos lógico es un modelo de datos relacional, si es que cuento con un DBMS de ese tipo. 

### Etapas para Diseño de BD – 5. Diseño Físico
**Objetivo**: escoger las estructuras de almacenamiento y métodos de acceso además de la ubicación de los archivos de bases de datos, para obtener un buen rendimiento de las distintas aplicaciones que interactúan con la base de datos.

**Criterios a considerar**:
  * Tiempo de Respuesta: es el tiempo que transcurre desde el ingreso de la transacción hasta el recibo de su respuesta.
  * Rendimiento del Sistema: número promedio de transacciones que pueden ser procesadas por minuto.
  * Utilización del espacio en disco: cantidad de memoria ocupada por los archivos e índices.


En esta etapa se realizan ajustes o mejoras para lograr un rendimiento óptimo de la BD. Las herramientas más potentes que proveen los DBMS para optimizar el almacenamiento y la recuperación de los datos, son:
 * Estructuras de almacenamiento:
    * Secuenciales: desordenados, ordenados
    * Directo: hashing estático, o con expansión dinámica
    * De tipo árbol: Árbol B.
 * Índices
    * Dinámicos: hashing con expansión dinámica, de tipo Árbol B o B+
    * Bitmap


**¿Cómo podemos optimizar el rendimiento de una BD?**

Pensemos que tenemos una tabla, en el almacenamiento en memoria secundaria se distinguen 2 áreas: Índices y Datos. El almacenamiento en el área de datos se realiza en bloques, en nuestro ejemplo el tamaño del bloque es 3 (b=3), esto significa que en un bloque se almacenan 3 filas de la tabla. 

Si suponemos que cada fila tiene como clave primaria un campo con las letras del alfabeto, y usamos un índice de tipo árbol para manejar la clave primaria, se genera en el área de índice una estructura de árbol donde cada nodo tiene valores de claves primarias ordenados alfabéticamente. Estos nodos tienen un puntero hacia niveles más bajos del índice, y el último nivel apunta al área de datos.

Por ejemplo, si deseo buscar una fila de la tabla cuyo valor de la clave primaria es “j”, la búsqueda parte en la raíz del árbol, comparando el valor de la clave buscada con lo que hay en la raíz (en este caso hay una “m”, entonces la pregunta es ¿ ”j” es mayor que “m”?, como no lo es, toma el puntero del lado izquierdo, y baja al siguiente nivel del árbol que contiene 3 valores de claves: “c”, “f”, “i”. Se compara con cada uno de esos valores, detectándose que ”j” es mayor que “i”, por lo que ahora baja al siguiente nivel tomando el puntero del lado derecho y llegando al nivel del árbol donde se encuentra con el valor “j” y un puntero al bloque del área de datos donde está la fila buscada, dentro del bloque se recorre secuencialmente.

Si no se hubiese definido un índice, la búsqueda habría sido secuencia sobre el área de datos, lo cual es muy lento si la tabla tiene millones de registros. El índice permite mejorar los tiempos de respuesta de las transacciones.
Un índice es como esas agendas antiguas donde uno registraba los números de teléfono, donde cada hoja tenia una letra del abecedario, y si uno buscaba un contacto cuyo apellido parte con “M”, no necesitaba recorrer las hojas previas, se va directo a la página con “M”, dentro de esa página si se hace un recorrido secuencial.

### Etapas para Diseño de BD – 6. Implementación de la BD

 * Objetivo: codificación de sentencias para la definición y la manipulación de la base de datos, para crear los archivos y su poblamiento.
CREATE TABLE Alumno
	( RUT         char(9)   primary key,
	Nombre    varchar(35) not null,
	Direccion   varchar(50)
	Carrera varchar (3));

SELECT RUT, Nombre FROM Alumno;
SELECT * FROM Alumno WHERE Carrera = ‘INF
