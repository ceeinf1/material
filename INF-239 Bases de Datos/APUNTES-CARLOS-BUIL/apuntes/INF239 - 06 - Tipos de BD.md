# INF239 - Bases de Datos - Tipos de Bases de Datos

## Tipos de BD, según nivel organizacional que apoyan

Dependiendo de a quién apoya la BD dentro de una organización se pueden clasificar éstas en los siguientes tipos: 
 * BD Operacional (Transaccional - OLTP): apoya a los niveles operacionales principalmente y en un grado menor también al nivel táctico. Estas BD registran las transacciones del día a día de estas organizaciones y son el soporte de los sistemas de tipo OLTP. Actualmente suelen ser BD relacionales.
 * BD de Gestión (Data Warehouse, Data Mart - OLAP): apoya principalmente a los niveles táctico y estratégico. Estas BD permiten entregar información y conocimiento a los tomadores de decisiones de las organizaciones y son el soporte de los sistemas de tipo OLAP. Actualmente suelen ser BD relacionales o multidimensionales, que permiten implementar un Data Warehouse (DW) o Data Marts (pequeños DW creados para un fin específico: Ventas, Producción, etc.). Las aplicaciones sobre estas BDs, trabajan con un enfoque descriptivo.
 * BD Estratégica (Data Warehouse – OLAP, Data Mining): apoya principalmente al nivel estratégico. Estas BD permiten entregar conocimiento a los tomadores de decisiones de las organizaciones y son el soporte de los sistemas de tipo OLAP y de los algoritmos de Data Mining (Minería de Datos). Actualmente suelen ser BD relacionales, multidimensionales o archivos planos. Las aplicaciones sobre estas BD, trabajan con un enfoque descriptivo y predictivo.
Cabe precisar el concepto de BD multidimensional que queda reflejado generalmente a través de un cubo donde se identifican tablas de dimensiones y tablas de hechos. Por ejemplo, dimensiones serían los Trimestres (tiempo), Sucursales (geográfica) y Categorías de Productos (objeto) y los hechos las Ventas Totales realizadas por trimestre, por sucursal y por categoría de producto. Se trabaja con valores agregados, no con la transacción misma, esa queda almacenada en la BD operacional. Se utilizan comandos SQL del tipo DRILL-DOWN y DRILL-UP.

## Tipos de BD, según tipo de dato almacenado

Dependiendo de a qué tipos de datos se requiere almacenar en la BD se define la siguiente clasificación: 
* Para datos estructurados y precisos, se tienen las BD relacional. Los valores asociados a las columnas de una tabla son todos de un mismo tipo, y las filas de una tabla son todas de un mismo largo (todas tienen asociadas las mismas columnas).
  ![Ejemplo de relación](imagenes/tabla.png "Ejemplo de relación")
* Para datos agregados, se tienen las BD multidimensional. Los valores que se almacenan suelen resumir las transacciones del negocio, no se trabaja con el nivel de detalle de las BD transaccionales, se juntan varios registros de ellas para obtener el valor agregado que queda en la BD multidimensional. Por ejemplo, no se almacena la factura (esa está en la BD transaccional), si se almacena la suma de todas las ventas asociadas a un determinado periodo de tiempo para una determinada sucursal y para determinada categoría de producto. 
* Para datos semiestructurados como datos espaciales, XML, o de texto, se tienen las BD de documentos. Es un tipo de BD no relacional diseñada para almacenar y consultar datos como documentos del tipo JSON. Por ejemplo, en sistemas de información geográficos (SIG), sistemas documentales para las bibliotecas, etc.
* Para datos no estructurados, se tienen las BD NoSQL (para algunos también NOSQL por Not Only SQL para recalcar que también pueden soportar lenguajes de tipo SQL). Los datos almacenados no requieren de estructuras de datos rígidas como tablas. Estas BD nacen asociadas al crecimiento de las redes sociales donde se generan grandes volúmenes de datos (BigData). Usan distintas formas de almacenar los datos (contenedores, grafos, etc.), algunas funcionan almacenando la información en memoria principal, de manera de aumentar la velocidad de los tiempos de respuesta. Algunos DBMS que trabajan de esta forma son mongoDB, Redis, Cassandra, CouchDB.

![Espectro de datos](imagenes/espectro.png "Espectro de datos")

En la figura del espectro de datos se muestran los distintos tipos de datos según la estructrura que tiene el modelo de datos que los almacenan. El modelo de datos más estructurado (y a la vez más rígido) es el relacional, basado en tablas y relaciones entre ellas donde todo está bien definido. Seguiría le modelo de datos de grafos, que no es tán rígido pero sigue teniendo una estructura definida. Una de las principales diferencias es que es posible añadir una relación nueva a un dato concreto sin romper el esquema (porque el esquema es flexible). Si seguimos escalando llegamos a árboles, que es más flexible que grafos pero tiene redundancia de datos, como podemos ver en la figura. Finalmente el modelo más flexible de todos es el texto plano, donde podemos almacenar cualquier dato fácilmente pero es muy difícil consultar porque no hay estructura.

<!-- Para datos estructurados se muestra una tabla con los datos de Personas. Cada Persona se detalla a través de una fila, con valores para cada una de sus columnas. Hay 4 filas (4 Personas) y 5 columnas (RUT, NOMBRE, ESTADO CIVIL, OCUPACION, SEXO).
Para datos agregados se muestra una tabla multidimensional, donde hay 4 dimensiones: Tiempo, Sexo, Estado Civil y Productos. Y los hechos que se registran son las ventas promedios por Producto, por Estado Civil, por Sexo, y por Año. -->


### Tipos de BD, según ubicación copia principal de los datos

Es importante tener en cuenta que el almacenamiento en un computador se da a nivel de memoria principal (RAM) y memoria secundaria (discos, almacenamiento en la nube). Es equivalente a lo que sucede en nuestro cerebro donde tenemos una memoria a corto plazo y una a largo plazo. Es así como la RAM procesa y recuerda los datos mientras está activa, cuando se desactiva transfiere los datos a la memoria secundaria. Nuestro cerebro cuando dormimos convierte la memoria a corto plazo en recuerdos a largo plazo.

Las BD en forma natural fueron creadas pensando en un almacenamiento secundario (permanente), pero cuando se ejecuta algún programa de aplicación que requiere de sus datos, éstos son transferidos a la memoria principal (almacenamiento temporal) hasta que se dejen de usar. En caso de ser cambiados, vuelven a ser transferidos al almacenamiento secundario. Esto genere muchas entradas y salidas entre ambos dispositivos. Además, por motivos de seguridad existen copias offline de la BD (por ejemplo, un disco duro externo, un pendrive, etc.).
Desde esta perspectiva se distinguen 3 niveles donde puede estar almacenada la BD
 * Basada en memoria principal (in-memory database) (1 nivel). VoltDB es un RDBMS (NewSQL) que trabaja de esta forma, almacenando y procesando sus datos en memoria principal. También Oracle tiene una versión de su DBMS para trabajar de esta forma, y Apache Spark. A este nivel se potencia el uso de memoria caché.
 * Basada en el disco (2 niveles): nivel 1 es la memoria principal y nivel 2 es el disco o algún dispositivo de almacenamiento masivo de datos. Los RDBMS trabajan de esta forma.
 * Basada en almacenamiento terciario (3 niveles): nivel 1 es la memoria principal, nivel 2 es el disco o algún dispositivo de almacenamiento masivo de datos, y nivel 3 es algún medio offline de almacenamiento.

### Tipos de BD, según número de procesadores
Otra característica importante de un computador que influye en nuestras BD, es el número de procesadores con que se cuenta. Dependiendo de esto, es si estamos frente a una BD que es procesada en forma serial o paralela.
* Serial (secuencial)
* Paralela:
  * Memoria Compartida (MC)
  * Nada Compartido (NC)
  * Disco Compartido (DC)
  * Arquitectura Híbrida (AC: Algo Compartido)
  
ApacheSpark, framework que permite la programación de clusters con paralelismo de datos implícitos. También provee de herramientas como Spark SQL (para el procesamiento de datos estructurados basada en SQL), MLlib para implementar machine learning, GraphX para el procesamiento de grafos y Spark Streaming.

### Tipos de BD, según número de procesadores-serial

La alternativa de una BD serial, es la más tradicional, se basa en un computador con un procesador, su memoria principal y un conjunto de almacenamiento secundario (discos, por ejemplo) donde iría almacenada la BD. Cada vez que se requiere un dato de la BD, se produce un traslado del dato hacia la memoria principal (se trasladan en bloques) y ahí se comienza la ejecución del programa que requiere de este dato. Si el dato es actualizado por el programa, se genera una operación de escritura desde la memoria principal hacia la BD para que pueda el dato persistir más allá de la ejecución misma del programa.

### Tipos de BD, según número de procesadores-paralela con memoria compartida o nada compartido

El paralelismo en BD se usa para poder procesar en forma paralela en distintos discos y distintos procesadores una sola operación sobre la BD. Es equivalente a particionar la BD en fragmentos o particiones y distribuirlos en la arquitectura paralela para mejorar la velocidad de las consultas. 

![Bases de datos paralelas](imagenes/bd_paralelas.png "Bases de datos paralelas")

En esta imagen se muestra la alternativa de una BD en un computador con paralelismo o múltiples procesadores trabajando en paralelo.
En este tipo de ambientes se distinguen 4 tipos de uso de las BD:
1.	Memoria compartida: se tienen varios procesadores, una sola memoria principal y nuestra BD como memoria secundaria, es decir, se comparte entre los diferentes procesadores las memorias (tanto la principal como la secundaria).
2.	Nada compartido: se tienen varios procesadores, donde cada uno de ellos tiene su propia memoria principal y memoria secundaria (BD), es decir, nada (ninguna memoria) se comparte entre los diferentes procesadores.
3.	Disco Compartido: se tienen varios procesadores, donde cada uno de ellos tiene su propia memoria principal y comparten la memoria secundaria (BD).
4.	Arquitecturas Híbridas: es una mezcla de las 3 alternativas anteriores, con todas las combinaciones posibles. Una de las combinaciones más usada es la de clusters, donde cada cluster tiene por ejemplo el esquema de Memoria Compartida, quedando unidos a través de un bus de datos.

### Tipos de BD, según número de sitios
Según sea el número de sitios o nodos de una red en la que se almacenan físicamente los datos de una BD, se tiene la siguiente clasificación: 
 * Centralizada: la BD es almacenada en un solo nodo, un servidor central. Los demás nodos si requieren algún dato, van a acceder a ellos en forma remota. 
 * Distribuida: la BD es distribuida en distintos nodos de la red, pudiendo algunos tener acceso local o remoto a los datos. Hay diversas estrategias para definir en base a qué criterio distribuir, es decir, decidir qué dato va en qué nodo.
 * Otras:
   * Web
   * SMBD (Sistemas Múltiples BD o BD Federadas)
   * Móvil 

Tomemos como ejemplo una tabla de Clientes de un conocido Banco de nuestro país, que contiene los siguientes atributos (donde la PK está subrayada):
CLIENTE (RUT, NOMBRE, DIRECCION, REGION, NIVEL-DEUDA, NIVEL-AHORRO, CANT-PROPIEDADES)
Una BD centralizada almacena todas las filas de esta tabla en un servidor de BD o nodo central, y los demás nodos de la red si necesitan leer o escribir en la BD, acceden a ella en forma remota, a través de la red. Lo positivo de este esquema es que es fácil de implementar. Lo negativo es que se dificulta el acceso desde sitios remotos, hay un alto costo de comunicación y fracasa la BD al fracasar el sistema central (frase típica: está caído el sistema).

### Tipos de BD, según número de sitios - BD distribuida
Una BD Distribuida, se define como una base de datos lógica que es repartida físicamente entre computadores que están en distintos lugares, pero conectados por una red.
A continuación de indican las distintas estrategias para decidir cómo realizar la distribución de datos:
 * Fragmentación o Particionamiento
   * Horizontal
   * Vertical
 * Replicación
 * Híbrida
  

La fragmentación o particionamiento, consiste en dividir la tabla a distribuir en pedazos más pequeños que permitan dejar almacenados los datos más cerca de quienes más los usan. Esta fragmentación puede ser horizontal (se seleccionan filas en base a algún criterio de interés común para los distintos nodos) o vertical (se seleccionan columnas en base a algún criterio que permita satisfacer distintas funciones o procesos que se realizan en los distintos nodos).
Por ejemplo, si retomamos el ejemplo de la tabla de Clientes anterior, podemos definir:

Fragmentación horizontal en base al criterio REGION, suponiendo que hay un nodo por cada REGION, se dejará en cada nodo de la red, los datos de los clientes de esa región, de tal manera que cuando un cliente vaya a realizar una transacción, que lo más seguro será en su región, ésta se ejecute en forma local. En el fondo se particiona la tabla en tantos fragmentos como regiones tengamos, es decir, se seleccionan las filas de la tabla en base al valor del atributo REGION:   

CLIENTE-Reg1 (RUT, NOMBRE, DIRECCION, REGION, NIVEL-DEUDA, NIVEL-AHORRO, CANT-PROPIEDADES)<br/>
CLIENTE-Reg2 (RUT, NOMBRE, DIRECCION, REGION, NIVEL-DEUDA, NIVEL-AHORRO, CANT-PROPIEDADES)<br/>
CLIENTE-Reg3 (RUT, NOMBRE, DIRECCION, REGION, NIVEL-DEUDA, NIVEL-AHORRO, CANT-PROPIEDADES)<br/>
…………………………..<br/>
CLIENTE-Reg16 (RUT, NOMBRE, DIRECCION, REGION, NIVEL-DEUDA, NIVEL-AHORRO, CANT-PROPIEDADES)


Fragmentación vertical en base al criterio de seleccionar las columnas que más les sirvan a los usuarios de cada nodo, esto sirve cuando en el Banco, por ejemplo, se tiene un nodo donde se realiza la atención general al cliente y otro nodo donde se otorgan los créditos hipotecarios. Es decir, los empleados del banco que trabajan en el nodo de atención general solo requieren ciertos datos (o columnas) del cliente, y los del otro nodo, de otras columnas. Así tendríamos 2 fragmentos de la tabla Cliente:

CLIENTE-DatosGrales (RUT, NOMBRE, DIRECCION, REGION)<br/>
CLIENTE-DatosEconomicos (RUT, NIVEL-DEUDA, NIVEL-AHORRO, CANT-PROPIEDADES)<br/>

Lo positivo de este esquema es que los datos quedan almacenados más cerca de los usuarios que más los requieren. Lo negativo es que se necesita hacer una UNION de fragmentos, si se requieren todos los datos (frase típica: que lento está el sistema).


### Tipos de BD, según número de sitios - BD distribuida – Estrategia Replicación

La replicación consiste en replicar en cada nodo de la red la tabla completa. Lo positivo de este esquema es que todos los nodos tienen fácil acceso a los datos pues los tiene localmente. Lo negativo son los problemas de actualización que se producen al existir múltiples copias de un mismo dato, pudiéndose generar un alto grado de inconsistencias. Es necesario contar con un DBMS que tenga un poderoso protocolo de sincronización de las copias.

* Tipos de BD, según número de sitios - BD distribuida – Estrategia Híbrida
La estrategia híbrida mezcla todas las anteriores, o combinaciones de ellas. 
Por ejemplo, tomando nuestro ejemplo de la tabla Clientes, se podría definir una BD distribuida con estrategia híbrida de la siguiente forma:
   * Centralización: tabla Cliente completa en un nodo de la red.
   * Particionamiento horizontal: se separan los Clientes en VIP (Very Important Person) y no VIP.
   * Replicación: los Clientes VIP están replicados en todos los nodos, de tal manera de siempre poder darles una respuesta, incluso cuando el sistema central esté caído. Cabe mencionar que los clientes no VIP solo están en el servidor central.

### Tipos de BD, según número de sitios - BD en la web

En un ambiente web se puede trabajar con distintos tipos de BD. Por lo que más que identificar un tipo de BD especial para ambientes web, se busca resaltar es la modalidad de trabajo de las aplicaciones que usan BD en la web, donde se distribuye la aplicación más que los datos. Son aplicaciones que utilizan el modelo de desarrollo de software de 3 capas: Presentación, Lógica del Negocio y Datos. Esto tiene la ventaja de si se requiere hacer algún cambio en el tiempo, sólo se cambia la capa requerida, sin necesidad de revisar el código fuente de las otras capas. En ambientes web la capa de Presentación es la que interactúa con el browser que tenga la estación cliente instalado. La comunicación entre capas es a través de APIs (Application Programming Interface).

### Tipos de BD, según número de sitios - BD federada
Al igual que un país que posee un gobierno federado, divido en estados con gobiernos independientes entre sí, en el ámbito de las BDs se pueden detectar distintas BDs creadas por diferentes DBMS en diversos momentos de la vida de una organización. Por ejemplo, una BD creada en Oracle, otra en SQL-Server, otra en Sybase, etc. equivaldrían a los estados de un gobierno federado, pero que frente al paso de los años y a nuevos requerimientos, surge la necesidad de integrarlas para poder aprovechar toda esa data almacenada. Para lograr esa integración, aparecen DBMS que actúan como mediadores o el nivel central del gobierno federado, que es capaz de hacer que se comuniquen todas las BDs. La gracia de estos softwares es que permiten que las aplicaciones que desarrollemos puedan tener una mirada unificada y estandarizada de los datos, sin duplicaciones, y con un diccionario de datos (catálogo) común. Por ejemplo, si en una BD el atributo Género era almacenado como F y M, y en otra BD, como 1 y 2; es necesario definir una única forma de referirse a él, y esa única forma es la que queda almacenada en el catálogo.
En el fondo una BD federada cuenta con un DBMS que hace que varias bases de datos parezcan funcionar como una sola entidad. Utiliza wrappers, que son software o bibliotecas que permite adaptar distintas fuentes de datos para que puedan ser usadas en forma integrada.  

### Tipos de BD, según número de sitios - BD móvil
En un ambiente móvil más que identificar un tipo de BD especial, lo interesante es la posibilidad que tienen hoy en día los usuarios de acceder a información desde lejos de donde puede estar almacenada, a través de una conexión de una red inalámbrica y algún dispositivo móvil.
Los dispositivos móviles suelen tener una capacidad de memoria limitada, pero que les permite almacenar datos incluso si es que están sin conexión a la red, también proveen la posibilidad de almacenar datos georefenciados, que aumentan su potencial para la captura de datos.
En este ambiente, se requiere contar con un DBMS central de la organización, un DBMS para los datos móviles, un dispositivo móvil y enlaces de comunicación bidireccional entre el DBMS corporativo y el DBMS móvil.

### Tipos de BD, ranking
En el sitio https://db-engines.com/en/ranking pueden encontrar una tabla comparativa de los distintos DBMS que existen en el mercado actualmente.
Para abril 2020, dentro de los 10 primeros lugares están 7 DBMS relacionales y 3 no relacionales. Siendo los 4 primeros relacionales: Oracle, MySQL, Microsoft SQLServer, PostgreSQL. En el lugar 5 aparece el primero no relacional MongoDB.
Esto muestra que las BD relacionales siguen vigentes y que las BD NO-SQL (Not Only SQL) están incorporándose 