.data
.balign 8
@---- Variables ----
stringInput: .string "%d"  
stringOutput: .string "%d\n"  
n: .word 0 @ Variable 1
@---- Librerias externas ----
.text
.global main @ El codigo empezara en main
.extern printf
.extern scanf
@----- Comienzo del Programa ------

@----- No modificar estas funciones ---

@ scan 
@ Descripción: Lee un entero en formato "%d"
@ Retorna:
@ r1 = n
scan:
	@------ scanf -------
	@ r0 almacena el string con formato a recibir los inputs
	ldr r0, =stringInput
	ldr r1, =n
	mov r11, lr  @ Guardar lr antes de llamar a scanf
	bl scanf
	mov lr, r11 @ Restaurar lr después de scanf
	ldr r1,=n
	ldr r1, [r1]
	@--------------------
	mov pc, lr

@ print
@ Descripción: Imprime dos enteros en el formato descrito en stringOutput
print:
	@------ printf ------
	@ r0 almacena el string con formato a imprimir
    @ printf imprime en orden de r1 a r3 -> "%d %d %d" = "r1 r2 r3"
	ldr	r0, =stringOutput
	mov r11, lr @ Guardar lr antes de llamar a printf
	bl printf
	mov lr, r11 @ Restaurar lr después de printf
	@--------------------
	mov pc,lr

@--------------------------------------

@---- Funciones adicionales --------

@-----------------------------------

main:  
	bl scan @ Leer n 
	
    mov r3, r1 		@ r3 = n (numero a convertir)
    mov r4, #0 		@ r4 = 0 (contador)
    loop: 			@ Loop para contar los bits en 1
        and r2, r3, #1 	@ r2 = r3 & 1
        add r4, r4, r2 	@ r4 = r4 + r2
        mov r3, r3, ASR #1 	@ r3 = r3 >> 1
        cmp r3, #0 		@ Comparar r3 con 0
        bne loop 		@ Si r3 != 0, volver a loop 
    mov r1, r4 		@ r1 = r4 (numero de bits en 1)
    
	bl print @ Imprimir r1 (numero de bits en 1)
	bl _exit @ Final del programa
	