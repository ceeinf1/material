.data
.balign 8
@---- Variables ----
stringInput: .string "%d"  
stringOutput: .string "%d "  
stringEndln: .string "\n"
n: .word 0 @ Variable 1
ai: .word 0 @ Variable 2
arr: .space 400 @ Variable 3
@---- Librerias externas ----
.text
.global main
.extern printf
.extern scanf
@---- Comienzo del Programa ----

@----- No cambiar estas funciones -----

@ scan 
@ Descripción: Lee un entero 'n' (tamaño del arreglo) y luego lee 'n' enteros 'ai'. 
@ Retorna:
@ r4 = arreglo, r5 = tamaño del arreglo
scan:
	@------ scanf -------
	@ r0 almacena el string con formato a recibir los inputs
	ldr r0, =stringInput
	ldr r1, =n
	mov r11, lr @ Guardar lr antes de llamar a scanf
	bl scanf
	mov lr, r11 @ Restaurar lr después de scanf
	ldr r1, =n
	ldr r1, [r1]
	mov r5, r1 @ Tamaño de array
	mov r9, #0 @ Contador
	mov r8, #0 @ Offset
	ldr r2, =arr
buclescanf:
	cmp r9, r5
	bge finalbuclescanf
	@ r0 almacena el string con formato a recibir los inputs
	ldr r0, =stringInput
	ldr r1, =ai
	mov r11, lr  @ Guardar lr antes de llamar a scanf
	bl scanf
	mov lr, r11 @ Restaurar lr después de scanf
	ldr r1, =ai
	ldr r1, [r1]
	str r1, [r4, r8, lsl #2] @ r4[r8*4] = r1
	add r8, r8, #1 @  r8+=1
	add r9, r9, #1
	b buclescanf
finalbuclescanf:
	@--------------------
	mov pc, lr

@ print
@ Descripción: Imprime el arreglo almacenado en r4
print:
	@------ printf ------
	mov r9, #0 @ Contador
	mov r8, #0 @ Offset
bucleprintf:
	cmp r9, r5
	bge finalbucleprintf
	ldr r1, [r4, r8, lsl #2] @ r4[r8*4] = r1
	add r8, r8, #1 @
	@ r0 almacena el string con formato a imprimir
    @ printf imprime en orden de r1 a r3 -> "%d %d %d" = "r1 r2 r3"
	ldr	r0,=stringOutput
	mov r11, lr @ Guardar lr antes de llamar a printf
	bl printf
	mov lr, r11 @ Asignar lr guardado en r11 a pc después de printf
	add r9, r9, #1
	b bucleprintf
finalbucleprintf:
	ldr	r0,=stringEndln
	mov r11, lr @ Guardar lr antes de llamar a printf
	bl printf
	mov pc,r11
	@--------------------
	mov pc, lr

@--------------------------------------

@---- Funciones adicionales --------

@-----------------------------------

main:
    bl scan                     @ Llamada a la función scan

    @---- Completar Aqui El Codigo -----
    mov r3, r5                     @ r3 = n
    sub r3, r3, #1                 @ r3 = n-1
outer_loop: 
    cmp r3, #0                  @ r3 >= 0
    ble end_outer_loop             @ Si r3 < 0, termina el programa
    mov r2, #0                     @ r2 = 0
inner_loop: 
    cmp r2, r3                     @ r2 >= r3
    bge end_inner_loop             @ Si r2 >= r3, termina el inner loop
    ldr r0, [r4, r2, lsl #2]     @ r0 = arr[r2]
    add r6, r2, #1                 @ r6 = r2 + 1
    ldr r1, [r4, r6, lsl #2]     @ r1 = arr[r2+1]
    cmp r0, r1                     @ r0 >= r1
    ble skip_swap                 @ Si r0 < r1, salta el swap
    str r0, [r4, r6, lsl #2]     @ arr[r2+1] = r0
    str r1, [r4, r2, lsl #2]     @ arr[r2] = r1
skip_swap: 
    add r2, r2, #1                 @ r2 += 1
    b inner_loop                 @ Vuelve al inner loop
end_inner_loop: 
    sub r3, r3, #1                 @ r3 -= 1
    b outer_loop                 @ Vuelve al outer loop
end_outer_loop: 
    @-----------------------------------
    
    bl print         
    bl _exit @ Final del programa 
