.data
.balign 8
@---- Variables ----
stringInput: .string "%d"  
stringOutput: .string "Para que el robot avance %d metros debe dar %d pasos\n"  
n: .word 0 @ Variable 1
@---- Librerias externas ----
.text
.global main @ El codigo empezara en main
.extern printf
.extern scanf
@----- Comienzo del Programa ------

@----- No modificar estas funciones ---

@ scan 
@ Descripción: Lee un entero en formato "%d"
@ Retorna:
@ r1 = n
scan:
	@------ scanf -------
	@ r0 almacena el string con formato a recibir los inputs
	ldr r0, =stringInput
	ldr r1, =n
	mov r11, lr  @ Guardar lr antes de llamar a scanf
	bl scanf
	mov lr, r11 @ Restaurar lr después de scanf
	ldr r1,=n
	ldr r1, [r1]
	@--------------------
	mov pc, lr

@ print
@ Descripción: Imprime dos enteros en el formato descrito en stringOutput
print:
	@------ printf ------
	@ r0 almacena el string con formato a imprimir
    @ printf imprime en orden de r1 a r3 -> "%d %d %d" = "r1 r2 r3"
	ldr	r0, =stringOutput
	mov r11, lr @ Guardar lr antes de llamar a printf
	bl printf
	mov lr, r11 @ Restaurar lr después de printf
	@--------------------
	mov pc,lr

@--------------------------------------

@---- Funciones adicionales --------

@-----------------------------------


main: 
    bl scan @ Lee un entero en formato "%d"

    mov r2, #0 @ Contador de pasos
    mov r3, r1 @ Guarda el valor de n en r3
    loop: @ Loop para calcular los pasos
        cmp r1, #5 @ Compara r1 con 5
        blt paso_4 @ Si r1 < 5, salta a paso_4
        sub r1, r1, #5 @ Si r1 >= 5, resta 5 a r1
        add r2, r2, #1 @ Suma 1 a r2
        b loop @ Vuelve al loop

    paso_4: @ Si r1 < 5, salta a paso_4
        cmp r1, #4 @ Compara r1 con 4
        blt paso_3 @ Si r1 < 4, salta a paso_3
        sub r1, r1, #4 @ Si r1 >= 4, resta 4 a r1
        add r2, r2, #1 @ Suma 1 a r2
        b loop @ Vuelve al loop

    paso_3: @ Si r1 < 4, salta a paso_3
        cmp r1, #3 @ Compara r1 con 3
        blt paso_2 @ Si r1 < 3, salta a paso_2
        sub r1, r1, #3 @ Si r1 >= 3, resta 3 a r1
        add r2, r2, #1 @ Suma 1 a r2
        b loop @ Vuelve al loop

    paso_2: @ Si r1 < 2, salta a paso_2
        cmp r1, #2 @ Compara r1 con 2
        blt paso_1 @ Si r1 < 2, salta a paso_1
        sub r1, r1, #2 @ Si r1 >= 2, resta 2 a r1
        add r2, r2, #1 @ Suma 1 a r2
        b loop @ Vuelve al loop

    paso_1: @ Si r1 < 1, salta a paso_1
        cmp r1, #1 @ Compara r1 con 1
        blt end @ Si r1 < 1, salta a end
        sub r1, r1, #1 @ Si r1 >= 1, resta 1 a r1
        add r2, r2, #1 @ Suma 1 a r2
        b loop @ Vuelve al loop

    end:
        mov r1, r3 @ Guarda el valor de n en r1
        bl print 
        bl _exit @ Final del programa