.data
.balign 8
@---- Variables ----
stringInput: .string "%d"  
stringOutput: .string "[%d,%d]\n"  
n: .word 0 @ Variable 1
@---- Librerias externas ----
.text
.global main @ El codigo empezara en main
.extern printf
.extern scanf
@----- Comienzo del Programa ------

@----- No modificar estas funciones ---

@ scan 
@ Descripción: Lee un entero en formato "%d"
@ Retorna:
@ r1 = n
scan:
	@------ scanf -------
	@ r0 almacena el string con formato a recibir los inputs
	ldr r0, =stringInput
	ldr r1, =n
	mov r11, lr  @ Guardar lr antes de llamar a scanf
	bl scanf
	mov lr, r11 @ Restaurar lr después de scanf
	ldr r1,=n
	ldr r1, [r1]
	@--------------------
	mov pc, lr

@ print
@ Descripción: Imprime dos enteros en el formato descrito en stringOutput
print:
	@------ printf ------
	@ r0 almacena el string con formato a imprimir
    @ printf imprime en orden de r1 a r3 -> "%d %d %d" = "r1 r2 r3"
	ldr	r0, =stringOutput
	mov r11, lr @ Guardar lr antes de llamar a printf
	bl printf
	mov lr, r11 @ Restaurar lr después de printf
	@--------------------
	mov pc,lr

@--------------------------------------

@-----------------------------------


main:
    bl scan @ leer el numero de veces que se repite el bucle
    mov r5, r1  @ guardar r1 en r5 para usar en el bucle
    mov r4, #0  @ inicializar el contador del bucle a 0

loop:   @ bucle que se repite n veces
    cmp r4, r5 @ comparar el contador con el numero de veces que se repite el bucle
    beq end @ si son iguales, terminar el programa 
    bl scan @ leer el siguiente numero 
    mov r2, r1 @ guardar el numero en r2
    
    rsb r1, r2, #1  @ r1 = r2 - 1

    bl print    @ imprimir el numero y el numero - 1

    add r4, r4, #1 @ incrementar el contador del bucle
    b loop @ volver al principio del bucle

end: 
    bl _exit @ Final del programa