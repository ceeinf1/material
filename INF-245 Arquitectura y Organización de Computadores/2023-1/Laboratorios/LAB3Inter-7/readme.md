# Instrucciones de compilación en makefile.

Instalación de Paquetes: 
    
    sudo apt install gcc-arm-linux-gnueabihf binutils-arm-linux-gnueabihf qemu-user make

Para compilación:

    make

Para ejecutar los programas:

    Ejercicio 1:
        qemu-arm LAB3-1
    Ejercicio 2:
        qemu-arm LAB3-2
    Ejercicio 3:
        qemu-arm LAB3-3
    Ejercicio 4:
        qemu-arm LAB3-4

Para limpieza de archivos:

    make clean