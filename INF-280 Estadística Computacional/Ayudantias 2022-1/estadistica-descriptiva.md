# Estadistica Descriptiva 
## Que es la estadistica
- Ciencia dedicada al estudio sistematico de los datos.
## Que es un dato
- Representacion simbolica de un atributo, caracteristica, hecho u objeto. Tambien puede ser una imagen, texto, documento o matriz.
- Caracteristica o conjunto de caracteristicas de un objeto de interes.
## Tipos de datos
- Cualitativos o Categoricos: 
    1. Nominales: Datos que solo se pueden nombrar y determinar su frecuencia. Pertenecen a una categoria. (paises)
    
        >   Al representar este tipo de datos se debe tener especial cuidado de no mapearlos a valores numericos para no inducir errores como los asociados a un orden implicito. Una alternativa seria utilizar one hot vectors.
    
    2. Ordinales: Nominales pero con orden implicito. (nivel de satisfaccion).
- Cuantitativos:
    1. Intervalares: Datos numericos que representan distancia entre objetos que pueden ser medidos. (temperatura, fechas de calendario)
    2. Razon: Datos numericos que incluyen el 0. (Longitud, tiempo, peso )

Tambien se pueden clasificar como datos estructurados o no estructurados, o segun su cantidad de variables.

Cada una de estas categorias tiene una **escala de medicion** subyacente.

Un **conjunto de datos** es una descripcion de distintas instancias de un mismo fenomeno. Es similar al concepto de muestra.

## Analisis exploratorio de datos
-   Conjunto de tecnicas estadisticas cuya finalidad es conseguir un entendimiento basico de los datos y de las relaciones existentes entre las variables analizadas.

-   Proceso que explora los datos con fines descriptivos sin tener ideas a priori de que se busca.

Sus pasos son los siguientes:
1. Identificar variables de interes.
2. Obtener los datos
3. Visualizarlos.
4. Obtener descripciones cuantitativas.
5. Identificar anomalias.
6. Corregir algunas descripciones y/o transformar datos.
## Poblacion y muestra
- Poblacion U: Conjunto de todos los objetos/individuos que conforman un fenomeno de interes.
- Muestra S: Subconjunto disponible de la poblacion bajo estudio.
## Organizacion de los datos
Consideremos $$n$$ el tamaño total de la muestra.
### Cualitativos
- Pueden ser ordenados en $$k$$ clases $$C_1, C_2, \dots C_k$$ denominadas etiquetas.

- Cada clase $$C_i$$ tiene una frecuencia absoluta $$n_i$$.

- Cada clase $$C_i$$ tiene una frecuencia relativa $$f_i = \frac{n_i}{n}$$.

- Cada clase $$C_i$$ tiene una marca de clase $$X_i$$ correspondiente al promedio de los limites del intervalo de la clase.

- Para datos ordinales se puede incluir la frecuencia acumulada.

  > Notar que $$n = \sum_{i=1}^k n_i$$ y $$\sum_{i=1}^k f_i = 1$$.
### Cuantitativos

- Para datos cuantitativos estos pueden ser analizados como datos crudos o bien agruparse por algun criterio (intervalos de una misma amplitud).

### Tabla de frecuencias

- Numero de intervalos (numero de clases) puede ser $$k = \sqrt{n}$$.
- El rango o recorrido de la muestra viene dado por $$R_m = max\{x_i\} - min\{x_i\}$$
- La amplitud del intervalo debe cumplir $$a_i \geq \frac{R_m}{k}, \forall i = 1, \dots, k$$.
- Se debe elegir el rango de la tabla $$R_t$$, obviamente $$R_t \geq R_m$$, de lo contrario quedarian datos fuera de los intervalos.
- Una opcion es $$\Delta = R_t - R_m$$:
  1. $$L_0 = min\{x_i\} - \Delta/2$$ 
  2. $$L_1 = L_0 + a$$
  3. $$L_2 = L_1 + a$$
  4. $$L_k = L_{k-1} + a$$
- En tal caso $$R_t = [L_0; L_k]$$.
- En la tabla deben haber las siguientes columnas:
  - Marca de clase $$X_i$$ (promedio limites del intervalo).
  - Frecuencia absoluta $$n_i$$.
  - Frecuencia absoluta acumulada $$N_i$$.
  - Frecuencia relativa $$f_i$$.
  - Frecuenta relativa acumulada $$F_i$$.

## Medidas de tendencia central

- Indicadores que proporcionan un valor simple y representativo que resumen un gran volumen de informacion.
- Las clasicas son la media, la mediana y la moda.
### Media aritmetica
- Para datos no agrupados: $$\displaystyle \overline{x} = \frac{1}{n} \sum_{i = 1}^n x_i$$ 

- Para datos agrupados: $$\displaystyle \overline{x} = \sum_{i = 1}^n f_i X_i$$ 

- Buen indicador cuando la distribucion de los datos es normal o simetrica.

- Sensible a outliers.

    >   Notación: Para la media muestral se utiliza $\overline{x}$, mientras que para la poblacional $\mu$.

### Moda 

- Es el valor que ocurre con mayor frecuencia. Puede que no exista, y puede haber mas de una.

- Para datos no agrupados la moda viene dada por la clase con mayor frecuencia absoluta.
- Para datos agrupados la clase modal es: aquella con mayor frecuencia.
### Mediana
- Para datos ordinales no agrupados  la mediana viene dada por el dato del medio para n impar, y por el promedio de los dos centrales para n par.
- Para datos agrupados la clase mediana es la primera clase que acomula al menos el 50% de los datos (se puede ver la frecuencia acumada).
  - Si los datos son cuantitativos se puede obtener la mediana mediante una formula.
- Es un indicador mucho más robusto que la media aritmética.

## Medidas de dispersion

- Indican la precision de un conjunto de datos.
- La desviación estandar y la varianza son muy sensibles a outliers.

### Rango

-   Es el maximo dato de la muestra menos el minimo: $R = x_n - x_1$

-   Rango cuartilico: $$IQR  = Q_3 - Q_1 = \tilde{x}_{0.75} - \tilde{x}_{0.25}$$
-   Rango percentil: $RP  = P_{90} - P_{10} = \tilde{x}_{0.90} - \tilde{x}_{0.10}$

### Taza de variacion

- La tasa de variación es una medida de la dispersión, viene dado por: $$V = 1 - f_M$$.
- El valor máximo que puede tomar es 1 y el mínimo 0. Por ejemplo si la frecuencia modal es 0.9, esto quiere decir que el 90% de los datos es el mismo por lo que la dispersión es bajísima.
- Es la única alternativa para datos nominales.

### Varianza

- La varianza poblacional viene dada por $$S^2 = \frac{1}{n} \sum_{i = 1}^n (x_i - \overline{x}) ^2 $$
- La varianza muestral viene dada por $$\sigma^2 = \frac{1}{n - 1} \sum_{i = 1}^n  (x_i - \overline{x}) ^2 $$
- Para datos agrupados: $$S^2 = \frac{1}{n - 1} \sum_{i = 1}^k n_i (X_i - \overline{x}) ^2$$

-   Se puede descomponer como: $S_n^2 = S^2_{inter} + S^2_{intra} = \sum_i^k f_i (\overline{x}_j - \overline{x})^2 + \sum_i^k f_i S_i^2 $

### Desviacion estandar

- La desviacion estandar poblacional viene dada por $$S_n = \sqrt{ \frac{1}{n} \sum_{i = 1}^n (x_i - \overline{x}) ^2 } $$
- La desviacion estandar viene dada por $$S_n = \sqrt{ \frac{1}{n - 1} \sum_{i = 1}^n  (x_i - \overline{x}) ^2 }$$
- Para datos agrupados: $$S_n = \sqrt{\sum_{i = 1}^k f_i (X_i - \overline{x}) ^2 }$$

### Desviaion media

- Para datos agrupados $$MD = \frac{1}{n} \sum_{i = 1}^n |x_i - \overline{x}| $$

- Para datos no agrupados: $$MD = \sum_{i = 1}^k f_i | X_i - \overline{x} |$$

### Coeficiente de variacion

- Viene dado por $$c_v  = \frac{\sigma}{ | \mu | }$$, indica el grado de variabilidad respecto de la media.
- No posee unidades.
- SI $$CV$$ es cercano a 0 la muestra es mas homogenea.
- Si $$CV$$ es cercano a 1 la muestra es mas heterogenea.
- Se puede ver como porcentaje al multiplicarlo por 100.
- Generalmente se mueve entre 0 y 1, pero puede ser mayor a 1.
    - Permite comparar las dispersiones de dos distribuciones distintas. La de mayor coeficiente de variación corresponderá a la más dispersa.
- Elimina el efecto de las distintas escalas.

### Indice de dispersion

-   Medida de dispersion estandarizada.

-   Se define como $D = \frac{\sigma^2}{\mu}$.

## Medidas de asociacion de variables

### Covarianza

- Para datos no agrupados $$Cov(x,y) = \frac{1}{n} \sum_i (x_i - \overline{x}) (y_i - \overline{y})$$
- Para datos agrupados $$Cov(x,y) = \sum_i f_{ij} (X_i - \overline{x}) (Y_i - \overline{y})$$
- El dominio de $$Cov(x,y)$$ es $$\R$$.
- Si $$Cov(x,y) > 0$$ entonces existe una relacion directamente proporcional.
- Si $$Cov(x,y) < 0$$ entonces existe una relacion indirectamente proporcional.
- No entrega informacion respecto a la intensidad de la relacion.

### Correlacion de Pearson

- Viene dado por $$r_{xy} = \frac{Cov(x,y)}{S_x S_y}$$.
- Notar que $$-1 \leq r_{xy} \leq 1$$.
- Si $$r_{xy}$$ es cercano a 0, hay una relacion lineal debil o no existe tal relacion.
- Si $$r_{xy}$$ es cercano a 1, hay una relacion lineal fuerte directamente prorpocional.
- Si $$r_{xy}$$ es cercano a -1, hay una relacion lineal fuerte indirectamente prorpocional.


## Medidas de localizacion

- Las **medidas de localización** (cuantiles) dividen la muestra en partes iguales, sirven para clasificar a un dato dentro de una determinada población o muestra.

-   El cuantil $k$ es el valor tal que el $k \%$ de los datos es menor o igual a ese valor.
-   Los grupos de cuantiles mas utilizados son:
    1.   Cuartiles.
    2.   Quintiles.
    3.   Deciles.
    4.   Percentiles.
-   No hay uniformidad sobre su calculo, una forma simple es la siguiente:

$$
\tilde{x}_\alpha = \begin{cases}
   x_{\lceil n\alpha \rceil } &\text{si } n \alpha \text{ no es entero}  \\
   \frac{1}{2} (x_{n \alpha} + x_{n \alpha + 1}) &\text{ si  } n\alpha \text{ es entero}
\end{cases}
$$

## Momentos muestrales

- Los momentos muestales no centrados vienen dados por $$m_k = \frac{1}{n} \sum_{i = 1}^n x_i^k$$.

- Los momentos muestales centrados vienen dados por $$\overline{m}_k = \frac{1}{n} \sum_{i = 1}^n (x_i - m_1)^k$$.

## Medidas de forma

### Asimetria (skewness) 

Indican el sesgo, o bien la medida de falta de simetria, de una distribucion. 

Se toma como distribucion no sesgada la normal y se comparan las demas con ella.

Es una forma de comparar distribuciones.

- Coeficiente de Asimetria del momento de Fisher (INF-280):
    $$
    \gamma_1 = b_1 = \frac{\overline{m}_3}{s^3}
    $$
    
    Al calcular el sesgo usando los momentos se asume una media de calidad. Lamentablemente sabemos que esto no sirve bien en presencia de outliers, lo cual nos trae algunos problemas al estimar el sesgo.
    
    
    >   Check https://arxiv.org/pdf/1908.06400.pdf para alternativas.
    
- Coeficiente de Asimetria Fisher-Pearson (Most popular):
    $$
    g_1 = \frac{\overline{m}_3}{\overline{m}_2^{3/2}}
    $$
    
    Notar que
    
    1.   Si $$ \gamma_1 < 0 $$ entonces existe una asimetria negativa $$MO > ME > \overline{X}$$. 
    2.   Si $$ \gamma_1 > 0 $$ entonces existe una asimetria positiva $$MO < ME < \overline{X}$$. 
    3.   Si $$ \gamma_1 = 0 $$ entonces existe una distribucion simetrica $$MO = ME = \overline{X}$$. 
    
    Un estimador muestral para la skewness de la poblacion es el siguiente:
    $$
    G_1 = \frac{n}{(n - 1) (n - 2)} \sum_i^n (\frac{x_i - \overline{x}}{s})^3 = \frac{\sqrt{n (n - 1)}}{n - 2} \cdot g_1
    $$
    
    >   Check http://jse.amstat.org/v19n2/doane.pdf.
    
-   Coeficiente 1 de Asimetria de Person:
    $$
    SK_1 = \frac{\overline{x} - \hat{x}}{s}
    $$
    
-   Coeficiente 2 de Asimetria Person:

$$
SK_2 = 3 \frac{\overline{x} - \tilde{x}_{0.5}}{s}
$$

- Coeficiente de Asimetria de Bowley-Yule (Indice de simetria):
    $$
    IS = \frac{Q_1 + Q_3 - 2Q_2}{IQR}
    $$
    Solo considera el 50 por ciento central de los datos. Esto no es deseable para calcular el sesgo.



### Curtosis

- Curtosis o coeficiente de apuntamiento: Da mas informacion respecto de la varianza de los datos.

$$
\gamma_2 = \frac{\overline{m}_4}{S^4} - 3
$$

- Si $$ \gamma_2 < 0 $$ la varianza es pequena.
-  si $$ \gamma_2 > 0 $$  la varianza es grande.
- Si $$\gamma_2 = 0$$  la distribucion es normal.

## Notacion Tabla de contigencia

Dominio indices:

- $$\displaystyle k \in [1,K], j \in [1,J]$$ 

Frecuencias absolutas marginales:

-   Indica la frecuencia absoluta de ocurrencia de cada una de las variables por separado.

- $\displaystyle n_{k \cdot} = \sum_{j = 1}^J n_{kj}$
- $\displaystyle n_{\cdot j} = \sum_{k = 1}^K n_{kj}$
- $\displaystyle n = \sum_{k = 1}^K n_{k \cdot} = \sum_{j = 1}^J n_{\cdot j} = \sum_{k = 1}^K \sum_{j = 1}^J n_{kj}$

Frecuencia relativa conjunta:

- $$\displaystyle f_{kj} = \frac{n_{kj}}{n}$$ 

Frecuencias relativas marginales: 

- $$\displaystyle f_{k \cdot} = \sum_{j = 1}^J f_{kj}$$
- $$\displaystyle f_{\cdot j} = \sum_{k = 1}^K f_{kj}$$
- $$\displaystyle 1 = f_{\cdot \cdot} = \sum_{k = 1}^K f_{k \cdot}  = \sum_{j = 1}^J f_{\cdot j}$$
- $\displaystyle f_{k \cdot } = \frac{n_{k \cdot}}{ n_{\cdot \cdot}}$
- $\displaystyle f_{\cdot j} = \frac{n_{\cdot j }}{ n_{\cdot \cdot}}$

Frecuencias condicionales: 

-   La frecuencia condicional $f_{x|y}$ es una frecuencia de $x$ restringida a las ocurrencias de $y$.
-   Se condiciona el tamano de la muestra.

- $$\displaystyle f_{k | j} = \frac{f_{k j}}{f_{\cdot j}} = \frac{n_{kj}}{n_{\cdot j}}$$

## Dependencia estadistica

- Recordar que correlacion en ningun caso implica dependencia.

- Buscamos $f_{k | j} = f{k \cdot}$  y $f_{j | k} = f{\cdot j}$

    >   Esto en probablidad se verá que es análogo a $P(X | Y ) = P(X)$. Es decir, da lo mismo si ocurrio o no el evento $Y$, la probabilidad del evento $X$ es la misma.

- Dependencia entre $$X$$ e $$Y$$ solo existencia si las frecuencias condicionales $$X | Y$$ son todas iguales, es decir, no dependen de la clase condicionante.
  $$
  f_{k|1} = f_{k|2} = \dots = f_{k|J} = f_{k \cdot} \space \space \forall k = 1,2,\dots, K
  $$
  
- La ultima igualdad plantea que la frecuencia relativa marginal de cualquier clase $k$ de la variable $X$ no varia si se condiciona la muestra  solo a los elementos observados de cualquier clase $j$ asociada la variable $Y$.

- La igualdad anterior se cumple si y solo si $$f_{kj} = f_{k \cdot} f_{\cdot j}$$ para todo $k$ y $j$.